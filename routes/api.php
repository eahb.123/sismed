<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TblEmpresasApiController;
use App\Http\Controllers\AuthController;
use App\Http\Middleware\CheckPermission;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\Api\UsuariosController;
use App\Http\Controllers\Api\SupermodulosController;
use App\Http\Controllers\Api\PermisoTotalController;
use App\Http\Controllers\Api\AlmacenController;
use App\Http\Controllers\Api\ModuloController;
use App\Http\Controllers\Api\SubmoduloController;
use App\Http\Controllers\Api\PermisoController;
use App\Http\Controllers\Api\StockController;
use App\Http\Controllers\Api\MedicamentoController;
use App\Http\Controllers\Api\MovimientoController;
use App\Http\Controllers\Api\MaestraCategoriaController;
use App\Http\Controllers\Api\LibroDeControlController;
use App\Http\Controllers\Api\IciController;
use App\Http\Controllers\Api\GeneralController;
use App\Http\Controllers\Api\EvaluacionController;
use App\Http\Controllers\Api\VerificadorController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// routes/web.php o routes/api.php

Route::post('/login', [AuthController::class, 'login']);
Route::post('/guardartoken', [AuthController::class, 'guardartoken']);
Route::post('/checkauth', [AuthController::class, 'checkauth']);
Route::post('/register', [AuthController::class, 'register']);
// Ruta con middleware específico
Route::post('/sesion/listarMenu', [MenuController::class, 'listarMenu']);

//APIS LIBRES
Route::post('/administrador/maestra/almacen/listar_almacen_combo', [AlmacenController::class, 'listar_almacen_combo'])
->name('almacen.listar_almacen_combo');
Route::get('/administrador/maestra/almacen/listar_ris_combo', [AlmacenController::class, 'listar_ris_combo'])
->name('almacen.listar_ris_combo');
Route::post('/administrador/maestra/medicamento/listar_medicamento_combo', [MedicamentoController::class, 'listar_medicamento_combo'])
->name('medicamento.listar_medicamento_combo');
Route::get('/administrador/maestra/maestra_categoria/lista_documento_identidad_combo', [MaestraCategoriaController::class, 'lista_documento_identidad_combo'])
->name('maestra_categoria.lista_documento_identidad_combo');
Route::get('/administrador/maestra/supermodulo/lista_supermodulo_combo', [SupermodulosController::class, 'lista_supermodulo_combo'])
->name('supermodulo.lista_supermodulo_combo');
Route::get('/administrador/maestra/modulo/lista_modulo_combo/{id_supermodulo}', [ModuloController::class, 'lista_modulo_combo'])
->name('modulo.lista_modulo_combo');
Route::get('/administrador/maestra/submodulo/lista_submodulo_combo/{id_modulo}', [SubmoduloController::class, 'lista_submodulo_combo'])
->name('submodulo.lista_submodulo_combo');
Route::get('/administrador/maestra/permiso/lista_permiso_combo/{id_submodulo}', [PermisoController::class, 'lista_permiso_combo'])
->name('permiso.lista_permiso_combo');
Route::post('/administrador/maestra/permisototal/buscarporusuario/{id}', [PermisoTotalController::class, 'listar_permiso_total_x_usuario'])
->name('permisototal.listar_permiso_total_x_usuario');
Route::post('/administrador/maestra/estrategia/listar_estrategia_combo', [GeneralController::class, 'listar_estrategia_combo'])
->name('general.listar_estrategia_combo');



// Rutas bajo el primer nivel de prefijo 'supermodulo'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:SUPERMODULO')->prefix('supermodulo')->group(function () {
    // Ruta hija con middleware específico
    Route::get('/listar', [SupermodulosController::class, 'listar'])->name('supermodulo.listar')
    ->middleware(CheckPermission::class . ':permiso:LISTAR');;

    Route::post('/reordenar', [SupermodulosController::class, 'reordenar'])->name('supermodulo.reordenar')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/cambiarEstado', [SupermodulosController::class, 'cambiarEstado'])->name('supermodulo.cambiarEstado')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/crear', [SupermodulosController::class, 'crear'])->name('supermodulo.crear')
        ->middleware(CheckPermission::class . ':permiso:CREAR');

    Route::post('/eliminar/{id}', [SupermodulosController::class, 'eliminar'])->name('supermodulo.eliminar')
        ->middleware(CheckPermission::class . ':permiso:ELIMINAR');

    Route::put('/editar/{id}', [SupermodulosController::class, 'editar'])->name('supermodulo.editar')
        ->middleware(CheckPermission::class . ':permiso:EDITAR');
});

// Rutas bajo el primer nivel de prefijo 'usuario'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:USUARIO')->prefix('usuario')->group(function () {
    // Ruta hija con middleware específico
    Route::get('/listar', [UsuariosController::class, 'listar'])->name('usuario.listar')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/crear', [UsuariosController::class, 'crear'])->name('usuario.crear')
        ->middleware(CheckPermission::class . ':permiso:CREAR');

    Route::get('/buscar/{id}', [UsuariosController::class, 'buscar'])->name('usuario.buscar');

    Route::put('/editar/{id}', [UsuariosController::class, 'editar'])->name('usuarios.editar')
        ->middleware(CheckPermission::class . ':permiso:EDITAR');

});

// Rutas bajo el primer nivel de prefijo 'almacen'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:ALMACEN')->prefix('almacen')->group(function () {
    // Ruta hija con middleware específico
    Route::post('/listar', [AlmacenController::class, 'listar'])->name('almacen.listar');

    Route::get('/listar_ris', [AlmacenController::class, 'listar_ris'])->name('almacen.listar_ris');

    Route::post('/listar_filtrado', [AlmacenController::class, 'listar_filtrado'])->name('almacen.listar_filtrado');

});

// Rutas bajo el primer nivel de prefijo 'modulo'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:MODULO')->prefix('modulo')->group(function () {
    // Ruta hija con middleware específico
    Route::post('/listar', [ModuloController::class, 'listar'])->name('modulo.listar')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::get('/buscarporsprmodulo/{id_supermodulo}', [ModuloController::class, 'buscarporsprmodulo'])->name('modulo.buscarporsprmodulo')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/crear', [ModuloController::class, 'crear'])->name('modulo.crear');

    Route::put('/editar/{id}', [ModuloController::class, 'editar'])->name('modulo.editar');

    Route::get('/eliminar/{id}', [ModuloController::class, 'eliminar'])->name('modulo.eliminar');

});

// Rutas bajo el primer nivel de prefijo 'submodulo'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:SUBMODULO')->prefix('submodulo')->group(function () {
    // Ruta hija con middleware específico

    Route::get('/buscarpormodulo/{id_modulo}', [SubmoduloController::class, 'buscarpormodulo'])->name('submodulo.buscarpormodulo')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/listar', [SubmoduloController::class, 'listar'])->name('submodulo.listar');

    Route::post('/crear', [SubmoduloController::class, 'crear'])->name('submodulo.crear');

    Route::put('/editar/{id}', [SubmoduloController::class, 'editar'])->name('submodulo.editar');

    Route::get('/eliminar/{id}', [SubmoduloController::class, 'eliminar'])->name('submodulo.eliminar');

});

// Rutas bajo el primer nivel de prefijo 'permiso'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:PERMISO')->prefix('permiso')->group(function () {
    // Ruta hija con middleware específico

    Route::get('/buscarporsubmodulo/{id_submodulo}', [PermisoController::class, 'buscarporsubmodulo'])->name('submodulo.buscarporsubmodulo')
        ->middleware(CheckPermission::class . ':permiso:LISTAR');

    Route::post('/listar', [PermisoController::class, 'listar'])->name('permiso.listar');

    Route::post('/crear', [PermisoController::class, 'crear'])->name('permiso.crear');

    Route::put('/editar/{id}', [PermisoController::class, 'editar'])->name('permiso.editar');

    Route::get('/eliminar/{id}', [PermisoController::class, 'eliminar'])->name('permiso.eliminar');
});


// Rutas bajo el primer nivel de prefijo 'permisototal'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:SUPERMODULO')->prefix('permisototal')->group(function () {
    // Ruta hija con middleware específico
    Route::post('/buscarporusuario/{id}', [PermisoTotalController::class, 'buscarporusuario'])->name('permisototal.buscarporusuario');

    Route::post('/crear/{id_usuario}', [PermisoTotalController::class, 'crear'])->name('permisototal.crear');

    Route::post('/eliminar', [PermisoTotalController::class, 'eliminar'])->name('permisototal.eliminar');

    Route::post('/activar', [PermisoTotalController::class, 'activar'])->name('permisototal.activar');

    Route::post('/desactivar', [PermisoTotalController::class, 'desactivar'])->name('permisototal.desactivar');
});


// Rutas bajo el segundo nivel de prefijo 'empresas'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:SUPERMODULO,permiso:LISTAR')->prefix('empresas')->group(function () {
    Route::get('/', [TblEmpresasApiController::class, 'index']);
});

// Rutas bajo el primer nivel de prefijo 'medicamento'
Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA,submodulo:SUPERMODULO')->prefix('medicamento')->group(function () {
    // Ruta hija con middleware específico
    Route::post('/listar', [MedicamentoController::class, 'listar'])->name('medicamento.listar');

});

// Rutas bajo el primer nivel de prefijo 'permisototal'
Route::middleware(CheckPermission::class . ':supermodulo:CONSULTA,modulo:STOCK,submodulo:MEDICAMENTO')->prefix('stock_medicamento')->group(function () {

    Route::post('/listar', [StockController::class, 'listar'])->name('stock_medicamento.listar');

    Route::post('/listar_detalle', [StockController::class, 'listar_detalle'])->name('stock_medicamento.listar_detalle');

    Route::post('/reporte_listar_detalle_stock_medicamento', [StockController::class, 'reporte_listar_detalle_stock_medicamento'])->name('stock_medicamento.reporte_listar_detalle_stock_medicamento');

    Route::post('/cuadro_resumen', [StockController::class, 'cuadro_resumen'])->name('stock_medicamento.cuadro_resumen');

    Route::post('/detalle_cuadro_resumen', [StockController::class, 'detalle_cuadro_resumen'])->name('stock_medicamento.detalle_cuadro_resumen');

    Route::post('/lotes_por_vencer', [StockController::class, 'lotes_por_vencer'])->name('stock_medicamento.lotes_por_vencer');
});

// Rutas bajo el primer nivel de prefijo 'movimientos'
Route::middleware(CheckPermission::class . ':supermodulo:CONSULTA,modulo:MOVIMIENTOS,submodulo:ESTABLECIMIENTOS')->prefix('consulta/movimientos/establecimientos')->group(function () {

    Route::post('/listar', [MovimientoController::class, 'listar'])->name('establecimientos.listar');

});

Route::middleware(CheckPermission::class . ':supermodulo:CONSULTA,modulo:MOVIMIENTOS,submodulo:ALMACENES')->prefix('consulta/movimientos/almacenes')->group(function () {
    Route::post('/listar_almacen', [MovimientoController::class, 'listar_almacen'])->name('almacenes.listar_almacen');
});



Route::post('/administrador/maestra/maestra_categoria/lista_maestra_categoria', [MaestraCategoriaController::class, 'lista_maestra_categoria'])->name('maestra_categoria.lista_maestra_categoria');;


Route::middleware(CheckPermission::class . ':supermodulo:CONSULTA,modulo:KITS')->prefix('consulta/kits')->group(function () {

    Route::post('/botiquin/botiquin_categoria_medicamento', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.botiquin_categoria_medicamento')
    ->middleware(CheckPermission::class . ':submodulo:BOTIQUIN,permiso:LISTAR');

    Route::post('/claves/listar_claves', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_claves')
    ->middleware(CheckPermission::class . ':submodulo:CLAVES,permiso:LISTAR');

    Route::post('/coche_de_paro/listar_coche_de_paro', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_coche_de_paro')
    ->middleware(CheckPermission::class . ':submodulo:COCHE DE PARO,permiso:LISTAR');

    Route::post('/dengue/listar_dengue', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_dengue')
    ->middleware(CheckPermission::class . ':submodulo:DENGUE,permiso:LISTAR');

    Route::post('/kit_de_parto/listar_kit_de_parto', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_kit_de_parto')
    ->middleware(CheckPermission::class . ':submodulo:KIT DE PARTO,permiso:LISTAR');

    Route::post('/kit_de_recien_nacido/listar_kit_de_recien_nacido', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_kit_de_recien_nacido')
    ->middleware(CheckPermission::class . ':submodulo:KIT DE RECIEN NACIDO,permiso:LISTAR');

    Route::post('/kit_en_caso_de_violencia_sexual/listar_kit_en_caso_de_violencia_sexual', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_kit_en_caso_de_violencia_sexual')
    ->middleware(CheckPermission::class . ':submodulo:KIT EN CASO DE VIOLENCIA SEXUAL,permiso:LISTAR');

    Route::post('/kits/listar_kits', [MaestraCategoriaController::class, 'consulta_categoria_maestra_medicamento'])
    ->name('kits.listar_kits')
    ->middleware(CheckPermission::class . ':submodulo:KITS,permiso:LISTAR');

    Route::post('/kit_escenario_iii/listar_kit_escenario_iii', [MaestraCategoriaController::class, 'listar_kit_escenario_iii'])
    ->name('kits.listar_kit_escenario_iii')
    ->middleware(CheckPermission::class . ':submodulo:KIT ESCENARIO III,permiso:LISTAR');


});

/*Route::middleware(CheckPermission::class . ':supermodulo:REPORTE,modulo:ICI')->prefix('reporte/ici')->group(function () {

    Route::post('/consulta_ici_diario', [StockController::class, 'consulta_ici_diario'])
    ->name('ici.consulta_ici_diario')->middleware(CheckPermission::class . ':submodulo:ICI DIARIO,permiso:LISTAR')
    ;
});*/


Route::middleware(CheckPermission::class . ':supermodulo:LIBRO,modulo:LIBRO DE CONTROL')->prefix('libro/libro_de_control')->group(function () {

    Route::post('/libro_de_control/listar', [LibroDeControlController::class, 'libro_de_control'])
    ->name('libro_de_control.listar')->middleware(CheckPermission::class . ':submodulo:ESTUPEFACIENTE IIA,permiso:LISTAR');
});


Route::middleware(CheckPermission::class . ':registrado')->prefix('mi_cuenta/usuario')->group(function () {

    Route::post('/perfil/listar', [UsuariosController::class, 'usuario.mi_cuenta_usuario_perfil_listar'])
    ->name('mi_cuenta_usuario_perfil_listar.listar');

    Route::get('/perfil/buscarme', [UsuariosController::class, 'buscarme'])->name('usuario.buscarme');

    Route::post('/perfil/actualizarme', [UsuariosController::class, 'actualizarme'])->name('usuario.actualizarme');

    Route::put('/editar/{id}', [UsuariosController::class, 'editar'])->name('usuario.editar')
        ->middleware(CheckPermission::class . ':permiso:EDITAR');

});

Route::middleware(CheckPermission::class . ':supermodulo:REPORTE,modulo:ICI')->prefix('reporte/ici')->group(function () {

    Route::post('/ici_diario/listar', [IciController::class, 'ici_diario'])
    ->name('ici_diario.listar')->middleware(CheckPermission::class . ':submodulo:ICI DIARIO,permiso:LISTAR');

    Route::post('/ici_estrategico/listar', [IciController::class, 'ici_estrategico'])
    ->name('ici_estrategico.listar')->middleware(CheckPermission::class . ':submodulo:ICI ESTRATEGICO,permiso:LISTAR');

    Route::post('/ici_por_producto/listar', [IciController::class, 'ici_por_producto'])
    ->name('ici_por_producto.listar')->middleware(CheckPermission::class . ':submodulo:ICI POR PRODUCTO,permiso:LISTAR');
});


Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:MAESTRA')->prefix('administrador/maestra')->group(function () {

    Route::post('/verificador/validar_stk_stkd', [VerificadorController::class, 'validar_stk_stkd'])
    ->name('verificador.validar_stk_stkd')->middleware(CheckPermission::class . ':submodulo:VERIFICADOR,permiso:LISTAR');

});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/usuario/buscar_x_documento/{tipo_documento}/{numero_documento}', [EvaluacionController::class, 'buscar_x_documento'])
->name('resolucion.buscar_x_documento');

Route::get('/administrador/evaluacion/buscar_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'buscar_evaluacion'])
->name('resolucion.buscar_evaluacion');

Route::get('/administrador/evaluacion/preguntas_x_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'preguntas_x_evaluacion'])
->name('resolucion.preguntas_x_evaluacion');

Route::post('/administrador/evaluacion/guardar_actualizar_persona', [EvaluacionController::class, 'guardar_actualizar_persona'])
->name('resolucion.guardar_actualizar_persona');

Route::post('/administrador/evaluacion/resolucion/enviar_prespuesta', [EvaluacionController::class, 'enviar_prespuesta'])
->name('resolucion.enviar_prespuesta');



Route::get('/administrador/evaluacion/evaluacion/combo_estado', [EvaluacionController::class, 'combo_estado'])
->name('evaluacion.combo_estado');
Route::get('/administrador/evaluacion/evaluacion/combo_tipo', [EvaluacionController::class, 'combo_tipo'])
->name('evaluacion.combo_tipo');


Route::get('/administrador/evaluacion/evaluacion/combo_tipo_pregunta', [EvaluacionController::class, 'combo_tipo_pregunta'])
->name('evaluacion.combo_tipo_pregunta');



Route::middleware(CheckPermission::class . ':supermodulo:ADMINISTRADOR,modulo:EVALUACION')->prefix('administrador/evaluacion')->group(function () {

    Route::get('/evaluacion/listar', [EvaluacionController::class, 'evaluacion_listar'])
    ->name('evaluacion.listar')->middleware(CheckPermission::class . ':submodulo:EVALUACION,permiso:LISTAR');

    Route::post('/evaluacion/crear_evaluacion', [EvaluacionController::class, 'crear_evaluacion'])
    ->name('evaluacion.crear_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION,permiso:CREAR');

    Route::get('/evaluacion/buscar_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'buscar_evaluacion'])
    ->name('evaluacion.buscar_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::post('/evaluacion/guardar_pregunta_respuesta', [EvaluacionController::class, 'guardar_pregunta_respuesta'])
    ->name('evaluacion.guardar_pregunta_respuesta')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::get('/evaluacion/preguntas_abiertas_x_persona/{id_evaluacion}', [EvaluacionController::class, 'preguntas_abiertas_x_persona'])
    ->name('evaluacion.preguntas_abiertas_x_persona')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::post('/evaluacion/guardar_preguntas_abiertas/{id_evaluacion}', [EvaluacionController::class, 'guardar_preguntas_abiertas'])
    ->name('evaluacion.guardar_preguntas_abiertas')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::get('/evaluacion/calcular_resultados_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'calcular_resultados_evaluacion'])
    ->name('evaluacion.calcular_resultados_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::get('/evaluacion/resultados_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'resultados_evaluacion'])
    ->name('evaluacion.resultados_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    //estados
    Route::get('/evaluacion/iniciar_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'iniciar_evaluacion'])
    ->name('evaluacion.iniciar_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::get('/evaluacion/cerrado_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'cerrado_evaluacion'])
    ->name('evaluacion.cerrado_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');

    Route::get('/evaluacion/finalizado_evaluacion/{id_evaluacion}', [EvaluacionController::class, 'finalizado_evaluacion'])
    ->name('evaluacion.finalizado_evaluacion')->middleware(CheckPermission::class . ':submodulo:EVALUACION');


});
