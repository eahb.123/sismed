<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Contracts\EmpresaServiceInterface;
use App\Services\EmpresaService;
use App\Repositories\Contracts\EmpresaRepositoryInterface;
use App\Repositories\EmpresaRepository;

use App\Services\Contracts\SesionServiceInterface;
use App\Services\SesionService;
use App\Repositories\Contracts\SesionRepositoryInterface;
use App\Repositories\SesionRepository;

use App\Services\Contracts\MenuServiceInterface;
use App\Services\MenuService;
use App\Repositories\Contracts\MenuRepositoryInterface;
use App\Repositories\MenuRepository;

use App\Services\Contracts\SupermoduloServiceInterface;
use App\Services\SupermoduloService;
use App\Repositories\Contracts\SupermoduloRepositoryInterface;
use App\Repositories\SupermoduloRepository;

use App\Services\Contracts\UsuarioServiceInterface;
use App\Services\UsuarioService;
use App\Repositories\Contracts\UsuarioRepositoryInterface;
use App\Repositories\UsuarioRepository;

use App\Services\Contracts\PermisoTotalServiceInterface;
use App\Services\PermisoTotalService;
use App\Repositories\Contracts\PermisoTotalRepositoryInterface;
use App\Repositories\PermisoTotalRepository;

use App\Services\Contracts\AlmacenServiceInterface;
use App\Services\AlmacenService;
use App\Repositories\Contracts\AlmacenRepositoryInterface;
use App\Repositories\AlmacenRepository;

use App\Services\Contracts\ModuloServiceInterface;
use App\Services\ModuloService;
use App\Repositories\Contracts\ModuloRepositoryInterface;
use App\Repositories\ModuloRepository;

use App\Services\Contracts\SubmoduloServiceInterface;
use App\Services\SubmoduloService;
use App\Repositories\Contracts\SubmoduloRepositoryInterface;
use App\Repositories\SubmoduloRepository;

use App\Services\Contracts\PermisoServiceInterface;
use App\Services\PermisoService;
use App\Repositories\Contracts\PermisoRepositoryInterface;
use App\Repositories\PermisoRepository;

use App\Services\Contracts\StockServiceInterface;
use App\Services\StockService;
use App\Repositories\Contracts\StockRepositoryInterface;
use App\Repositories\StockRepository;

use App\Services\Contracts\MovimientoServiceInterface;
use App\Services\MovimientoService;
use App\Repositories\Contracts\MovimientoRepositoryInterface;
use App\Repositories\MovimientoRepository;

use App\Services\Contracts\MaestraCategoriaServiceInterface;
use App\Services\MaestraCategoriaService;
use App\Repositories\Contracts\MaestraCategoriaRepositoryInterface;
use App\Repositories\MaestraCategoriaRepository;

use App\Services\Contracts\LibroDeControlServiceInterface;
use App\Services\LibroDeControlService;
use App\Repositories\Contracts\LibroDeControlRepositoryInterface;
use App\Repositories\LibroDeControlRepository;

use App\Services\Contracts\IciServiceInterface;
use App\Services\IciService;
use App\Repositories\Contracts\IciRepositoryInterface;
use App\Repositories\IciRepository;

use App\Services\Contracts\GeneralServiceInterface;
use App\Services\GeneralService;
use App\Repositories\Contracts\GeneralRepositoryInterface;
use App\Repositories\GeneralRepository;

use App\Services\Contracts\EvaluacionServiceInterface;
use App\Services\EvaluacionService;
use App\Repositories\Contracts\EvaluacionRepositoryInterface;
use App\Repositories\EvaluacionRepository;

use App\Services\Contracts\VerificadorServiceInterface;
use App\Services\VerificadorService;
use App\Repositories\Contracts\VerificadorRepositoryInterface;
use App\Repositories\VerificadorRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //empresa
        $this->app->bind(EmpresaServiceInterface::class, EmpresaService::class);
        $this->app->bind(EmpresaRepositoryInterface::class, EmpresaRepository::class);

        //sesion
        $this->app->bind(SesionServiceInterface::class, SesionService::class);
        $this->app->bind(SesionRepositoryInterface::class, SesionRepository::class);

        //menu
        $this->app->bind(MenuServiceInterface::class, MenuService::class);
        $this->app->bind(MenuRepositoryInterface::class, MenuRepository::class);

        //supermodulo
        $this->app->bind(SupermoduloServiceInterface::class, SupermoduloService::class);
        $this->app->bind(SupermoduloRepositoryInterface::class, SupermoduloRepository::class);

        //usuario
        $this->app->bind(UsuarioServiceInterface::class, UsuarioService::class);
        $this->app->bind(UsuarioRepositoryInterface::class, UsuarioRepository::class);

        //permisoTotal
        $this->app->bind(PermisoTotalServiceInterface::class, PermisoTotalService::class);
        $this->app->bind(PermisoTotalRepositoryInterface::class, PermisoTotalRepository::class);

        //almacen
        $this->app->bind(AlmacenServiceInterface::class, AlmacenService::class);
        $this->app->bind(AlmacenRepositoryInterface::class, AlmacenRepository::class);

        //modulo
        $this->app->bind(ModuloServiceInterface::class, ModuloService::class);
        $this->app->bind(ModuloRepositoryInterface::class, ModuloRepository::class);

        //submodulo
        $this->app->bind(SubmoduloServiceInterface::class, SubmoduloService::class);
        $this->app->bind(SubmoduloRepositoryInterface::class, SubmoduloRepository::class);

        //Permiso
        $this->app->bind(PermisoServiceInterface::class, PermisoService::class);
        $this->app->bind(PermisoRepositoryInterface::class, PermisoRepository::class);

        //Stock
        $this->app->bind(StockServiceInterface::class, StockService::class);
        $this->app->bind(StockRepositoryInterface::class, StockRepository::class);

        //Movimiento
        $this->app->bind(MovimientoServiceInterface::class, MovimientoService::class);
        $this->app->bind(MovimientoRepositoryInterface::class, MovimientoRepository::class);

        //MaestraCategoria
        $this->app->bind(MaestraCategoriaServiceInterface::class, MaestraCategoriaService::class);
        $this->app->bind(MaestraCategoriaRepositoryInterface::class, MaestraCategoriaRepository::class);

        //LibroDeControl
        $this->app->bind(LibroDeControlServiceInterface::class, LibroDeControlService::class);
        $this->app->bind(LibroDeControlRepositoryInterface::class, LibroDeControlRepository::class);

        //Ici
        $this->app->bind(IciServiceInterface::class, IciService::class);
        $this->app->bind(IciRepositoryInterface::class, IciRepository::class);

        //General
        $this->app->bind(GeneralServiceInterface::class, GeneralService::class);
        $this->app->bind(GeneralRepositoryInterface::class, GeneralRepository::class);

        //Evaluacion
        $this->app->bind(EvaluacionServiceInterface::class, EvaluacionService::class);
        $this->app->bind(EvaluacionRepositoryInterface::class, EvaluacionRepository::class);

        //Verificador
        $this->app->bind(VerificadorServiceInterface::class, VerificadorService::class);
        $this->app->bind(VerificadorRepositoryInterface::class, VerificadorRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
