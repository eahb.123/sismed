<?php
// app/Repositories/Contracts/MenuRepositoryInterface.php

namespace App\Repositories\Contracts;

interface MenuRepositoryInterface
{
    public function listar($usuario);

}
