<?php
// app/Repositories/Contracts/LibroDeControlRepositoryInterface.php

namespace App\Repositories\Contracts;

interface LibroDeControlRepositoryInterface
{
    public function libroDeControl($desde,$hasta,$local,$libroDeControl);

}
