<?php
// app/Repositories/Contracts/UsuarioRepositoryInterface.php

namespace App\Repositories\Contracts;

interface UsuarioRepositoryInterface
{
    public function listar();

    public function crear($usuario,$id_usuario);

    public function buscar($id_usuario);

    public function editar($id,$usuario,$id_login);

    public function buscarme();

    public function actualizarme($usuario,$request);

}
