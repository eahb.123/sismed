<?php
// app/Repositories/Contracts/AlmacenRepositoryInterface.php

namespace App\Repositories\Contracts;

interface AlmacenRepositoryInterface
{
    public function listar($parametros);

    public function listarRis();

    public function listarFiltrado($parametros,$usuario);

}
