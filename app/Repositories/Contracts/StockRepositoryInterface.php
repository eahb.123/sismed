<?php
// app/Repositories/Contracts/StockRepositoryInterface.php

namespace App\Repositories\Contracts;

interface StockRepositoryInterface
{
    public function listar($parametros);

    public function listarDetalle($parametros);

    public function ReportelistarDetalle($parametros);

    public function CuadroResumen($parametros);

    public function DetalleCuadroResumen($parametros);

    public function LotesPorVencer($parametros);

    public function listDisponibilidadDiaria($id_almacen,$anio,$nmes,$id_tipo,$fecha);
}
