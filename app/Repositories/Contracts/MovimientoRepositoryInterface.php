<?php
// app/Repositories/Contracts/MovimientoRepositoryInterface.php

namespace App\Repositories\Contracts;

interface MovimientoRepositoryInterface
{
    public function listar($parametros);

    public function listar_almacen($parametros);

}
