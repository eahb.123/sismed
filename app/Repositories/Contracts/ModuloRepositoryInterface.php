<?php
// app/Repositories/Contracts/ModuloRepositoryInterface.php

namespace App\Repositories\Contracts;

interface ModuloRepositoryInterface
{
    public function listar($parametros);

    public function buscarPorSprmodulo($id_supermodulo);

    public function crear($modulo,$id_usuario);

    public function eliminar($id,$id_usuario);

    public function actualizar($id,$modulo,$id_usuario);

    public function lista_modulo_combo();

}
