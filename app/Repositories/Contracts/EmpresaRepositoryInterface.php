<?php
// app/Repositories/Contracts/EmpresaRepositoryInterface.php

namespace App\Repositories\Contracts;

interface EmpresaRepositoryInterface
{
    public function all();

    public function find($id);

    public function create($data);

    public function update($id, $data);

    public function delete($id);
}