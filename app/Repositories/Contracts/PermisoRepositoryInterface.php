<?php
// app/Repositories/Contracts/PermisoRepositoryInterface.php

namespace App\Repositories\Contracts;

interface PermisoRepositoryInterface
{

    public function buscarPorSubmodulo($id_submodulo);

    public function listar($parametros);

    public function crear($permiso,$id_usuario);

    public function eliminar($id,$id_usuario);

    public function actualizar($id,$permiso,$id_usuario);

}
