<?php
// app/Repositories/Contracts/PermisoTotalRepositoryInterface.php

namespace App\Repositories\Contracts;

interface PermisoTotalRepositoryInterface
{
    public function buscarPorUsuario($id_usuario,$parametros);

    public function crear($permisototal,$id_usuario);

    public function eliminarPermisosTotales($arrPermisototalIds);

    public function activarPermisosTotales($arrPermisototalIds);

    public function desactivarPermisosTotales($arrPermisototalIds);
}
