<?php
// app/Repositories/Contracts/IciRepositoryInterface.php

namespace App\Repositories\Contracts;

interface IciRepositoryInterface
{
    public function ici_diario($parametros);

    public function ici_estrategico($parametros);

    public function ici_por_producto($parametros);


}
