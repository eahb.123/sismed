<?php
// app/Repositories/Contracts/MaestraCategoriaRepositoryInterface.php

namespace App\Repositories\Contracts;

interface MaestraCategoriaRepositoryInterface
{
    public function listar($parametros);

    public function consultaCategoriaMaestraMedicamento($categoria,$ris);

    public function lista_documento_identidad_combo();

    public function listar_kit_escenario_iii();



}
