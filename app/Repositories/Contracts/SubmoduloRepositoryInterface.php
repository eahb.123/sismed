<?php
// app/Repositories/Contracts/SubmoduloRepositoryInterface.php

namespace App\Repositories\Contracts;

interface SubmoduloRepositoryInterface
{

    public function buscarPorModulo($id_modulo);

    public function listar($parametros);

    public function crear($submodulo,$id_usuario);

    public function eliminar($id,$id_usuario);

    public function actualizar($id,$submodulo,$id_usuario);

}
