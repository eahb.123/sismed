<?php
// app/Repositories/Contracts/SesionRepositoryInterface.php

namespace App\Repositories\Contracts;

interface SesionRepositoryInterface
{
    public function create($usuario,$token);

    public function validar($usuario,$token,$payload,$route);

    public function all();

    public function find($id);

    public function update($id, $data);

    public function delete($id);
}
