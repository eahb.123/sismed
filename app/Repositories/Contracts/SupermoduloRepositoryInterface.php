<?php
// app/Repositories/Contracts/SupermoduloRepositoryInterface.php

namespace App\Repositories\Contracts;

interface SupermoduloRepositoryInterface
{
    public function listar();

    public function reordenar($supermodulo);

    public function cambiarEstado($supermodulo);

    public function crear($supermodulo);

    public function eliminar($id,$id_usuario);

    public function actualizar($id,$supermodulo,$id_usuario);

    public function listarSupermoduloCombo();
}
