<?php
// app/Repositories/EvaluacionRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\EvaluacionRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;
use Illuminate\Support\Facades\Auth;
use App\Models\PermisoTotal;
use App\Models\Submodulo;
use Carbon\Carbon;


class EvaluacionRepository implements EvaluacionRepositoryInterface
{

    public function buscar_evaluacion($id_evaluacion)
    {

        $evaluacion = DB::table('eval_evaluaciones')
        ->where('eval_evaluaciones.id_evaluacion', $id_evaluacion)
        ->select('eval_evaluaciones.*') // Seleccionar solo los campos de eval_evaluaciones
        ->first();

        if ($evaluacion) {
            $catalogoMaestro = DB::table('catalogo_maestro')
                ->where('id', $evaluacion->id_estado)
                ->first(); // Traer el registro relacionado

            $evaluacion->estado = $catalogoMaestro; // Añadir como hijo
        }

        return $evaluacion;
    }

    public function preguntas_x_evaluacion($id_evaluacion)
    {
        $preguntas = DB::table('eval_preguntas')
        ->where('id_evaluacion', $id_evaluacion)
        ->get();

        // Crear un array para las respuestas (preguntas con sus alternativas)
        $resultado = [];

        foreach ($preguntas as $pregunta) {
            // Obtener las alternativas para cada pregunta
            $respuestas = DB::table('eval_respuesta')
                ->where('id_pregunta', $pregunta->id_pregunta)
                ->orderBy('id_respuesta', 'asc')
                ->get();

            $tipo_pregunta = DB::table('catalogo_maestro')
                ->where('id',$pregunta->id_tipo_pregunta)
                ->first();

            // Crear un array con la pregunta y sus alternativas
            $resultado[] = [
                'id_pregunta' => $pregunta->id_pregunta,
                'id_tipo_pregunta' => $pregunta->id_tipo_pregunta,
                'pregunta' => $pregunta->pregunta_texto,
                'tipo_pregunta' => $tipo_pregunta,
                'alternativas' => $respuestas->map(function ($respuesta) {
                    return [
                        'id_alternativa' => $respuesta->id_respuesta,
                        'alternativa' => $respuesta->respuesta_texto,
                        'es_correcta' => $respuesta->es_correcta,
                    ];
                })
            ];

        }

    return $resultado;
    }

    public function buscar_x_documento($tipo_documento,$numero_documento)
    {
        $personas = DB::table('tbl_persona')
        ->where('id_tipo_documento', $tipo_documento)
        ->where('numero_documento', $numero_documento)
        ->first();


    return $personas;
    }

    public function guardar_actualizar_persona($parametros)
    {

        if ($parametros["parametros"]["id"] == 0) {

            $inserted = DB::table('tbl_persona')->insertGetId([
                'nombres' => $parametros["parametros"]["nombres"],
                'ape_paterno' => $parametros["parametros"]["ape_paterno"],
                'ape_materno' => $parametros["parametros"]["ape_materno"],
                'id_tipo_documento' => $parametros["parametros"]["id_tipo_documento"],
                'numero_documento' => $parametros["parametros"]["numero_documento"],
                'sexo' => $parametros["parametros"]["sexo"],
                'email' => $parametros["parametros"]["email"],
                'celular' =>$parametros["parametros"]["celular"],
                'direccion' => $parametros["parametros"]["direccion"],

            ]);
            return $inserted;
        }else{

            $updated = DB::table('tbl_persona')
            ->where('id', $parametros["parametros"]["id"])
            ->update([
                'nombres' => $parametros["parametros"]["nombres"],
                'ape_paterno' => $parametros["parametros"]["ape_paterno"],
                'ape_materno' => $parametros["parametros"]["ape_materno"],
                'sexo' => $parametros["parametros"]["sexo"],
                'email' => $parametros["parametros"]["email"],
                'celular' =>$parametros["parametros"]["celular"],
                'direccion' => $parametros["parametros"]["direccion"],
            ]);

            return $parametros["parametros"]["id"];
        }

    }


    public function enviar_prespuesta($parametros)
    {

        $id_evaluacion = $parametros["parametros"]["id_evaluacion"];
        $alternativas = DB::table('eval_preguntas')
        ->join('eval_respuesta','eval_preguntas.id_pregunta','=','eval_respuesta.id_pregunta')
        ->where('id_evaluacion', $id_evaluacion)
        ->get();

        $respuestas = $parametros["parametros"]["respuestasFinales"];

        if (count($respuestas) != count($alternativas)) {
            return [
                'success' => false,
                'mensaje' => "La cantidad de respuestas no coincide por favor volver a resolver la evaluación"
            ];
        }

        foreach ($alternativas as $alternativa) {
            foreach ($respuestas as $respuesta) {
                if ($alternativa->id_respuesta == $respuesta["id_alternativa"]) {
                    if ($alternativa->id_tipo_pregunta == 1 || $alternativa->id_tipo_pregunta == 2) {
                        $inserted = DB::table('eval_persona_respuesta')->upsert([
                            'id_persona' => $parametros["parametros"]["id_persona"],
                            'id_pregunta' => $respuesta["id_pregunta"],
                            'id_respuesta' => $respuesta["id_alternativa"],
                            'respuesta_texto_persona' => '',
                            'respuesta_boolean_persona' =>(bool) $respuesta["respuestas"],
                            'correccion' => null
                        ],
                        ['id_pregunta','id_respuesta','id_persona'],
                        ['respuesta_texto_persona','respuesta_boolean_persona','correccion']
                    );
                    }

                    if ($alternativa->id_tipo_pregunta == 3) {

                        $inserted = DB::table('eval_persona_respuesta')->upsert([
                            'id_persona' => $parametros["parametros"]["id_persona"],
                            'id_pregunta' => $respuesta["id_pregunta"],
                            'id_respuesta' => $respuesta["id_alternativa"],
                            'respuesta_texto_persona' => $respuesta["respuestas"],
                            'respuesta_boolean_persona' => null,
                            'correccion' => null
                        ],
                        ['id_persona','id_pregunta','id_respuesta'],
                        ['respuesta_texto_persona','respuesta_boolean_persona','correccion']
                    );
                    }
                }

            }

        }
        return [
            'success' => true,
            'mensaje' => "Información guardada correctamente"
        ];

    }


    public function evaluacion_listar()
    {
        $evaluaciones = DB::table('eval_evaluaciones')
        ->join('catalogo_maestro','eval_evaluaciones.id_estado','=','catalogo_maestro.id')
        ->select('eval_evaluaciones.*',
                'catalogo_maestro.id as id_estado',
                'catalogo_maestro.valor as estado'
                )
        ->get();


        return $evaluaciones;
    }


    public function combo_estado()
    {
        $estado = DB::table('catalogo_maestro')
        ->where('categoria', 'ESTADO DE EVALUACION')
        ->orderBy('orden', 'asc')
        ->get();
        return $estado;
    }

    public function combo_tipo()
    {
        $tipo = DB::table('catalogo_maestro')
        ->where('categoria', 'TIPO DE EVALUACION')
        ->get();
        return $tipo;
    }

    public function combo_tipo_pregunta()
    {
        $tipo = DB::table('catalogo_maestro')
        ->where('categoria', 'TIPO DE PREGUNTA')
        ->get();
        return $tipo;
    }


    public function crear_evaluacion($parametros)
    {

        $usuario = Auth::user();


        $id_usuario=$usuario->id_usuario;
        $fechaActual = now();


        if ($parametros["id_evaluacion"] == 0) {

            $insertedId = DB::table('eval_evaluaciones')->insertGetId([
                'nombre' => $parametros["nombre"],
                'descripcion' => $parametros["descripcion"],
                'id_estado' => $parametros["id_estado"],
                'id_tipo_evaluacion' => $parametros["id_tipo_evaluacion"],
                'fecha_hora_limite' => $parametros["fecha_hora_limite"],
                'fecha_crear' => $fechaActual,
                'usuario_crear' => $id_usuario,
            ], 'id_evaluacion');  // Especificamos la columna que queremos como retorno

            // Obtener el registro completo con la columna correcta 'id_evaluacion'

            return $insertedId;
        }else{

            $updated = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $parametros["id_evaluacion"])
            ->update([
                'nombre' => $parametros["nombre"],
                'descripcion' => $parametros["descripcion"],
                'id_estado' => $parametros["id_estado"],
                'id_tipo_evaluacion' => $parametros["id_tipo_evaluacion"],
                'fecha_hora_limite' => $parametros["fecha_hora_limite"],
                'fecha_editar' => $fechaActual,
                'usuario_editar' => $id_usuario,
            ]);

            return $parametros["id_evaluacion"];
        }

    }

    public function guardar_pregunta_respuesta($parametros)
    {


        $usuario = Auth::user();

        $id_usuario=$usuario->id_usuario;
        $fechaActual = now();

        //($parametros["cabeceras"],'$parametros');
        if (isset($parametros["cabeceras"]) && isset($parametros["preguntas"]) ) {

            $id_evaluacion = $parametros["cabeceras"];
            $preguntas = $parametros["preguntas"];

            $relacionadas = DB::table('eval_persona_respuesta')
            ->whereIn('id_respuesta', function ($query) use ($id_evaluacion) {
                $query->select('id_respuesta')
                    ->from('eval_respuesta')
                    ->whereIn('id_pregunta', function ($query) use ($id_evaluacion) {
                        $query->select('id_pregunta')
                            ->from('eval_preguntas')
                            ->where('id_evaluacion', $id_evaluacion);
                    });
            })
            ->exists();

            if ($relacionadas) {
                return [
                    'success' => false,
                    'mensaje' => 'No se pudo actualizar, existen respuestas relacionadas.'
                ];
                // Si hay registros relacionados, abortamos la transacción y devolvemos un mensaje de error

            }
            // Eliminar las respuestas asociadas a las preguntas de una evaluación específica
            $eliminarespuestas = DB::table('eval_respuesta')
            ->whereIn('id_pregunta', function ($query) use ($id_evaluacion) {
                $query->select('id_pregunta')
                    ->from('eval_preguntas')
                    ->where('id_evaluacion', $id_evaluacion);
            })
            ->delete();

            // Eliminar las preguntas de una evaluación específica
            DB::table('eval_preguntas')
            ->where('id_evaluacion', $id_evaluacion)
            ->delete();

            foreach ($preguntas as $pregunta) {

                $id_pregunta = DB::table('eval_preguntas')->insertGetId([
                    'id_tipo_pregunta' => $pregunta['id_tipo_pregunta'],
                    'pregunta_texto' => $pregunta['pregunta'],
                    'id_evaluacion' => $id_evaluacion,
                ],'id_pregunta');

                foreach ($pregunta["alternativas"] as $respuesta) {

                    DB::table('eval_respuesta')->insert([
                        'id_pregunta' => $id_pregunta,
                        'respuesta_texto' => $respuesta['alternativa'],
                        'es_correcta' => $respuesta['es_correcta'],
                    ]);

                }
            }
            return [
                'success' => true,
                'mensaje' => 'Preguntas creadas correctamente.'
            ];
        }
    }



    public function preguntas_abiertas_x_persona($id_evaluacion)
    {

        $resultado = [];

        $preguntas = DB::table('eval_preguntas')
        ->join('catalogo_maestro','eval_preguntas.id_tipo_pregunta','=','catalogo_maestro.id')
        ->where('eval_preguntas.id_evaluacion', $id_evaluacion)
        ->where('catalogo_maestro.valor', 'RESPUESTA ABIERTA')
        ->get();

        foreach ($preguntas as $pregunta) {

            $respuestas = DB::table('eval_respuesta')
            ->select(
                'eval_respuesta.id_respuesta as id_respuesta',
                'eval_respuesta.id_pregunta as id_pregunta',
                'eval_respuesta.es_correcta as es_correcta',

                'eval_persona_respuesta.id_persona_respuesta as id_persona_respuesta',
                'eval_persona_respuesta.id_persona as id_persona',
                'eval_persona_respuesta.respuesta_texto_persona as respuesta_texto_persona',
                'eval_persona_respuesta.respuesta_boolean_persona as respuesta_boolean_persona',
                'eval_persona_respuesta.correccion as correccion',

                'tbl_persona.id as id_persona',
                'tbl_persona.nombres as nombres',
                'tbl_persona.ape_paterno as ape_paterno',
                'tbl_persona.ape_materno as ape_materno'
            )
            ->join('eval_persona_respuesta','eval_respuesta.id_respuesta','=','eval_persona_respuesta.id_respuesta')
            ->join('tbl_persona','eval_persona_respuesta.id_persona','=','tbl_persona.id')
            ->where('eval_respuesta.id_pregunta', $pregunta->id_pregunta)
            ->orderBy('eval_respuesta.id_respuesta', 'asc')
            ->orderBy('tbl_persona.id', 'asc')
            ->get();

            $tipo_pregunta = DB::table('catalogo_maestro')
            ->where('id',$pregunta->id_tipo_pregunta)
            ->first();


            $resultado[] = [
                'id_pregunta' => $pregunta->id_pregunta,
                'id_tipo_pregunta' => $pregunta->id_tipo_pregunta,
                'pregunta' => $pregunta->pregunta_texto,
                'tipo_pregunta' => $tipo_pregunta,
                'persona_respuestas' => $respuestas->map(function ($respuesta) {
                    return [
                        'id_respuesta' => $respuesta->id_respuesta,
                        'id_pregunta' => $respuesta->id_pregunta,
                        'es_correcta' => $respuesta->es_correcta,

                        'id_persona_respuesta' => $respuesta->id_persona_respuesta,
                        'id_persona' => $respuesta->id_persona,
                        'respuesta_texto_persona' => $respuesta->respuesta_texto_persona,
                        'respuesta_boolean_persona' => $respuesta->respuesta_boolean_persona,
                        'correccion' => $respuesta->correccion,

                        'nombres' => $respuesta->nombres,
                        'ape_paterno' => $respuesta->ape_paterno,
                        'ape_materno' => $respuesta->ape_materno,
                    ];
                })
            ];

        }
        return [
            'success' => true,
            'mensaje' => 'Preguntas creadas correctamente.',
            'data' => $resultado
        ];

    }

    public function guardar_preguntas_abiertas($id_evaluacion,$respuestas)
    {
        $updates = [];

        //($respuestas);
        // Recorre todas las preguntas
        foreach ($respuestas as $respuesta) {

            foreach ($respuesta['persona_respuestas'] as $opciones) {
                // Guarda cada actualización en el array
                $updates[] = [
                    'id_persona_respuesta' => $opciones['id_persona_respuesta'],
                    'correccion' => $opciones['correccion'],
                ];
            }
        }

        // Usar DB::transaction para realizar el volcado completo en una transacción
        DB::transaction(function() use ($updates) {
            foreach ($updates as $update) {
                // Realiza la actualización utilizando DB::table (sin Eloquent ORM)
                DB::table('eval_persona_respuesta')
                    ->where('id_persona_respuesta', $update['id_persona_respuesta'])
                    ->update([
                        'correccion' => $update['correccion']
                    ]);
            }
        });


        return [
            'success' => true,
            'mensaje' => 'preguntas guardadas correctamente.',
        ];

    }


    public function calcular_resultados_evaluacion($id_evaluacion)
    {

        DB::transaction(function () use ($id_evaluacion) {

            // Paso 1: Actualizar las respuestas en `eval_persona_respuesta` con las correcciones
            DB::statement("
            UPDATE eval_persona_respuesta epr
            SET correccion = CASE
                    WHEN epr.respuesta_boolean_persona = er.es_correcta THEN TRUE
                    ELSE
                        FALSE
                END
            FROM eval_respuesta er
            WHERE epr.id_respuesta = er.id_respuesta
            AND er.es_correcta IS NOT NULL  -- Solo actualiza cuando es_correcta NO sea NULL
            AND epr.id_pregunta IN (
                SELECT id_pregunta
                FROM eval_preguntas
                WHERE id_evaluacion = :id_evaluacion
            )
            ", ['id_evaluacion' => $id_evaluacion]);


            // Paso 2: Eliminar los registros anteriores en `eval_persona_pregunta` (si corresponde)
            DB::statement("
                DELETE FROM eval_persona_pregunta
                WHERE id_pregunta IN (
                    SELECT id_pregunta
                    FROM eval_preguntas
                    WHERE id_evaluacion = :id_evaluacion
                )
            ", ['id_evaluacion' => $id_evaluacion]);

            // Paso 3: Calcular las respuestas correctas y actualizar `eval_persona_pregunta`
            DB::statement("
            INSERT INTO eval_persona_pregunta (id_persona, id_pregunta, correccion)
            SELECT epr.id_persona, epr.id_pregunta,
                   -- Marcar como TRUE si todas las respuestas son correctas (ninguna incorrecta)
                   CASE
                       WHEN NOT EXISTS (
                           SELECT 1
                           FROM eval_persona_respuesta epr_check
                           WHERE epr_check.id_pregunta = epr.id_pregunta
                           AND epr_check.id_persona = epr.id_persona
                           AND (epr_check.correccion IS NULL OR epr_check.correccion = FALSE)
                       ) THEN TRUE
                       -- Si alguna respuesta es incorrecta o no corregida, marcar como FALSE
                       ELSE FALSE
                   END as correccion
            FROM eval_persona_respuesta epr
            WHERE epr.id_pregunta IN (
                SELECT id_pregunta
                FROM eval_preguntas
                WHERE id_evaluacion = :id_evaluacion
            )
            AND NOT EXISTS (
                -- Verifica que no exista un registro en eval_persona_pregunta con el mismo id_persona y id_pregunta
                SELECT 1
                FROM eval_persona_pregunta epp
                WHERE epp.id_persona = epr.id_persona
                AND epp.id_pregunta = epr.id_pregunta
            )
        ", ['id_evaluacion' => $id_evaluacion]);




            // Paso 4: Eliminar los registros anteriores en `eval_persona_evaluacion` (si corresponde)
            DB::statement("
                DELETE FROM eval_persona_evaluacion
                WHERE id_evaluacion = :id_evaluacion
            ", ['id_evaluacion' => $id_evaluacion]);

            // Paso 5: Calcular la cantidad de preguntas, respuestas correctas y el porcentaje por persona
            DB::statement("
                INSERT INTO eval_persona_evaluacion (id_persona, id_evaluacion, cantidad_pregunta, cantidad_pregunta_correcta, porcentaje)
                SELECT epr.id_persona,
                       :id_evaluacion AS id_evaluacion,
                       COUNT(DISTINCT epr.id_pregunta) AS cantidad_pregunta,
                       COUNT(DISTINCT CASE WHEN epr.correccion = TRUE THEN epr.id_pregunta END) AS cantidad_pregunta_correcta,
                       -- Calculamos el porcentaje (respuesta correcta / total preguntas)
                       ROUND(
                           (COUNT(DISTINCT CASE WHEN epr.correccion = TRUE THEN epr.id_pregunta END) * 100.0) /
                           COUNT(DISTINCT epr.id_pregunta)
                       ) AS porcentaje
                FROM eval_persona_respuesta epr
                WHERE epr.id_pregunta IN (
                    SELECT id_pregunta
                    FROM eval_preguntas
                    WHERE id_evaluacion = :id_evaluacion
                )
                GROUP BY epr.id_persona
            ", ['id_evaluacion' => $id_evaluacion]);

        });

        return [
            'success' => true,
            'mensaje' => 'Respuestas calculadas correctamente.',
        ];

    }


    public function resultados_evaluacion($id_evaluacion)
    {

        $resultados = DB::table('eval_persona_evaluacion')
        ->join('tbl_persona','eval_persona_evaluacion.id_persona','=','tbl_persona.id')
        ->join('tbl_tablas','tbl_persona.id_tipo_documento','=','tbl_tablas.id_tipo')
        ->where('eval_persona_evaluacion.id_evaluacion', $id_evaluacion)
        ->where('tbl_tablas.id_tabla', '13')
        ->select(
            'tbl_persona.*',  // Selecciona todas las columnas de tbl_persona
            'eval_persona_evaluacion.*',
            'tbl_tablas.id_tipo as id_tipo_doc_identidad',
            'tbl_tablas.abreviado as tipo_doc_identidad'
        )
        ->get();

        return [
            'success' => true,
            'mensaje' => 'preguntas guardadas correctamente.',
            'data' => $resultados
        ];

    }


    public function iniciar_evaluacion($id_evaluacion)
    {
        $estado = DB::table('catalogo_maestro')
            ->where('categoria', 'ESTADO DE EVALUACION')
            ->where('valor', 'INICIADO')
            ->first();
        if (!$estado) {
            return [
                'success' => false,
                'mensaje' => 'Error en el estado'
            ];
        }

        $evaluacion = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->first();

        if (!$evaluacion) {
            return [
                'success' => false,
                'mensaje' => 'Evaluacion no encontrada'
            ];
        }

        $updated = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->update([
                'id_estado' => $estado->id
            ]);

        if ($updated) {
            return [
                'success' => true,
                'mensaje' => 'Evaluacion actualizada correctamente.',
            ];
        } else {
            return [
                'success' => false,
                'mensaje' => 'Error al actualizar la evaluación',

            ];
        }
    }

    public function cerrado_evaluacion($id_evaluacion)
    {
        $estado = DB::table('catalogo_maestro')
            ->where('categoria', 'ESTADO DE EVALUACION')
            ->where('valor', 'CERRADO')
            ->first();

        if (!$estado) {
            return [
                'success' => false,
                'mensaje' => 'Error en el estado'
            ];
        }

        $evaluacion = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->first();

        if (!$evaluacion) {
            return [
                'success' => false,
                'mensaje' => 'Evaluacion no encontrada'
            ];
        }

        $updated = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->update([
                'id_estado' => $estado->id
            ]);

        if ($updated) {
            return [
                'success' => true,
                'mensaje' => 'Evaluacion actualizada correctamente.',
            ];
        } else {
            return [
                'success' => false,
                'mensaje' => 'Error al actualizar la evaluación',

            ];
        }

    }

    public function finalizado_evaluacion($id_evaluacion)
    {
        $estado = DB::table('catalogo_maestro')
            ->where('categoria', 'ESTADO DE EVALUACION')
            ->where('valor', 'FINALIZADO')
            ->first();

        if (!$estado) {
            return [
                'success' => false,
                'mensaje' => 'Error en el estado'
            ];
        }

        $evaluacion = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->first();

        if (!$evaluacion) {
            return [
                'success' => false,
                'mensaje' => 'Evaluacion no encontrada'
            ];
        }

        $updated = DB::table('eval_evaluaciones')
            ->where('id_evaluacion', $id_evaluacion)
            ->update([
                'id_estado' => $estado->id
            ]);

        if ($updated) {
            return [
                'success' => true,
                'mensaje' => 'Evaluacion actualizada correctamente.',
            ];
        } else {
            return [
                'success' => false,
                'mensaje' => 'Error al actualizar la evaluación',

            ];
        }
    }


}
