<?php
// app/Repositories/StockRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\StockRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
//use App\Models\Almacen;


class StockRepository implements StockRepositoryInterface
{

    public function listar($parametros)
    {
        $risWhere="";
        $almacenWhere="";
        $medicamentoWhere="";

        if (isset($parametros["ris"]) && $parametros["ris"]!=null) {
            $ris = $parametros["ris"];
            $risWhere = " AND a.id_ris = '$ris' ";
        }

        if (isset($parametros["almacen"]) && !empty($parametros["almacen"])) {
            $almacen = implode('\',\'',$parametros["almacen"]);
            $almacenWhere = " AND a.id_almacen IN ('$almacen')  ";
        }

        if (isset($parametros["medicamento"]) && $parametros["medicamento"]!=null) {
            $medicamento = $parametros["medicamento"];
            $medicamentoWhere = " AND m.id_medicamento = '$medicamento' ";
        }

        $limitSql="";
        if (isset($parametros["cantidad"]) && isset($parametros["pagina"])) {
            $limitSql=" LIMIT ".$parametros["cantidad"]." OFFSET ".$parametros["pagina"]*$parametros["cantidad"];
        }

        $orderSql="";
        if (isset($parametros["campo"]) && isset($parametros["direccion"])) {
            $orderSql=" ORDER BY ".$parametros["campo"]." ".$parametros["direccion"];
        }

        $resultadosPaginados = DB::select("
        SELECT
            r.descripcion AS ris_nombre,
            a.id_alterno AS almacen_codigo,
            i.id_almacen AS almacen_codigo_total,
            a.nombre AS almacen_nombre,
            MAX(m.id_medicamento) AS medicamento_codigo,
            m.nombre AS medicamento_nombre,
            m.presentacion AS medicamento_presentacion,
            m.concentracion AS medicamento_concentracion,
            m.forma_farmaceutica AS medicamento_forma_farmaceutica,
            CONCAT(i.id_almacen, MAX(i.id_medicamento)) AS \"i.id_stock\",
            i.stock AS stock,
            TO_CHAR(i.fecha_ult, 'DD-MM-YYYY') AS stock_ultima_fecha_mov,
            TO_CHAR(i.fecha_actualizacion, 'DD-MM-YYYY HH24:MI') AS fecha_reporte,
            STRING_AGG(DISTINCT pe.nombre, '/') AS progr_estrat
        FROM
            tbl_stocks i
            INNER JOIN tbl_almacenes a ON i.id_almacen = a.id_almacen
            LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla = '1'
            INNER JOIN tbl_medicamentos m ON i.id_medicamento = m.id_medicamento
            LEFT JOIN tbl_progr_estrat_medicamento pem ON m.id_medicamento = pem.id_medicamento
            LEFT JOIN tbl_progr_estrat pe ON pem.id_progr_estrat = pe.id_progr_estrat
        WHERE
            i.id_almacen IS NOT NULL
            AND a.estado = true
            AND a.instalado = true

            $risWhere
            $almacenWhere
            $medicamentoWhere
        GROUP BY
            a.nombre, r.descripcion, m.nombre, i.id_almacen, a.id_alterno, m.presentacion, m.concentracion, m.forma_farmaceutica, i.stock, i.fecha_ult, i.fecha_actualizacion
        $orderSql
        $limitSql
        ");


        $totalResultados = DB::select("
        SELECT COUNT(*) AS total
            FROM (
                SELECT
                    a.nombre AS almacen_nombre,
                    r.descripcion AS ris_nombre,
                    m.nombre AS medicamento_nombre,
                    CONCAT(i.id_almacen, MAX(i.id_medicamento)) AS \"i.id_stock\",
                    a.id_alterno AS almacen_codigo,
                    MAX(m.id_medicamento) AS medicamento_codigo,
                    m.presentacion AS medicamento_presentacion,
                    m.concentracion AS medicamento_concentracion,
                    m.forma_farmaceutica AS medicamento_forma_farmaceutica,
                    i.stock AS stock,
                    TO_CHAR(i.fecha_ult, 'DD-MM-YYYY') AS stock_ultima_fecha_mov,
                    TO_CHAR(i.fecha_actualizacion, 'DD-MM-YYYY HH24:MI') AS fecha_reporte,
                    STRING_AGG(DISTINCT pe.nombre, '/') AS progr_estrat
                FROM
                    tbl_stocks i
                    INNER JOIN tbl_almacenes a ON i.id_almacen = a.id_almacen
                    LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla = '1'
                    INNER JOIN tbl_medicamentos m ON i.id_medicamento = m.id_medicamento
                    LEFT JOIN tbl_progr_estrat_medicamento pem ON m.id_medicamento = pem.id_medicamento
                    LEFT JOIN tbl_progr_estrat pe ON pem.id_progr_estrat = pe.id_progr_estrat
                WHERE
                    i.id_almacen IS NOT NULL
                    AND a.estado = true
                    $risWhere
                    $almacenWhere
                    $medicamentoWhere
                GROUP BY
                    a.nombre, r.descripcion, m.nombre, i.id_almacen, a.id_alterno, m.presentacion, m.concentracion, m.forma_farmaceutica, i.stock, i.fecha_ult, i.fecha_actualizacion
            ) AS subconsulta;
        ");

        // Extraer el total de la consulta
        $total = $totalResultados[0]->total;

        return [
                'resultsLength' => $total,
                'data' => $resultadosPaginados
            ];
        //$almacenes = Almacen::all();
    }


    public function listarDetalle($parametros)
    {
        $id_almacen = $parametros["almacen_codigo_total"];
        $id_medicamento = $parametros["medicamento_codigo"];


        $sql="SELECT *
                FROM
                    tbl_stocks_detalle
                WHERE
                id_almacen = '$id_almacen'
                AND id_medicamento = '$id_medicamento'
                AND stock>0";

        $totalResultados = DB::select($sql);
        //echo ($sql);
        return $totalResultados;
    }


    public function ReportelistarDetalle($parametros)
    {
        $risWhere="";
        $almacenWhere="";
        $medicamentoWhere="";


        if (isset($parametros["ris"]) && $parametros["ris"]!=null) {
            $ris = $parametros["ris"];
            $risWhere = " AND a.id_ris = '$ris' ";
        }

        if (isset($parametros["almacen"]) && !empty($parametros["almacen"])) {
            $almacen = implode('\',\'',$parametros["almacen"]);
            $almacenWhere = " AND a.id_almacen IN ('$almacen')  ";
        }

        if (isset($parametros["medicamento"]) && $parametros["medicamento"]!=null) {
            $medicamento = $parametros["medicamento"];
            $medicamentoWhere = " AND m.id_medicamento = '$medicamento' ";
        }



        $sql="SELECT
                a.id_almacen AS almacen_codigo,
                a.nombre AS almacen_nombre,
                m.id_medicamento AS medicamento_codigo,
                m.nombre AS medicamento_nombre,
                m.presentacion AS medicamento_presentacion,
                m.concentracion AS medicamento_concentracion,
                m.forma_farmaceutica AS medicamento_forma_farmaceutica,
                r.descripcion as ris,
                l.stock AS stock,
                l.lote AS lote,
                l.fecha_vto AS fecha_vencimiento,
                l.tipsum AS tipo_suministro,
                l.ffinan AS fuente_financiamiento,
                l.med_reg_san AS registro_sanitario,
                l.precio_op AS precio_de_opracion,
                STRING_AGG(DISTINCT pe.nombre, '/') AS progr_estrat
            FROM
                tbl_stocks_detalle l
            INNER JOIN tbl_almacenes a ON l.id_almacen=a.id_almacen
            LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla='1'
            INNER JOIN tbl_medicamentos m ON l.id_medicamento = m.id_medicamento
            LEFT JOIN tbl_progr_estrat_medicamento pem ON l.id_medicamento = pem.id_medicamento
            LEFT JOIN tbl_progr_estrat pe ON pem.id_progr_estrat = pe.id_progr_estrat
            WHERE
            a.estado = true
            AND a.instalado = true
            $risWhere
            $almacenWhere
            $medicamentoWhere
            GROUP BY
            a.id_almacen, a.nombre, m.id_medicamento, m.nombre, m.presentacion, m.concentracion, m.forma_farmaceutica, r.descripcion, l.stock,l.lote,l.fecha_vto,
            l.tipsum, l.ffinan, l.med_reg_san, l.precio_op";

        $totalResultados = DB::select($sql);
        //echo ($sql);
        return $totalResultados;
    }


    public function CuadroResumen($parametros)
    {
        if ($parametros['campo'] == 'cod_almacen_1' || $parametros['campo'] == 'nombre_almacen_1' || $parametros['campo'] == 'cantidad_total_1' || $parametros['campo'] == 'fecha_1' ) {
            $parametros['campo'] = substr($parametros['campo'],0,-2);
        }

        if (isset($parametros["campo"]) && isset($parametros["direccion"])) {
            if ($parametros["campo"] == "fecha") {
                $orderSql=" ORDER BY ".$parametros["campo"]."::timestamp ".$parametros["direccion"];
            }else{
                $orderSql=" ORDER BY ".$parametros["campo"]." ".$parametros["direccion"];
            }
        }

        $sql="SELECT
                id_almacen cod_almacen,
                almacen nombre_almacen,
                cantidad_tot cantidad_total,
                cantidad_med cantidad_medicamento,
                con_stock_med con_stock_medicamento,
                sin_stock_med sin_stock_medicamento,
                cantidad_ins cantidad_insumo,
                con_stock_ins con_stock_insumo,
                sin_stock_ins sin_stock_insumo,
                fecha fecha
            FROM
                vw_stock80
            ". $orderSql;

        $totalResultados = DB::select($sql);
        //echo ($sql);
        return $totalResultados;
    }

    public function DetalleCuadroResumen($parametros)
    {

        $medicamento = ($parametros["medicamento"]) ? " AND m.medtip= 'M' " : " AND m.medtip= 'I' " ;


        $almacen = $parametros["almacen"];

        $conStock = ($parametros["conStock"]) ? " s.stock>0 " : " s.stock<=0 " ;



        $sql="SELECT
                (m.nombre||'-'||presentacion||'-'||concentracion||'-'||forma_farmaceutica) AS medicamento,
                s.stock,
                m.id_medicamento
            FROM tbl_stocks s
            INNER JOIN tbl_medicamentos m ON m.id_medicamento=s.id_medicamento
            WHERE
            $conStock
            AND s.id_almacen='$almacen'
            AND medsm<>true
            AND medsituacion=true
            $medicamento
            ORDER BY 1";

        $totalResultados = DB::select($sql);

        return $totalResultados;
    }




    public function LotesPorVencer($parametros)
    {

        $risWhere="";
        $almacenWhere="";
        $medicamentoWhere="";
        $fechasWhere="";

        if (isset($parametros["ris"]) && $parametros["ris"]!=null) {
            $ris = $parametros["ris"];
            $risWhere = " AND a.id_ris = '$ris' ";
        }

        if (isset($parametros["almacen"]) && !empty($parametros["almacen"])) {
            $almacen = implode('\',\'',$parametros["almacen"]);
            $almacenWhere = " AND a.id_almacen IN ('$almacen')  ";
        }

        if (isset($parametros["medicamento"]) && $parametros["medicamento"]!=null) {
            $medicamento = $parametros["medicamento"];
            $medicamentoWhere = " AND m.id_medicamento = '$medicamento' ";
        }

        if (isset($parametros["inicio"]) && $parametros["inicio"]!=null && isset($parametros["fin"]) && $parametros["fin"]!=null) {
            $inicio = $parametros["inicio"];
            $fin = $parametros["fin"];
            $fechasWhere = " AND fecha_vto BETWEEN '".$inicio."' and '".$fin."' ";
        }

        $limitSql="";
        if (isset($parametros["cantidad"]) && isset($parametros["pagina"])) {
            $limitSql=" LIMIT ".$parametros["cantidad"]." OFFSET ".$parametros["pagina"]*$parametros["cantidad"];
        }

        $orderSql="";
        if (isset($parametros["campo"]) && isset($parametros["direccion"])) {
            $orderSql=" ORDER BY ".$parametros["campo"]." ".$parametros["direccion"];
        }



        $sql="SELECT
            r.descripcion AS ris,
            a.id_almacen AS codigo_establecimiento,
            a.nombre AS nombre_establecimiento,
            m.id_medicamento codigo_medicamento,
            (m.nombre||'-'||presentacion||'-'||concentracion||'-'||forma_farmaceutica) AS nombre_medicamento,
            d.lote lote,
            d.stock stock,
            d.fecha_vto fecha_vto
        FROM
            tbl_stocks_detalle d
        INNER JOIN
            tbl_almacenes a ON a.id_almacen = d.id_almacen
        LEFT JOIN
            tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla = '1'
        INNER JOIN
            tbl_medicamentos m ON m.id_medicamento = d.id_medicamento
        WHERE
            d.stock>0
            $risWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere

            $orderSql
            $limitSql
            ";

        $totalResultados = DB::select($sql);


        $sql="SELECT
            COUNT(*) AS total
        FROM
            tbl_stocks_detalle d
        INNER JOIN
            tbl_almacenes a ON a.id_almacen = d.id_almacen
        LEFT JOIN
            tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla = '1'
        INNER JOIN
            tbl_medicamentos m ON m.id_medicamento = d.id_medicamento
        WHERE
            d.stock>0
            $risWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere";

        $cantidadTotal = DB::select($sql);

        $total = $cantidadTotal[0]->total;


        return [
            'resultsLength' => $total,
            'data' => $totalResultados
        ];
    }



    public function listDisponibilidadDiaria($id_almacen,$anio,$nmes,$id_tipo,$fecha)
	{
		$sql="SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();";

		ejecutarConsulta($sql);

		//MES DE LA FECHA
		$nmes=substr($fecha,3,2);    //01
		//AÑO DE LA FECHA
		$anio=substr($fecha,6,4);    //2023
		//FORMATEO DE LA FECHA
		$fech=str_replace("/", "-", $fecha);  //17-01-2023
		//FECHA PRIMER DIA DEL MES ACTUAL
		$fec='01-'.$nmes.'-'.$anio; //01-01-2023
		//JUNTA TODA LA FECHA
		$dat=$anio.$nmes.substr($fecha,0,2); //20230117

		//$fec='01-'.$nmes.'-'.$anio;
		//RESTO 11 MESES PARA LA FECHA INICIAL
		$monthIni=date("Ym",strtotime($fec."- 11 month"));
		//RESTO 1 MES PARA LA FECHA FINAL
		$monthEnd=date("Ym",strtotime($fec."- 1 month"));
		//AÑO MES JUNTO
		$monthFin=$anio.$nmes;
		//FECHA INICIAL
		$fi=substr($monthIni,0,4).'-'.substr($monthIni,4,2).'-01';
		//FECHA FINAL
		$ff=$anio."-".$nmes."-01";
		$ff=$anio."-".$nmes."-".date("t", strtotime($ff));

		$sql="SELECT * FROM ici_diario_consulta('$fi'::text, '$ff'::text,$monthIni::int,$monthFin::int,".substr($dat,0,6)."::int,'$id_almacen'::text);";

		return ejecutarConsulta($sql);
	}


}
