<?php
// app/Repositories/LibroDeControlRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\LibroDeControlRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;
use Illuminate\Support\Facades\Auth;
use App\Models\PermisoTotal;
use App\Models\LibroDeControl;
use App\Models\Submodulo;

class LibroDeControlRepository implements LibroDeControlRepositoryInterface
{

    public function libroDeControl($desde,$hasta,$local,$libroDeControl)
    {


        $sql=DB::select("SELECT * FROM consulta_libro_de_control('".$desde->format('Y-m-d H:i:s')."','".$hasta->format('Y-m-d H:i:s')."','$local','$libroDeControl');");
        $consulta_libro_de_control=DB::select($sql[0]->consulta_libro_de_control);

        return $consulta_libro_de_control;
    }



}
