<?php
// app/Repositories/MovimientoRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\MovimientoRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;

class MovimientoRepository implements MovimientoRepositoryInterface
{

    public function listar($parametros)
    {
        $risWhere="";
        $almacenWhere="";
        $medicamentoWhere="";
        $fechasWhere="";

        if (isset($parametros["ris"]) && $parametros["ris"]!=null) {
            $ris = $parametros["ris"];
            $risWhere = " AND a.id_ris = '$ris' ";
        }

        if (isset($parametros["almacen"]) && !empty($parametros["almacen"])) {
            $almacen = implode('\',\'',$parametros["almacen"]);
            $almacenWhere = " AND a.id_almacen IN ('$almacen')  ";
        }

        if (isset($parametros["medicamento"]) && $parametros["medicamento"]!=null) {
            $medicamento = $parametros["medicamento"];
            $medicamentoWhere = " AND me.id_medicamento = '$medicamento' ";
        }

        if (isset($parametros["inicio"]) && $parametros["inicio"]!=null && isset($parametros["fin"]) && $parametros["fin"]!=null) {
            $inicio = $parametros["inicio"];
            $fin = $parametros["fin"];
            $fechasWhere = " AND m.movfechreg BETWEEN '".$inicio."' and '".$fin."' ";
        }

        $orderSql="";
        if (isset($parametros["campo"]) && isset($parametros["direccion"])) {
            if ($parametros["campo"] == "movfechreg") {
                $orderSql=" ORDER BY ".$parametros["campo"]."::timestamp ".$parametros["direccion"];
            }else{
                $orderSql=" ORDER BY ".$parametros["campo"]." ".$parametros["direccion"];
            }
        }

        $limitSql="";
        if (isset($parametros["cantidad"]) && isset($parametros["pagina"])) {
            $limitSql=" LIMIT ".$parametros["cantidad"]." OFFSET ".$parametros["pagina"]*$parametros["cantidad"];
        }

        $sql="SELECT m.movnumero,m.movcoditip,
		to_char(m.movfechreg,'dd-mm-yyyy') as movfechreg,
		to_char(m.movfechreg,'hh:mi AM') as hora,

		a.id_almacen AS \"codigo_establecimiento\",
		m.movrefe,
        m.movsitua,
		r.descripcion AS ris,
		a.nombre AS \"nombre_establecimiento\",
		td.descripcion AS tipodoc,
        m.movnumedco,
        me.id_medicamento AS \"codigo_medicamento\",
        me.nombre||'-'||me.presentacion||'-'||me.concentracion||'-'||me.forma_farmaceutica AS \"nombre_medicamento\" ,
		round(d.movcantid) AS movcantid,
		c.nombre AS cliente,
		sc.subdes,(perapepat||' '||perapemat||' '||pernom) AS medico,
        pres.pctnumeclg,
        m.diagcod,
		ad.nombre AS destino,
		m.usrdescrip
		FROM
		tbl_almacenes a
		INNER JOIN  tbl_movim m ON a.id_almacen = m.id_almacen
		INNER JOIN tbl_movim_detalle d ON m.id_almacen = d.id_almacen AND m.movcoditip = d.movcoditip AND m.movnumero = d.movnumero
		LEFT JOIN tbl_tablas td ON td.id_tipo = m.movtipodco||m.cctcodigo AND td.id_tabla='12'
		LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla='1'
		LEFT JOIN tbl_clientes c ON c.id_cliente = m.clicod AND c.id_almacen = a.id_almacen
		LEFT JOIN tbl_prescrip pres ON pres.pctcod = m.pctcod AND pres.id_almacen = m.id_almacen
		LEFT JOIN tbl_personal per ON pres.percod = per.percod AND pres.id_almacen = per.id_almacen
		LEFT JOIN tbl_almacenes ad  ON ad.id_alterno = m.id_almacen_destino
		LEFT JOIN tbl_medicamentos me ON me.id_medicamento = d.medcod
		LEFT JOIN tbl_subcomponentes sc ON sc.subcodprg = m.progcod
        WHERE
            1 = 1
            $risWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere

            $orderSql
            $limitSql
            ";

        //echo($sql);
        //exit;
        $totalResultados = DB::select($sql);


        $sql="SELECT
            COUNT(*) AS total
            FROM
		tbl_almacenes a
		INNER JOIN  tbl_movim m ON a.id_almacen = m.id_almacen
		INNER JOIN tbl_movim_detalle d ON m.id_almacen = d.id_almacen AND m.movcoditip = d.movcoditip AND m.movnumero = d.movnumero
		LEFT JOIN tbl_tablas td ON td.id_tipo = m.movtipodco||m.cctcodigo AND td.id_tabla='12'
		LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla='1'
		LEFT JOIN tbl_clientes c ON c.id_cliente = m.clicod AND c.id_almacen = a.id_almacen
		LEFT JOIN tbl_prescrip pres ON pres.pctcod = m.pctcod AND pres.id_almacen = m.id_almacen
		LEFT JOIN tbl_personal per ON pres.percod = per.percod AND pres.id_almacen = per.id_almacen
		LEFT JOIN tbl_almacenes ad  ON ad.id_alterno = m.id_almacen_destino
		LEFT JOIN tbl_medicamentos me ON me.id_medicamento = d.medcod
		LEFT JOIN tbl_subcomponentes sc ON sc.subcodprg = m.progcod
        WHERE
            1 = 1
            $risWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere";

        $cantidadTotal = DB::select($sql);

        $total = $cantidadTotal[0]->total;

        return [
            'resultsLength' => $total,
            'data' => $totalResultados
        ];
    }


    public function listar_almacen($parametros)
    {
        $tipoWhere="";
        $almacenWhere="";
        $medicamentoWhere="";
        $fechasWhere="";

        if (isset($parametros["movcoditip"]) && $parametros["movcoditip"]!=null) {
            $tipo = $parametros["movcoditip"];
            $tipoWhere = " AND m.movcoditip = '$tipo' ";
        }

        if (isset($parametros["almacen"]) && !empty($parametros["almacen"])) {
            $almacen = implode('\',\'',$parametros["almacen"]);
            $almacenWhere = " AND a.id_almacen IN ('$almacen')  ";
        }

        if (isset($parametros["medicamento"]) && $parametros["medicamento"]!=null) {
            $medicamento = $parametros["medicamento"];
            $medicamentoWhere = " AND me.id_medicamento = '$medicamento' ";
        }

        if (isset($parametros["inicio"]) && $parametros["inicio"]!=null && isset($parametros["fin"]) && $parametros["fin"]!=null) {
            $inicio = $parametros["inicio"];
            $fin = $parametros["fin"];
            $fechasWhere = " AND m.movfechreg BETWEEN '".$inicio."' and '".$fin."' ";
        }

        $orderSql="";
        if (isset($parametros["campo"]) && isset($parametros["direccion"])) {
            if ($parametros["campo"] == "movfechreg") {
                $orderSql=" ORDER BY ".$parametros["campo"]."::timestamp ".$parametros["direccion"];
            }else{
                $orderSql=" ORDER BY ".$parametros["campo"]." ".$parametros["direccion"];
            }

        }

        $limitSql="";
        if (isset($parametros["cantidad"]) && isset($parametros["pagina"])) {
            $limitSql=" LIMIT ".$parametros["cantidad"]." OFFSET ".$parametros["pagina"]*$parametros["cantidad"];
        }

        $sql="SELECT m.movnumero,m.movcoditip,
		to_char(m.movfechreg,'dd-mm-yyyy') AS movfechreg,
		to_char(m.movfechreg,'hh:mi AM') AS hora,
		a.id_almacen AS \"codigo_establecimiento\",
		m.movrefe AS movrefe,
        m.movsitua AS movsitua,
		r.descripcion AS ris,
		a.nombre AS \"nombre_establecimiento\",
		td.descripcion AS tipodoc,
        m.movnumedco AS movnumedco,
        me.id_medicamento AS \"codigo_medicamento\",
        me.nombre||'-'||me.presentacion||'-'||me.concentracion||'-'||me.forma_farmaceutica AS \"nombre_medicamento\" ,
		round(d.movcantid) AS movcantid,
		c.nombre AS cliente,
		sc.subdes,(perapepat||' '||perapemat||' '||pernom) AS medico,
        pres.pctnumeclg AS pctnumeclg,
        m.diagcod AS diagcod,
		ad.nombre AS destino,
		m.usrdescrip AS usrdescrip
		FROM
		tbl_almacenes a
		INNER JOIN  tbl_movim m ON a.id_almacen = m.id_almacen
		INNER JOIN tbl_movim_detalle d ON m.id_almacen = d.id_almacen AND m.movcoditip = d.movcoditip AND m.movnumero = d.movnumero
		LEFT JOIN tbl_tablas td ON td.id_tipo = m.movtipodco||m.cctcodigo AND td.id_tabla='10'
		LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla='1'
		LEFT JOIN tbl_clientes c ON c.id_cliente = m.clicod AND c.id_almacen = a.id_almacen
		LEFT JOIN tbl_prescrip pres ON pres.pctcod = m.pctcod AND pres.id_almacen = m.id_almacen
		LEFT JOIN tbl_personal per ON pres.percod = per.percod AND pres.id_almacen = per.id_almacen
		LEFT JOIN tbl_almacenes ad  ON ad.id_alterno = m.id_almacen_destino
		LEFT JOIN tbl_medicamentos me ON me.id_medicamento = d.medcod
		LEFT JOIN tbl_subcomponentes sc ON sc.subcodprg = m.progcod
        WHERE
            1 = 1
            $tipoWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere

            $orderSql
            $limitSql
            ";

        $totalResultados = DB::select($sql);



        $sql="SELECT
            COUNT(*) AS total
            FROM
		tbl_almacenes a
		INNER JOIN  tbl_movim m ON a.id_almacen = m.id_almacen
		INNER JOIN tbl_movim_detalle d ON m.id_almacen = d.id_almacen AND m.movcoditip = d.movcoditip AND m.movnumero = d.movnumero
		LEFT JOIN tbl_tablas td ON td.id_tipo = m.movtipodco||m.cctcodigo AND td.id_tabla='10'
		LEFT JOIN tbl_tablas r ON r.id_tipo = a.id_ris AND r.id_tabla='1'
		LEFT JOIN tbl_clientes c ON c.id_cliente = m.clicod AND c.id_almacen = a.id_almacen
		LEFT JOIN tbl_prescrip pres ON pres.pctcod = m.pctcod AND pres.id_almacen = m.id_almacen
		LEFT JOIN tbl_personal per ON pres.percod = per.percod AND pres.id_almacen = per.id_almacen
		LEFT JOIN tbl_almacenes ad  ON ad.id_alterno = m.id_almacen_destino
		LEFT JOIN tbl_medicamentos me ON me.id_medicamento = d.medcod
		LEFT JOIN tbl_subcomponentes sc ON sc.subcodprg = m.progcod
        WHERE
            1 = 1
            $tipoWhere
            $almacenWhere
            $medicamentoWhere
            $fechasWhere";

        $cantidadTotal = DB::select($sql);

        $total = $cantidadTotal[0]->total;

        return [
            'resultsLength' => $total,
            'data' => $totalResultados
        ];
    }


}
