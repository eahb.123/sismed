<?php
// app/Repositories/ModuloRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\ModuloRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Modulo;


class ModuloRepository implements ModuloRepositoryInterface
{

    public function ContinuaOrden(){
        $sp = Modulo::max('orden');
        return $sp+1;
    }

    public function crear($modulo,$id_usuario){

        $orden = $this->ContinuaOrden();

        $sp = Modulo::create([
            'id_supermodulo' => $modulo["id_supermodulo"],
            'nombre'=> $modulo["nombre"],
            'descripcion'=> $modulo["descripcion"],
            'ruta'=> $modulo["ruta"],
            'fecha_creacion'=> now(),
            'usuario_creacion'=>$id_usuario,
            'orden'=> $orden
        ]);

        $supermodulo = $sp;
        return $supermodulo;
    }

    public function listar($parametros)
    {


        $modulos = Modulo::where("eliminado",false);

        if (isset($parametros["selectedSupermodulo"]) )  {
            $modulos = $modulos->where("id_supermodulo",$parametros["selectedSupermodulo"]);
        }

        if (isset($parametros["selectedEstado"]) )  {
            $modulos = $modulos->where("estado",$parametros["selectedEstado"]);
        }

        $modulos = $modulos->with('supermodulo');
        $modulosrs = $modulos->get();
        return $modulosrs;
    }

    public function buscarPorSprmodulo($id_supermodulo)
    {
        $modulos = Modulo::
        where("id_supermodulo","=",$id_supermodulo)
        ->get();
        return $modulos;
    }



    public function eliminar($id,$id_usuario){

        $modulo = Modulo::findOrFail($id);


        $modulo->eliminado = true;
        $modulo->fecha_elimino = now();
        $modulo->usuario_elimino = $id_usuario;

        $rsp=$modulo->save();

        return $rsp;
    }

    public function actualizar($id,$modulo,$id_usuario){
        $modulors = Modulo::findOrFail($id);

        if ($modulors) {
            $modulors->id_supermodulo = $modulo["id_supermodulo"];
            $modulors->nombre = $modulo["nombre"];
            $modulors->descripcion = $modulo["descripcion"];
            $modulors->ruta = $modulo["ruta"];
            $modulors->fecha_modificacion = now();
            $modulors->usuario_modificacion = $id_usuario;
            $modulors->save();
            return $modulors;
        }else{
            return null;
        }
    }

    public function lista_modulo_combo(){
        $modulors = Modulo::findOrFail($id);

        if ($modulors) {
            $modulors->id_supermodulo = $modulo["id_supermodulo"];
            $modulors->nombre = $modulo["nombre"];
            $modulors->descripcion = $modulo["descripcion"];
            $modulors->ruta = $modulo["ruta"];
            $modulors->fecha_modificacion = now();
            $modulors->usuario_modificacion = $id_usuario;
            $modulors->save();
            return $modulors;
        }else{
            return null;
        }
    }

}
