<?php
// app/Repositories/SupermoduloRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Supermodulo;
use App\Repositories\Contracts\SupermoduloRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class SupermoduloRepository implements SupermoduloRepositoryInterface
{

    public function listar()
    {
        $supermodulo = Supermodulo::
        where('eliminado','=',false)
        ->orderBy('orden', 'asc')->get();
        return $supermodulo;
    }

    public function reordenar($supermodulos)
    {

        $i=0;
        foreach ($supermodulos as $supermodulo) {
            $i = $i + 1;
            $sp = Supermodulo::findOrFail($supermodulo['id_supermodulo']);

            $sp->update([
                'orden' => $i
            ]);
        }

        $supermodulo = $this->listar();
        return $supermodulo;
    }

    public function cambiarEstado($supermodulo){

        $sp = Supermodulo::findOrFail($supermodulo['id_supermodulo']);


        $sp->estado = !$sp->estado; // Asigna el nuevo valor al campo 'estado'
        $sp->save();

        $supermodulo = $sp;
        return $supermodulo;
    }

    public function ContinuaOrden(){


        $sp = Supermodulo::max('orden');

        return $sp+1;
    }

    public function crear($supermodulo){
        $orden = $this->ContinuaOrden();


        $sp = Supermodulo::create([
            'nombre'=> $supermodulo["nombre"],
            'descripcion'=> $supermodulo["descripcion"],
            'ruta'=> $supermodulo["ruta"],
            'orden'=> $orden
        ]);

        $supermodulo = $sp;
        return $supermodulo;
    }

    public function eliminar($id,$id_usuario){

        $supermodulo = Supermodulo::findOrFail($id);


        $supermodulo->eliminado = true;
        $supermodulo->fecha_elimino = now(); // Establecer la fecha de eliminación
        $supermodulo->usuario_elimino = $id_usuario; // Establecer el ID del usuario que eliminó

        $rsp=$supermodulo->save();

        return $rsp;
    }

    public function actualizar($id,$supermodulo,$id_usuario){
        $supermodulors = Supermodulo::findOrFail($id);
        if ($supermodulors) {
            $supermodulors->nombre = $supermodulo["nombre"];
            $supermodulors->descripcion = $supermodulo["descripcion"];
            $supermodulors->ruta = $supermodulo["ruta"];
            $supermodulors->fecha_modificacion = now();
            $supermodulors->usuario_modificacion = $id_usuario;
            $supermodulors->save();
            return $supermodulors;
        }else{
            return null;
        }
    }

    public function listarSupermoduloCombo(){

        $supermodulo = Supermodulo::
        where('eliminado','=',false)
        ->orderBy('orden', 'asc')->get();
        return $supermodulo;
    }



}
