<?php
// app/Repositories/EmpresaRepository.php

namespace App\Repositories;

use App\Models\Tbl_empresas;
use App\Repositories\Contracts\EmpresaRepositoryInterface;

class EmpresaRepository implements EmpresaRepositoryInterface
{
    public function all()
    {
        return Tbl_empresas::all();
    }

    public function find($id)
    {
        return Tbl_empresas::find($id);
    }

    public function create($data)
    {
        return Tbl_empresas::create($data);
    }

    public function update($id, $data)
    {
        $empresa = Tbl_empresas::find($id);
        $empresa->update($data);
        return $empresa;
    }

    public function delete($id)
    {
        $empresa = Tbl_empresas::find($id);
        $empresa->delete();
    }
}
