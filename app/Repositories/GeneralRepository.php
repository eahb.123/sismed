<?php
// app/Repositories/GeneralRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\GeneralRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class GeneralRepository implements GeneralRepositoryInterface
{

    public function listarEstrategia($parametros)
    {
        $resultado = DB::select("SELECT * FROM tbl_progr_estrat ORDER BY nombre ASC");

        return $resultado;
    }

}
