<?php
// app/Repositories/SesionRepository.php

namespace App\Repositories;
use JWTAuth;
use App\Models\Sesion;
use App\Models\PermisoTotal;
use App\Models\Supermodulo;
use App\Models\Modulo;
use App\Models\Submodulo;
use App\Repositories\Contracts\SesionRepositoryInterface;

class SesionRepository implements SesionRepositoryInterface
{
    public function all()
    {
        return Sesion::all();
    }

    public function find($id)
    {
        return Sesion::find($id);
    }

    public function create($usuario,$token)
    {

        $tokenInfo = JWTAuth::getPayload($token)->toArray();

        $sesion = [
            "id_usuario" => $usuario->id_usuario,
            "token_sesion" => $token,
            "fecha_inicio"=>$tokenInfo["iat"],
            "fecha_expiracion"=>$tokenInfo["exp"]
        ];

        //protected $fillable = ['nombre', 'password', 'correo_electronico', 'fecha_registro', 'id_cargo', 'created_at', 'updated_at'];

        //protected $fillable = ['id_usuario', 'token_sesion', 'fecha_inicio', 'fecha_expiracion', 'created_at', 'updated_at'];
        $sesionCreada = Sesion::create($sesion);
        return response()->json(['sesion' => $sesionCreada, 'message' => 'Usuario creado con éxito']);

    }

    public function validar($usuario,$token,$payload,$route)
    {

        $permisoTotal = PermisoTotal::
        where('id_usuario','=',$usuario->id_usuario)
        ->where('estado','=', true)
        ->where('eliminado','=', false);




        if ($permisoTotal->count()==0) {
            return response()->json(['error' => 'Error en el permiso','success'=>'false','code'=>'401']);

        }

        $now = now()->timestamp;

        $sesion = Sesion::
        where('id_usuario','=',$usuario->id_usuario)
        ->where('token_sesion','=', $token)
        ->where('fecha_inicio','<=', $now)
        ->where('fecha_expiracion','>=',$now)
        ->count();


        if ($sesion==0) {
            return response()->json(['error' => 'Error en la sesion','success'=>'false','code'=>'405']);
        }

        $segments = explode('/', $route);
        $supermoduloRuta = $segments[2] ?? null;
        $moduloRuta = $segments[3] ?? null;
        $submoduloRuta = $segments[4] ?? null;
        $permisoRuta = $segments[5] ?? null;


        $supermodulo = Supermodulo::
        where('ruta','=', $supermoduloRuta)
        ->where('estado','=', true)
        ->where('eliminado','=', false)
        ;
        if ($supermodulo->count()==0) {
            return response()->json(['error' => 'Error de acceso en el supermodulo','success'=>'false','code'=>'401']);
        }
        //$supermodulo=$supermodulo->first();


        $modulo = Modulo::
        where('ruta','=', $moduloRuta)
        ->where('estado','=', true)
        ->where('eliminado','=', false)
        ->where('id_supermodulo',"=",$supermodulo->first()->id_supermodulo);

        if ($modulo->count()==0) {
            return response()->json(['error' => 'Error de acceso en el modulo','success'=>'false','code'=>'401']);
        }

        $submodulo = Submodulo::
        where('ruta','=', $submoduloRuta)
        ->where('estado','=', true)
        ->where('eliminado','=', false)
        ->where('id_modulo',"=",$modulo->first()->id_modulo);
        ;

        if ($submodulo->count()==0) {
            return response()->json(['error' => 'Error de acceso en el submodulo','success'=>'false','code'=>'401']);
        }



        $permisoTotal = PermisoTotal::
        where('id_usuario','=',$usuario->id_usuario)
        ->where('id_supermodulo','=',$supermodulo->first()->id_supermodulo)
        ->where('id_modulo','=',$modulo->first()->id_modulo)
        ->where('id_submodulo','=',$submodulo->first()->id_submodulo)
        ->where('estado','=', true)
        ->where('eliminado','=', false);

        if ($permisoTotal->count()==0) {
            return response()->json(['error' => 'Error en el permiso','success'=>'false','code'=>'401']);

        }

        return response()->json(['error' => '','success'=>'true','code'=>'200']);
    }


    public function update($id, $data)
    {
        $empresa = Sesion::find($id);
        $empresa->update($data);
        return $empresa;
    }

    public function delete($id)
    {
        $empresa = Sesion::find($id);
        $empresa->delete();
    }
}
