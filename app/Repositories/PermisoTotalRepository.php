<?php
// app/Repositories/PermisoTotalRepository.php

namespace App\Repositories;
use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\PermisoTotalRepositoryInterface;
use Illuminate\Support\Facades\DB;
//use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\PermisoTotal;
use App\Models\Supermodulo;
use App\Models\Submodulo;
use App\Models\Modulo;
use App\Models\Permiso;
use Illuminate\Support\Facades\Auth;


class PermisoTotalRepository implements PermisoTotalRepositoryInterface
{

    public function buscarPorUsuario($id_usuario,$parametros){

        $permisoTotal = PermisoTotal::where("id_usuario","=",$id_usuario)
        ->where("eliminado",false);

        if (isset($parametros["selectedAlmacen"]) && is_array($parametros["selectedAlmacen"]) && count($parametros["selectedAlmacen"]) > 0)  {
            $permisoTotal = $permisoTotal->whereIn("id_almacen",$parametros["selectedAlmacen"]);
        }

        if (isset($parametros["selectedSupermodulo"]) )  {
            $permisoTotal = $permisoTotal->where("id_supermodulo",$parametros["selectedSupermodulo"]);
        }

        if (isset($parametros["selectedModulo"]) )  {
            $permisoTotal = $permisoTotal->where("id_modulo",$parametros["selectedModulo"]);
        }

        if (isset($parametros["selectedSubmodulo"]) )  {
            $permisoTotal = $permisoTotal->where("id_submodulo",$parametros["selectedSubmodulo"]);
        }

        if (isset($parametros["selectedEstado"]) )  {
            $permisoTotal = $permisoTotal->where("estado",$parametros["selectedEstado"]);
        }

        if (isset($parametros["selectedPermiso"]) && is_array($parametros["selectedPermiso"]) && count($parametros["selectedPermiso"]) > 0)  {
            $permisoTotal = $permisoTotal->whereIn("id_permiso",$parametros["selectedPermiso"]);
        }


        $permisoTotal = $permisoTotal
        ->with('almacen','usuario','supermodulo','modulo','submodulo','permiso')
        ->orderBy('id_permiso_total','desc')
        ->get();
        //->with('almacen','usuario','supermodulo','modulo','submodulo','permiso')
        //->get();
        //($permisoTotal);
        return $permisoTotal;
    }

    public function crear($permisoTotal,$id_usuario){



        $almacenesArr = $permisoTotal["almacen"];
        $supermodulo =  $permisoTotal["supermodulo"];



        $modulo =  explode(",", $permisoTotal["modulo"]);


        if ($modulo[0] == '') {
            $modulo = Modulo::where('id_supermodulo', '=', $supermodulo)->pluck('id_modulo');
        }



        $submodulo = explode(",", $permisoTotal["submodulo"]);

        if ($submodulo[0] == '') {
            $submodulo = Submodulo::whereIn('id_modulo',$modulo)->pluck('id_submodulo');
        }



        $permisosArr =  $permisoTotal["permiso"];

        if ($permisosArr == null) {
            $permisosArr = Permiso::whereIn('id_submodulo',$submodulo)->pluck('id_permiso');
        }






        $contadorCreado = 0;
        $contadorExiste = 0;
        $contadorTotal = 0;

        foreach ($almacenesArr as $keyalm => $valuealm) {

            foreach ($permisosArr as $keyPer => $valuePer) {




                if ($valuealm!=0 && $valuePer!=0) {

                    $permiso_q = Permiso::where('id_permiso','=',$valuePer)->first();

                    $submodulo_q = Submodulo::where('id_submodulo','=',$permiso_q->id_submodulo)->first();

                    $modulo_q = Modulo::where('id_modulo','=',$submodulo_q->id_modulo)->first();

                    $supermodulo_q = Supermodulo::where('id_supermodulo','=',$modulo_q->id_supermodulo)->first();


                    $existe = PermisoTotal::
                    where('id_supermodulo','=',$supermodulo_q->id_supermodulo)
                    ->where('id_modulo','=',$modulo_q->id_modulo)
                    ->where('id_submodulo','=',$submodulo_q->id_submodulo)
                    ->where('id_permiso','=',$valuePer)
                    ->where('id_usuario','=',$id_usuario)
                    ->where('eliminado','=',false);
                    $smAlm = Submodulo::where('id_submodulo','=',$submodulo_q->id_submodulo)->where('almacenes','=',true)->count();

                    if ($smAlm>0) {
                        $existe=$existe->where('id_almacen','=',$valuealm);
                        $valorAlmacen = $valuealm;
                    }else{
                        $valorAlmacen = null;
                    }


                    $existe = $existe->first();

                    if (!!!$existe) {
                        $permisototalcreado = PermisoTotal::create([
                            'id_almacen'=> $valorAlmacen,
                            'id_supermodulo'=> $supermodulo_q->id_supermodulo,
                            'id_modulo'=> $modulo_q->id_modulo,
                            'id_submodulo'=> $permiso_q->id_submodulo,
                            'id_permiso'=> $valuePer,
                            'id_usuario'=> $id_usuario,
                            'fecha_creacion'=> now(),
                            'usuario_creacion'=> $id_usuario,
                            'eliminado' => false,
                            'estado' => true
                        ]);
                        $contadorCreado++;
                    }else{
                        $existe->update([
                            'eliminado' => false,
                            'estado' => true
                        ]);
                        $contadorExiste++;
                    }
                    $contadorTotal++;
                }

            }
        }

        return json_encode([
            'creados' => $contadorCreado,
            'existentes' => $contadorExiste,
            'total' => $contadorTotal
        ]);
    }

    public function eliminarPermisosTotales($arrPermisototalIds){
        $permisosTotalesEliminados = PermisoTotal::whereIn('id_permiso_total',$arrPermisototalIds)->update(['eliminado' => true]);
        return $permisosTotalesEliminados;
    }

    public function activarPermisosTotales($arrPermisototalIds){
        $permisosTotalesEliminados = PermisoTotal::whereIn('id_permiso_total',$arrPermisototalIds)->update(['estado' => true]);
        return $permisosTotalesEliminados;
    }

    public function desactivarPermisosTotales($arrPermisototalIds){
        $permisosTotalesEliminados = PermisoTotal::whereIn('id_permiso_total',$arrPermisototalIds)->update(['estado' => false]);
        return $permisosTotalesEliminados;
    }

}
