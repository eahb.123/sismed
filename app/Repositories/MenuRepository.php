<?php
// app/Repositories/SesionRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\MenuRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class MenuRepository implements MenuRepositoryInterface
{

    public function listar($usuario)
    {

        //$tokenInfo = JWTAuth::getPayload($token)->toArray();
        //$payload = JWTAuth::parseToken($token)->getPayload()->toArray();
        $resultados = DB::table('permiso_total as pt')
            ->join('supermodulos as spm', 'pt.id_supermodulo', '=', 'spm.id_supermodulo')
            ->join('modulos as m', 'pt.id_modulo', '=', 'm.id_modulo')
            ->join('submodulos as sbm', 'pt.id_submodulo', '=', 'sbm.id_submodulo')
            ->join('permisos as p', 'pt.id_permiso', '=', 'p.id_permiso')
            ->select(
                'spm.nombre as supermodulo',
                'm.descripcion as modulo',
                'sbm.descripcion as submodulo',
                'sbm.id_submodulo as id_submodulo',
                DB::raw("spm.ruta || '/' || m.ruta as ruta_modulo"),
                DB::raw("spm.ruta || '/' || m.ruta || '/' || sbm.ruta || '/listar' as ruta")
            )
            ->where(function ($query) use ($usuario) {
                $query->where('spm.estado', true)
                    ->where('spm.eliminado', false)
                    ->where('m.estado', true)
                    ->where('m.eliminado', false)
                    ->where('sbm.estado', true)
                    ->where('sbm.eliminado', false)
                    ->where('p.estado', true)
                    ->where('p.eliminado', false);

                if ($usuario->correo != 'admin@admin.com') {
                    $query->where('pt.id_usuario', '=', $usuario->id_usuario)
                        ->where('pt.estado', true)
                        ->where('pt.eliminado', false);
                }
            })
            ->groupBy(
                'spm.orden',
                'spm.nombre',
                'm.descripcion',
                'm.orden',
                'sbm.id_submodulo',
                'sbm.descripcion',
                'sbm.orden',
                DB::raw("spm.ruta || '/' || m.ruta || '/' || sbm.ruta || '/listar'"),
                DB::raw("spm.ruta || '/' || m.ruta")
            )
            ->orderBy('spm.orden')
            ->orderBy('m.orden')
            ->orderBy('sbm.orden')
            ->get();



        return $resultados;
        $sesionCreada = Sesion::create($sesion);
        return response()->json(['sesion' => $sesionCreada, 'message' => 'Usuario creado con éxito']);

    }


}
