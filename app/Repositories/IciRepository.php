<?php
// app/Repositories/IciRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\IciRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;
use Illuminate\Support\Facades\Auth;
use App\Models\PermisoTotal;
use App\Models\Ici;
use App\Models\Submodulo;

class IciRepository implements IciRepositoryInterface
{

    public function ici_diario($parametros)
    {
        //$sql="SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();";

        $sql=DB::select("SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();");

        $array = $parametros["arrAlmacen"];

        if (count($array)!=0) {
            $cadena = implode("'',''", $array);
            $cadena = "'''" . $cadena . "'''";
        }else{
            $cadena = "''";
        }
        //($cadena);
        $sql=DB::select("SELECT * FROM ici_diario_nuevo_consulta('".$parametros["date"]."',$cadena);");
        $sql=DB::select($sql[0]->ici_diario_nuevo_consulta);
        return $sql;

    }

    public function ici_estrategico($parametros)
    {
        //$sql="SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();";

        $sql=DB::select("SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();");

        $array = $parametros["arrEstrategia"];
        if (count($array)!=0) {
            $cadena = implode("'',''", $array);
            $cadena = "'''" . $cadena . "'''";
        }else{
            $cadena = "''";
        }
        $sql=DB::select("SELECT * FROM ici_progr_estrategico_nuevo_consulta('".$parametros["date"]."',$cadena);");

        $sql=DB::select($sql[0]->ici_progr_estrategico_nuevo_consulta);

        return $sql;

    }

    public function ici_por_producto($parametros)
    {
        //$sql="SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();";

        $sql=DB::select("SELECT * FROM ici_diario_tabla_temporal_medicamento_recursivo();");


        $producto = $parametros["arrMedicamento"];
        $sql=DB::select("SELECT * FROM ici_por_producto_nuevo_consulta('".$parametros["date"]."','$producto');");
        $sql=DB::select($sql[0]->ici_por_producto_nuevo_consulta);


        return $sql;

    }



}
