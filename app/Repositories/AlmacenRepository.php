<?php
// app/Repositories/AlmacenRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\AlmacenRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;
use Illuminate\Support\Facades\Auth;
use App\Models\PermisoTotal;
use App\Models\Submodulo;

class AlmacenRepository implements AlmacenRepositoryInterface
{

    public function listar($parametros)
    {
        $usuario = Auth::user();


        $almacenes=Almacen::query();
        if (isset($parametros["ris"])) {
            $almacenes = $almacenes->where("id_ris",$parametros["ris"]);

        }
        if (isset($parametros["tipo"])) {
            $almacenes = $almacenes->where("tipo",$parametros["tipo"]);
        }

        if (isset($parametros["search"])) {
            $search = strtoupper($parametros["search"]);
            $almacenes = $almacenes->where('nombre', 'like', "%$search%")
            ->orWhere('id_almacen', 'like', "%$search%");
        }

        if (isset($parametros["filtraUsuario"]) && $parametros["filtraUsuario"] && isset($parametros["nombreSubmodulo"])) {

            //TRAER TODOS LOS SUBMODULOS
            $submodulo = Submodulo::where("ruta",$parametros["nombreSubmodulo"])
                                    ->where("estado",true)
                                    ->where("eliminado",false)
                                    ->first();



            //VER QUE PERMISOS TIENE EN ESE SUBMODULO
            $almacenesId = PermisoTotal::where("estado",true);
            $almacenesId = $almacenesId->where("eliminado",false);
            $almacenesId = $almacenesId->where("id_usuario",$usuario->id_usuario);
            $almacenesId = $almacenesId->where("id_submodulo",$submodulo->id_submodulo);
            $almacenesId = $almacenesId->pluck('id_almacen')
            ->toArray();

            //RECUPERAR TODOS LOS ALMACENES
            $almacenes = $almacenes->whereIn('id_almacen',$almacenesId);



        }
        $almacenes = $almacenes->where('estado', true);
        $almacenes = $almacenes->orderBy('nombre', 'asc');
        $almacenes = $almacenes->get();

        return $almacenes;
    }

    public function listarRis() {
        $ris = Ris::WHERE("id_tabla",1)->get();
        return $ris;
    }

    public function listarFiltrado($parametros,$usuario)
    {

        $almacenes=Almacen::query();
        if (isset($parametros["ris"])) {
            $almacenes = $almacenes->where("id_ris",$parametros["ris"]);
        }

        if (isset($parametros["tipo"])) {
            $almacenes = $almacenes->where("tipo",$parametros["tipo"]);
        }
        $almacenes = $almacenes->where('estado', true);
        $almacenes = $almacenes->get();

        return $almacenes;
    }

}
