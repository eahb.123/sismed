<?php
// app/Repositories/PermisoRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\PermisoRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Permiso;


class PermisoRepository implements PermisoRepositoryInterface
{

    public function buscarPorSubmodulo($id_submodulo)
    {
        $permisos = Permiso::
        where("id_submodulo","=",$id_submodulo)
        ->get();
        return $permisos;
    }

    public function listar($parametros){


        $permisos = Permiso::where("eliminado",false);


        if (isset($parametros["selectedSupermodulo"])) {
            $permisos = $permisos->whereHas('submodulo.modulo.supermodulo', function ($query) use ($parametros) {
                $query->where("id_supermodulo", $parametros["selectedSupermodulo"]);
            });
        }

        if (isset($parametros["selectedModulo"])) {
            $permisos = $permisos->whereHas('submodulo.modulo', function ($query) use ($parametros) {
                $query->where("id_modulo", $parametros["selectedModulo"]);
            });
        }

        if (isset($parametros["selectedSubmodulo"]) )  {
            $permisos = $permisos->where("id_submodulo",$parametros["selectedSubmodulo"]);
        }

        if (isset($parametros["selectedEstado"]) )  {
            $permisos = $permisos->where("estado",$parametros["selectedEstado"]);
        }

        $permisos = $permisos->with('submodulo');
        $permisos = $permisos->with('submodulo.modulo');
        $permisos = $permisos->with('submodulo.modulo.supermodulo');
        $permisos = $permisos->get();
        return $permisos;
    }



    public function crear($permiso,$id_usuario){

        //$orden = $this->ContinuaOrden();

        $perm = Permiso::create([
            'id_submodulo' => $permiso["id_submodulo"],
            'nombre'=> $permiso["nombre"],
            'descripcion'=> $permiso["descripcion"],
            'ruta'=> $permiso["ruta"],
            'fecha_creacion' => now(),
            'usuario_creacion' => $id_usuario,
            //'orden'=> $orden
        ]);
        return $perm;
    }

    public function eliminar($id,$id_usuario){

        $permiso = Permiso::findOrFail($id);

        if (!!$permiso) {
            $permiso->eliminado = true;
            $permiso->fecha_elimino = now();
            $permiso->usuario_elimino = $id_usuario;

            $rsp=$permiso->save();
            return $rsp;
        }else{
            return false;
        }

    }

    public function actualizar($id,$permiso,$id_usuario){
        $perm = Permiso::findOrFail($id);

        if ($perm) {
            $perm->id_submodulo = $permiso["id_submodulo"];
            $perm->nombre = $permiso["nombre"];
            $perm->descripcion = $permiso["descripcion"];
            $perm->ruta = $permiso["ruta"];
            $perm->fecha_modificacion = now();
            $perm->usuario_modificacion = $id_usuario;
            $perm->save();
            return $perm;

        }else{
            return null;
        }
    }


}
