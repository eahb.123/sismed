<?php
// app/Repositories/MaestraCategoriaRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\MaestraCategoriaRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Almacen;
use App\Models\Ris;
use App\Models\MaestraCategoria;

class MaestraCategoriaRepository implements MaestraCategoriaRepositoryInterface
{

    public function listar($parametros)
    {

        $tipoWhere="";

        $maestraCategoria=MaestraCategoria::query();

        if (isset($parametros["tipo"]) && $parametros["tipo"]!=null) {
            $tipo = $parametros["tipo"];
            $maestraCategoria=$maestraCategoria
            ->where('tipo',$tipo);
        }
        $maestraCategoria=$maestraCategoria
        ->with('maestraCategoriaMedicamento')
        ->get();

        //$almacenes=MaestraCategoria::query();

        //$almacenes = $almacenes->get();

        return $maestraCategoria;
    }

    public function consultaCategoriaMaestraMedicamento($categoria,$ris){
        $resultado = DB::select("SELECT * FROM consulta_stock_categoria_maestra(?, ?) as query", [$categoria, $ris]);
        //$resultado = DB::select('SELECT * FROM consulta_stock_categoria_maestra(?, ?)', ['1', '1']);
        $query = $resultado[0]->query;
        $resultado = DB::select($query);
        return $resultado;
    }

    public function lista_documento_identidad_combo(){

        $resultado = DB::select("SELECT id_tipo FROM tbl_tablas WHERE abreviado = 'TIP_DOC_IDENT'");
        $query = $resultado[0]->id_tipo;

        $resultado = DB::select("SELECT * FROM tbl_tablas WHERE id_tabla =  ?",[$query]);
        return $resultado;
    }

    public function listar_kit_escenario_iii(){
        $resultado = DB::select("SELECT * FROM kit_prevencion_transmision_materno_infantil() AS query");
        //$resultado = DB::select('SELECT * FROM consulta_stock_categoria_maestra(?, ?)', ['1', '1']);
        $query = $resultado[0]->query;
        $resultado = DB::select($query);
        return $resultado;
    }
}
