<?php
// app/Repositories/UsuarioRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\UsuarioRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Usuario;
use App\Models\Persona;
use Illuminate\Support\Facades\Auth;

class UsuarioRepository implements UsuarioRepositoryInterface
{

    public function listar()
    {
        $usuarios = Usuario::
        where('eliminado','=',false)
        ->with('persona')
        ->get();
        return $usuarios;
    }

    public function crear($usuario,$id_usuario)
    {
        $persona = Persona::where('numero_documento','=',$usuario["numero_documento"])
        ->orWhere('email','=',$usuario["correo_electronico"])
        ->first();


        if ($persona == null) {
            $persona = Persona::create([
                'nombres'=> $usuario["nombres"],
                'ape_paterno'=> $usuario["ape_paterno"],
                'ape_materno'=> $usuario["ape_materno"],
                'id_tipo_documento'=> $usuario["id_tipo_documento"],
                'numero_documento'=> $usuario["numero_documento"],
                'sexo'=> isset($usuario['sexo']) && !empty($usuario['sexo']) ? $usuario['sexo'] : null,
                'fecha_nac'=> isset($usuario['fecha_nac']) && !empty($usuario['fecha_nac']) ? $usuario['fecha_nac'] : null,
                'correo_electronico'=> $usuario["correo_electronico"],
                'celular'=> isset($usuario['celular']) && !empty($usuario['celular']) ? $usuario['celular'] : null,
                'direccion'=> isset($usuario['direccion']) && !empty($usuario['direccion']) ? $usuario['direccion'] : null
            ]);
        }

        $usuarioCreado = Usuario::where('id_persona','=',$persona->id)->first();

        if ($usuarioCreado == null) {
            $usuarioCreado = Usuario::create([
                'correo_electronico'=> $usuario["correo_electronico"],
                'password'=> bcrypt($usuario["password"]),
                'id_persona'=> $persona->id,
                'imagen_perfil' => 'imagenes_perfil/default-profile.jpg',
                'usuario_creacion' => $id_usuario,
                'fecha_creacion' => now()
            ]);
        }

        return $usuarioCreado;
    }

    public function buscar($id_usuario){
        if ($id_usuario == 0) {
            $usuario = Auth::user();
            $id_usuario=$usuario->id_usuario;
        }
        $usuario = Usuario::where('id_usuario','=',$id_usuario)->with('persona')->first();

        return $usuario;
    }

    public function editar($id,$usuario,$id_login){

        $usuariors = Usuario::findOrFail($id);

        if ($usuariors) {
            $usuariors->correo_electronico = $usuario["correo_electronico"];
            if (!empty($usuario["password"]) && $usuario["password"] != "") {
                $usuariors->password = bcrypt($usuario["password"]);
            }
            $usuariors->fecha_modificacion = now();
            $usuariors->usuario_modificacion = $id_login;
            $usuariors->save();
        }else {
            return null;
        }

        $personars = Persona::findOrFail($usuariors->id_persona);



        if ($personars) {
            $personars->nombres = $usuario["nombres"];
            $personars->ape_paterno = $usuario["ape_paterno"];
            $personars->ape_materno = $usuario["ape_materno"];
            $personars->id_tipo_documento = $usuario["id_tipo_documento"];
            $personars->numero_documento = $usuario["numero_documento"];
            $personars->sexo = $usuario["sexo"];
            $personars->fecha_nac = isset($usuario['fecha_nac']) && !empty($usuario['fecha_nac']) ? $usuario['fecha_nac'] : null;
            $personars->email = $usuario["correo_electronico"];
            $personars->celular = $usuario["celular"];
            $personars->direccion = $usuario["direccion"];
            $resp=$personars->save();
            return $usuariors;
        }else{
            return null;
        }
    }

    public function buscarme(){

        $usuario = Auth::user();

        $id_usuario=$usuario->id_usuario;

        $usuario = Usuario::where('id_usuario','=',$id_usuario)->with('persona')->first();

        return $usuario;

    }


    public function actualizarme($usuario,$request){


        $id = Auth::user()->id_usuario;


        $usuariors = Usuario::findOrFail($id);


        if ($usuariors) {
            $usuariors->correo_electronico = $usuario["correo_electronico"];

            // Actualizar la contraseña si se proporciona
            if (!empty($usuario["password"]) && $usuario["password"] != "") {
                $usuariors->password = bcrypt($usuario["password"]);
            }

            // Manejar la carga de la imagen de perfil
            if ($request->hasFile('imagen_perfil')) {
                $file = $request->file('imagen_perfil');
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $file->storeAs('imagenes_perfil', $filename, 'public');
                $usuariors->imagen_perfil = $filePath;
            } else {
                // Usar la imagen de perfil por defecto si no se carga una nueva
                $usuariors->imagen_perfil = 'imagenes_perfil/default-profile.jpg';
            }

            $usuariors->fecha_modificacion = now();
            $usuariors->usuario_modificacion = $id;
            $usuariors->save();
        }else {
            return null;
        }

        // Encontrar el registro personal asociado
        $personars = Persona::findOrFail($usuariors->id_persona);

        // Actualizar la información personal
        if ($personars) {
            $personars->nombres = $usuario["nombres"];
            $personars->ape_paterno = $usuario["ape_paterno"];
            $personars->ape_materno = $usuario["ape_materno"];
            $personars->id_tipo_documento = $usuario["id_tipo_documento"];
            $personars->numero_documento = $usuario["numero_documento"];
            $personars->sexo = $usuario["sexo"];
            $personars->fecha_nac = isset($usuario['fecha_nac']) && !empty($usuario['fecha_nac']) && $usuario['fecha_nac'] != 'null' ? $usuario['fecha_nac'] : null;
            $personars->email = $usuario["correo_electronico"];
            $personars->celular = $usuario["celular"];
            $personars->direccion = $usuario["direccion"];

            // Guardar los detalles personales
            $resp=$personars->save();

            // Devolver el objeto del usuario actualizado
            return $usuariors;
        }else{
            return null;
        }


    }


}
