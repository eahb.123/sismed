<?php
// app/Repositories/VerificadorRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\VerificadorRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class VerificadorRepository implements VerificadorRepositoryInterface
{

    public function validar_stk_stkd($parametros)
    {
        $resultado = DB::select("SELECT
            ta.nombre almacen,
            ta.id_almacen id_almacen,
            ts.id_medicamento id_medicamento,
            tm.nombre medicamento,
            ts.stock stock,
            sum(tsd.stock) stock_detalle
            from
            tbl_stocks ts
            inner join tbl_stocks_detalle tsd on ts.id_almacen = tsd.id_almacen and ts.id_medicamento = tsd.id_medicamento
            inner join tbl_almacenes ta ON ts.id_almacen =ta.id_almacen and ta.version_exportador = '1.0.1' --and ta.id_almacen = '06952F0101'
            inner join tbl_medicamentos tm on ts.id_medicamento =tm.id_medicamento

            group by 1,2,3,4,5

            HAVING
                SUM(tsd.stock) != ts.stock
            order by ta.nombre, tm.nombre;
        ");

        return $resultado;
    }

}
