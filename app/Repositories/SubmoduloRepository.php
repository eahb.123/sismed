<?php
// app/Repositories/SubmoduloRepository.php

namespace App\Repositories;
//use JWTAuth;
use App\Models\Sesion;
use App\Repositories\Contracts\SubmoduloRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Submodulo;


class SubmoduloRepository implements SubmoduloRepositoryInterface
{
    public function ContinuaOrden(){


        $sp = Submodulo::max('orden');

        return $sp+1;
    }

    public function buscarPorModulo($id_modulo)
    {
        $submodulos = Submodulo::
        where("id_modulo","=",$id_modulo)
        ->get();
        return $submodulos;
    }


    public function listar($parametros)
    {


        $submodulos = Submodulo::where("eliminado",false);


        if (isset($parametros["selectedSupermodulo"]) )  {
            $id_supermodulo = $parametros["selectedSupermodulo"];
            $submodulos = $submodulos->whereHas('modulo', function ($query) use ($id_supermodulo) {
                $query->where('id_supermodulo', $id_supermodulo);});

        }

        if (isset($parametros["selectedModulo"]) )  {
            $submodulos = $submodulos->where("id_modulo",$parametros["selectedModulo"]);
        }

        if (isset($parametros["selectedEstado"]) )  {
            $submodulos = $submodulos->where("estado",$parametros["selectedEstado"]);
        }

        $submodulos = $submodulos->with('modulo');
        $submodulos = $submodulos->with('modulo.supermodulo');
        $modulosrs = $submodulos->get();
        return $modulosrs;
    }

    public function crear($submodulo,$id_usuario){

        $orden = $this->ContinuaOrden();

        $sp = Submodulo::create([
            'id_modulo' => $submodulo["id_modulo"],
            'nombre'=> $submodulo["nombre"],
            'descripcion'=> $submodulo["descripcion"],
            'ruta'=> $submodulo["ruta"],
            'almacenes'=> $submodulo["almacenes"],
            'fecha_creacion' => now(),
            'usuario_creacion' => $id_usuario,
            'orden'=> $orden
        ]);

        $submodulo = $sp;
        return $submodulo;
    }

    public function eliminar($id,$id_usuario){

        $submodulo = Submodulo::findOrFail($id);


        $submodulo->eliminado = true;
        $submodulo->fecha_elimino = now();
        $submodulo->usuario_elimino = $id_usuario;

        $rsp=$submodulo->save();

        return $rsp;
    }

    public function actualizar($id,$submodulo,$id_usuario){
        $submodulors = Submodulo::findOrFail($id);

        if ($submodulors) {
            $submodulors->id_modulo = $submodulo["id_modulo"];
            $submodulors->nombre = $submodulo["nombre"];
            $submodulors->descripcion = $submodulo["descripcion"];
            $submodulors->ruta = $submodulo["ruta"];
            $submodulors->almacenes = $submodulo["almacenes"];
            $submodulors->fecha_modificacion = now();
            $submodulors->usuario_modificacion = $id_usuario;
            $submodulors->save();
            return $submodulors;
        }else{
            return null;
        }
    }

}
