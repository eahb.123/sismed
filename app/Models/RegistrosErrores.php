<?php
// app/Models/RegistrosErrores.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrosErrores extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_error';
    protected $fillable = ['id_usuario', 'fecha_error', 'descripcion_error', 'detalles_error', 'created_at', 'updated_at'];
}
