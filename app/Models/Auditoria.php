<?php
// app/Models/Auditoria.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_auditoria';
    protected $fillable = ['id_usuario', 'accion_realizada', 'fecha_auditoria', 'detalles', 'created_at', 'updated_at'];
}
