<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'tbl_persona';
    protected $primaryKey = 'id'; // Especificar la clave primaria
    //public $incrementing = false; // Indicar que la clave primaria no es autoincrementable
    //protected $keyType = 'string';
    //public $timestamps = false; // Si no tienes campos de timestamps

    // Campos de la tabla
    protected $fillable = [
        'nombres',
        'ape_paterno',
        'ape_materno',
        'id_tipo_documento',
        'numero_documento',
        'tipo_documento',
        'id_cargo',
        'id_almacen',
        'sexo',
        'fecha_nac',
        'email',
        'celular',
        'direccion',
        'almacen'
    ];

    // Relación con el modelo Ris
    public function ris()
    {
        return $this->belongsTo(Ris::class, 'id_ris', 'id_tabla');
    }


    public function usuario()
    {
        return $this->hasMany(Usuario::class, 'id_persona','id');
    }
    // Otras relaciones y métodos si es necesario...
}
