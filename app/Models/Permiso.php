<?php
// app/Models/Permiso.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'permisos';

    protected $primaryKey = 'id_permiso';

    protected $fillable = [ 'id_submodulo',
    'orden' ,
    'nombre',
    'ruta',
    'descripcion',
    'fecha_creacion',
    'usuario_creacion',
    'fecha_modificacion',
    'usuario_modificacion',
    'fecha_elimino',
    'usuario_elimino',
    'estado',
    'eliminado'];

    public function submodulo()
    {
        return $this->belongsTo(Submodulo::class, 'id_submodulo','id_submodulo');
    }

}
