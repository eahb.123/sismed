<?php
// app/Models/PermisoTotal.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermisoTotal extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'permiso_total';
    protected $primaryKey = 'id_permiso_total';


    protected $fillable = ['id_empresa', 'id_ris',
    'id_almacen', 'id_supermodulo', 'id_modulo',
    'id_submodulo', 'id_permiso', 'id_usuario',
    'id_rol',  'fecha_creacion',
    'fecha_modificacion', 'usuario_creacion',
    'usuario_modificacion', 'estado', 'eliminado'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'id_usuario','id_usuario');
    }

    public function supermodulo()
    {
        return $this->belongsTo(Supermodulo::class, 'id_supermodulo','id_supermodulo');
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'id_modulo','id_modulo');
    }

    public function submodulo()
    {
        return $this->belongsTo(Submodulo::class, 'id_submodulo','id_submodulo');
    }

    public function permiso()
    {
        return $this->belongsTo(Permiso::class, 'id_permiso','id_permiso');
    }

    public function almacen()
    {
        return $this->belongsTo(Almacen::class, 'id_almacen', 'id_almacen');
    }

}
