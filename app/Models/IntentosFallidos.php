<?php
// app/Models/IntentosFallidos.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntentosFallidos extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_intento';
    protected $fillable = ['id_usuario', 'fecha_intento', 'ip_origen', 'usuario_intento', 'created_at', 'updated_at'];
}
