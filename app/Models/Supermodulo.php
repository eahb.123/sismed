<?php

// app/Models/Supermodulo.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supermodulo extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'supermodulos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'id_supermodulo'; // Nombre de la clave primaria en la tabla

    protected $fillable = [
        'nombre',
        'id_almacen',
        'ruta',
        'descripcion',
        'orden',
        'almacenes',
        'estado',
        'eliminado',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_modificacion',
        'usuario_modificacion',
        'fecha_elimino',
        'usuario_elimino'
    ];


    public function modulo()
    {
        return $this->hasMany(Modulo::class, 'id_supermodulo','id_supermodulo');
    }
}
