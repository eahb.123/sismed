<?php

// app/Models/Modulo.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'modulos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'id_modulo';

    protected $fillable = [
        'nombre',
        'ruta',
        'descripcion',
        'orden',
        'id_supermodulo',
        'estado',
        'eliminado',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_modificacion',
        'usuario_modificacion',
        'fecha_elimino',
        'usuario_elimino',
    ];

    public function supermodulo()
    {
        return $this->belongsTo(Supermodulo::class, 'id_supermodulo','id_supermodulo');
    }


    public function submodulo()
    {
        return $this->hasMany(Submodulo::class, 'id_modulo','id_modulo');
    }

}
