<?php

// app/Models/Cargo.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_cargo';
    protected $fillable = ['nombre_cargo', 'descripcion_cargo', 'jerarquia_cargo', 'created_at', 'updated_at'];
}
