<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ris extends Model
{
    protected $table = 'tbl_tablas';
    public $timestamps = false;

    protected $attributes = ['id_tabla' => 1];

    protected $fillable = [
        'id_tipo',
        'descripcion',
        'abreviatura',
    ];

}
