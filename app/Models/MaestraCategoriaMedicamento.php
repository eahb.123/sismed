<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaestraCategoriaMedicamento extends Model
{
    protected $table = 'tbl_maestra_categoria_medicamento';
    protected $primaryKey = 'id_maestra_categoria_medicamento'; // Especificar la clave primaria

    // Campos de la tabla
    protected $fillable = [
        'id_maestra_categoria',
        'id_medicamento',
        'cantidad'
    ];

    // Relación con el modelo MaestraCategoria
    public function maestraCategoria()
    {
        return $this->belongsTo(MaestraCategoria::class, 'id_maestra_categoria','id_maestra_categoria');
    }

    // Relación con el modelo Medicamento
    public function medicamento()
    {
        return $this->belongsTo(Medicamento::class, 'id_medicamento','id_medicamento');
    }


}
