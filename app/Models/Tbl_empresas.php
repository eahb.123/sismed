<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tbl_empresas extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $fillable = ['nombre'];

    // Relación con la tabla TablaCompleta
    public function tablaCompleta()
    {
        return $this->hasMany(TablaCompleta::class, 'id_empresa');
    }
}