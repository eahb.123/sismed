<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaestraCategoria extends Model
{
    protected $table = 'tbl_maestra_categoria';
    protected $primaryKey = 'id_maestra_categoria'; // Especificar la clave primaria

    // Campos de la tabla
    protected $fillable = [
        'nombre',
        'tipo',
        'almacen_categoria',
        'grupo',
        'abreviado',
        'rango'
    ];

    // Relación con el modelo Ris
    // Relación con el modelo Medicamento
    public function maestraCategoriaMedicamento()
    {
        return $this->hasMany(MaestraCategoriaMedicamento::class, 'id_maestra_categoria','id_maestra_categoria');
    }


}
