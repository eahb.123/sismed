<?php

// app/Models/Submodulo.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submodulo extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'submodulos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'id_submodulo';

    protected $fillable = [
    'nombre',
    'orden',
    'id_modulo',
    'ruta',
    'descripcion',
    'fecha_creacion',
    'usuario_creacion',
    'fecha_modificacion',
    'usuario_modificacion',
    'estado',
    'eliminado'];

    public function supermodulo()
    {
        return $this->belongsTo(Supermodulo::class, 'id_supermodulo','id_supermodulo');
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'id_modulo','id_modulo');
    }


    public function permiso()
    {
        return $this->hasMany(Permiso::class, 'id_submodulo','id_submodulo');
    }

}
