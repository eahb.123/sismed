<?php
// app/Models/ConfiguracionSistema.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfiguracionSistema extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_configuracion';
    protected $fillable = ['nombre', 'valor', 'created_at', 'updated_at'];
}
