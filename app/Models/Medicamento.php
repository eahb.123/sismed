<?php
// app/Models/Medicamento.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    use HasFactory;

    protected $table = 'tbl_medicamentos';
    protected $primaryKey = 'id_medicamento'; // Especificar la clave primaria
    public $incrementing = false; // Indicar que la clave primaria no es autoincrementable
    protected $keyType = 'string';
    public $timestamps = false; // Si no tienes campos de timestamps


    protected $fillable = [
        'nombre',
        'presentacion',
        'concentracion',
        'forma_farmaceutica',
        'medtip',
        'medpet',
        'medest',
        'medsm',
        'medsituacion',
        'cod_siga',
        'id_programa_estrategico',
        'evalua_digemid',
        'estado'
    ];

}
