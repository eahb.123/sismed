<?php
// app/Models/Usuario.php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\PermisoTotal;
use App\Models\Supermodulo;
use App\Models\Modulo;
use App\Models\Submodulo;
use App\Models\Permiso;


class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public $timestamps = false;

    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';

    protected $fillable = [
        'correo_electronico',
        'fecha_registro',
        'imagen_perfil',
        'id_persona',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_modificacion',
        'usuario_modificacion',
        'fecha_elimino',
        'usuario_elimino',
        'estado',
        'eliminado',
        'password'
    ];

    public function persona()
    {
        return $this->belongsTo(Persona::class, 'id_persona', 'id');
    }


    //protected $hidden = ['password'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'correo_electronico' => $this->correo_electronico,
        ];
    }

    public function hasPermission($usuario,$permission)
    {

        $validarPermiso = $this->permissions();

        if ($usuario->correo_electronico == 'admin@admin.com') {
            return true;
        }
        foreach ($permission as $value) {
            $array = explode(":", $value);
            $id = null;
            $respuesta = true;


            if ($array[0]=='registrado') {
                return true;
            }


            switch ($array[0]) {
                case 'supermodulo':
                    $supermodulo = Supermodulo::where("nombre", $array[1])->first();
                    $id = $supermodulo ? $supermodulo['id_supermodulo'] : null;
                    break;
                case 'modulo':
                    $modulo = Modulo::where("nombre", $array[1])->first();
                    $id = $modulo ? $modulo['id_modulo'] : null;
                    break;
                case 'submodulo':
                    $submodulo = Submodulo::where("nombre", $array[1])->first();
                    $id = $submodulo ? $submodulo['id_submodulo'] : null;
                    break;
                case 'permiso':
                    $permiso = Permiso::where("nombre", $array[1])->pluck('id_permiso');;

                    $id = $permiso ? $permiso : null;
                    break;
            }

            if ($id) {
                if ($array[0] == 'permiso') {

                    $validarPermiso = $validarPermiso->whereIn("id_".$array[0], $permiso);

                    //$validarPermiso = $validarPermiso->where("id_".$array[0], $permiso.);
                } else {
                    $validarPermiso = $validarPermiso->where("id_".$array[0], $id);
                }


            } else {
                $respuesta = false;
                break;
            }

        }

        return $respuesta && $validarPermiso->exists();
    }

    public function permissions()
    {
        return $this->hasMany(PermisoTotal::class, 'id_usuario', 'id_usuario');
    }
}
