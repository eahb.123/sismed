<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    protected $table = 'tbl_almacenes';
    protected $primaryKey = 'id_almacen'; // Especificar la clave primaria
    public $incrementing = false; // Indicar que la clave primaria no es autoincrementable
    protected $keyType = 'string';
    public $timestamps = false; // Si no tienes campos de timestamps

    // Campos de la tabla
    protected $fillable = [
        'nombre',
        'estado',
        'fecha_venta_exp',
        'fecha_ultima_conexion',
        'instalado',
        'indicador',
        'resp_farm',
        'telf_resp',
        'mail_resp',
        'filter_cenares',
        'id_ris',
        'id_categoria',
        'id_alterno',
        'renaes',
        'id_farmacia',
        'tipo',
        'id_ipress',
    ];

    // Relación con el modelo Ris
    public function ris()
    {
        return $this->belongsTo(Ris::class, 'id_ris', 'id_tabla');
    }

    // Otras relaciones y métodos si es necesario...
}
