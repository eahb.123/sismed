<?php
// app/Models/Sesion.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    protected $table = 'sesiones';
    //use Notifiable;
    use HasFactory;

    protected $primaryKey = 'id_sesion';
    protected $fillable = ['id_usuario', 'token_sesion', 'fecha_inicio', 'fecha_expiracion', 'created_at', 'updated_at'];
}
