<?php
// app/Services/EmpresaService.php

namespace App\Services;

use App\Repositories\Contracts\EmpresaRepositoryInterface;
use App\Services\Contracts\EmpresaServiceInterface;

class EmpresaService implements EmpresaServiceInterface
{
    private $empresaRepository;

    public function __construct(EmpresaRepositoryInterface $empresaRepository)
    {
        $this->empresaRepository = $empresaRepository;
    }

    public function getAllEmpresas()
    {
        return $this->empresaRepository->all();
    }

    public function getEmpresaById($id)
    {
        return $this->empresaRepository->find($id);
    }

    public function createEmpresa($data)
    {
        return $this->empresaRepository->create($data);
    }

    public function updateEmpresa($id, $data)
    {
        return $this->empresaRepository->update($id, $data);
    }

    public function deleteEmpresa($id)
    {
        return $this->empresaRepository->delete($id);
    }
}
