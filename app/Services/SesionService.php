<?php
// app/Services/SesionService.php

namespace App\Services;

use App\Repositories\Contracts\SesionRepositoryInterface;
use App\Services\Contracts\SesionServiceInterface;

class SesionService implements SesionServiceInterface
{
    private $sesionRepository;

    public function __construct(SesionRepositoryInterface $sesionRepository)
    {
        $this->sesionRepository = $sesionRepository;
    }


    public function createSesion($usuario,$token)
    {
        return $this->sesionRepository->create($usuario,$token);
    }

    public function ValidarSesionActiva($usuario,$token,$payload,$route)
    {
        return $this->sesionRepository->validar($usuario,$token,$payload,$route);
    }

    public function getAllSesion()
    {
        return $this->sesionRepository->all();
    }

    public function getSesionById($id)
    {
        return $this->sesionRepository->find($id);
    }

    public function updateSesion($id, $data)
    {
        return $this->sesionRepository->update($id, $data);
    }

    public function deleteSesion($id)
    {
        return $this->sesionRepository->delete($id);
    }
}
