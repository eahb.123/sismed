<?php
// app/Services/VerificadorService.php

namespace App\Services;

use App\Repositories\Contracts\VerificadorRepositoryInterface;
use App\Services\Contracts\VerificadorServiceInterface;

class VerificadorService implements VerificadorServiceInterface
{
    private $verificadorRepository;

    public function __construct(VerificadorRepositoryInterface $verificadorRepository)
    {
        $this->verificadorRepository = $verificadorRepository;
    }

    public function validar_stk_stkd($parametros)
    {
        return $this->verificadorRepository->validar_stk_stkd($parametros);
    }



}
