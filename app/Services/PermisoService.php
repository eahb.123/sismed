<?php
// app/Services/PermisoService.php

namespace App\Services;

use App\Repositories\Contracts\PermisoRepositoryInterface;
use App\Services\Contracts\PermisoServiceInterface;

class PermisoService implements PermisoServiceInterface
{
    private $permisoRepository;

    public function __construct(PermisoRepositoryInterface $permisoRepository)
    {
        $this->permisoRepository = $permisoRepository;
    }


    public function buscarPermisoPorSubmodulo($id_submodulo)
    {
        return $this->permisoRepository->buscarPorSubmodulo($id_submodulo);
    }

    public function listarPermiso($parametros)
    {
        return $this->permisoRepository->listar($parametros);
    }

    public function crearPermiso($permiso,$id_usuario)
    {
        return $this->permisoRepository->crear($permiso,$id_usuario);
    }

    public function eliminarPermiso($id,$id_usuario)
    {
        return $this->permisoRepository->eliminar($id,$id_usuario);
    }

    public function actualizarPermiso($id,$permiso,$id_usuario)
    {
        return $this->permisoRepository->actualizar($id,$permiso,$id_usuario);
    }

}
