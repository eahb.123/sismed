<?php
// app/Services/MovimientoService.php

namespace App\Services;

use App\Repositories\Contracts\MovimientoRepositoryInterface;
use App\Services\Contracts\MovimientoServiceInterface;

class MovimientoService implements MovimientoServiceInterface
{
    private $movimientoRepository;

    public function __construct(MovimientoRepositoryInterface $movimientoRepository)
    {
        $this->movimientoRepository = $movimientoRepository;
    }

    public function listarMovimientos($parametros)
    {
        return $this->movimientoRepository->listar($parametros);
    }

    public function listarMovimientosAlmacen($parametros)
    {
        return $this->movimientoRepository->listar_almacen($parametros);
    }
}
