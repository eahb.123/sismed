<?php
// app/Services/GeneralService.php

namespace App\Services;

use App\Repositories\Contracts\GeneralRepositoryInterface;
use App\Services\Contracts\GeneralServiceInterface;

class GeneralService implements GeneralServiceInterface
{
    private $generalRepository;

    public function __construct(GeneralRepositoryInterface $generalRepository)
    {
        $this->generalRepository = $generalRepository;
    }

    public function listarEstrategia($parametros)
    {
        return $this->generalRepository->listarEstrategia($parametros);
    }



}
