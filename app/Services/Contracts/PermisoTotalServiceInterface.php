<?php
// app/Services/Contracts/PermisoTotalServiceInterface.php

namespace App\Services\Contracts;

interface PermisoTotalServiceInterface
{

    public function buscarPorUsuarioPermisoTotal($id_usuario,$parametros);

    public function crearPermisoTotal($permisototal,$id_usuario);

    public function eliminarPermisosTotales($arrPermisototalIds);

    public function activarPermisosTotales($arrPermisototalIds);

    public function desactivarPermisosTotales($arrPermisototalIds);

}
