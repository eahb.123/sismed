<?php
// app/Services/Contracts/ModuloServiceInterface.php

namespace App\Services\Contracts;

interface ModuloServiceInterface
{

    public function listarModulo($parametros);

    public function buscarModuloPorSprmodulo($id_supermodulo);

    public function crearModulo($modulo,$id_usuario);

    public function eliminarModulo($id,$id_usuario);

    public function actualizarModulo($id,$modulo,$id_usuario);

    public function lista_modulo_combo();

}
