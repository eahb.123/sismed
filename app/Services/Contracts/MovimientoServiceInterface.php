<?php
// app/Services/Contracts/MovimientoServiceInterface.php

namespace App\Services\Contracts;

interface MovimientoServiceInterface
{

    public function listarMovimientos($parametros);

    public function listarMovimientosAlmacen($parametros);

}
