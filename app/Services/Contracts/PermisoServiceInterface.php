<?php
// app/Services/Contracts/PermisoServiceInterface.php

namespace App\Services\Contracts;

interface PermisoServiceInterface
{

    public function buscarPermisoPorSubmodulo($id_submodulo);

    public function listarPermiso($parametros);

    public function crearPermiso($permiso,$id_usuario);

    public function eliminarPermiso($id,$id_usuario);

    public function actualizarPermiso($id,$permiso,$id_usuario);


}
