<?php
// app/Services/Contracts/SupermoduloServiceInterface.php

namespace App\Services\Contracts;

interface SupermoduloServiceInterface
{

    public function listarSupermodulo();

    public function reordenarSupermodulo($supermodulos);

    public function cambiarEstadoSupermodulo($supermodulos);

    public function crearSupermodulo($supermodulo);

    public function eliminarSupermodulo($id,$id_usuario);

    public function actualizarSupermodulo($id,$supermodulo,$id_usuario);

    public function listarSupermoduloCombo();
}
