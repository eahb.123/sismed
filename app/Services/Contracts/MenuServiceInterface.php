<?php
// app/Services/Contracts/MenuServiceInterface.php

namespace App\Services\Contracts;

interface MenuServiceInterface
{

    public function listarMenu($usuario);

}
