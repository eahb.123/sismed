<?php
// app/Services/Contracts/EmpresaServiceInterface.php

namespace App\Services\Contracts;

interface EmpresaServiceInterface
{
    public function getAllEmpresas();

    public function getEmpresaById($id);

    public function createEmpresa($data);

    public function updateEmpresa($id, $data);

    public function deleteEmpresa($id);
}
