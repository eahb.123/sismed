<?php
// app/Services/Contracts/IciServiceInterface.php

namespace App\Services\Contracts;

interface IciServiceInterface
{

    public function ici_diario($parametros);

    public function ici_estrategico($parametros);

    public function ici_por_producto($parametros);

}
