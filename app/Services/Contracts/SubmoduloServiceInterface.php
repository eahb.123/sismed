<?php
// app/Services/Contracts/SubmoduloServiceInterface.php

namespace App\Services\Contracts;

interface SubmoduloServiceInterface
{

    public function buscarSubmoduloPorModulo($id_modulo);

    public function listarSubmodulo($parametros);

    public function crearSubmodulo($submodulo,$id_usuario);

    public function eliminarSubmodulo($id,$id_usuario);

    public function actualizarSubmodulo($id,$submodulo,$id_usuario);


}
