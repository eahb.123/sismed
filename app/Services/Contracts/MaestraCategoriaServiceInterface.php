<?php
// app/Services/Contracts/listarMaestraCategoriaServiceInterface.php

namespace App\Services\Contracts;

interface MaestraCategoriaServiceInterface
{

    public function listarMaestraCategoria($parametros);

    public function consultaCategoriaMaestraMedicamento($categoria,$ris);

    public function lista_documento_identidad_combo();

    public function listar_kit_escenario_iii();
}
