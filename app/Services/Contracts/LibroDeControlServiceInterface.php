<?php
// app/Services/Contracts/LibroDeControlServiceInterface.php

namespace App\Services\Contracts;

interface LibroDeControlServiceInterface
{

    public function libroDeControl($desde,$hasta,$local,$libroDeControl);

}
