<?php
// app/Services/Contracts/EvaluacionServiceInterface.php

namespace App\Services\Contracts;

interface EvaluacionServiceInterface
{

    public function buscar_evaluacion($id_evaluacion);

    public function preguntas_x_evaluacion($id_evaluacion);

    public function buscar_x_documento($tipo_documento,$numero_documento);

    public function guardar_actualizar_persona($parametros);

    public function enviar_prespuesta($parametros);

    public function evaluacion_listar();

    public function combo_estado();

    public function combo_tipo();

    public function combo_tipo_pregunta();

    public function crear_evaluacion($parametros);

    public function guardar_pregunta_respuesta($parametros);

    public function preguntas_abiertas_x_persona($id_evaluacion);

    public function guardar_preguntas_abiertas($id_evaluacion,$respuestas);

    public function calcular_resultados_evaluacion($id_evaluacion);

    public function resultados_evaluacion($id_evaluacion);

    public function iniciar_evaluacion($id_evaluacion);

    public function cerrado_evaluacion($id_evaluacion);

    public function finalizado_evaluacion($id_evaluacion);


}
