<?php
// app/Services/Contracts/UsuarioServiceInterface.php

namespace App\Services\Contracts;

interface UsuarioServiceInterface
{

    public function listarUsuario();

    public function crearUsuario($usuario,$id_usuario);

    public function buscarUsuario($id_usuario);

    public function editarUsuario($id,$usuario,$id_login);

    public function buscarmeUsuario();

    public function actualizarmeUsuario($usuario,$request);
}
