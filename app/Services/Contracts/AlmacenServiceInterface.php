<?php
// app/Services/Contracts/AlmacenServiceInterface.php

namespace App\Services\Contracts;

interface AlmacenServiceInterface
{

    public function listarAlmacen($parametros);

    public function listarRis();

    public function listarAlmacenFiltrado($parametros,$usuario);

}
