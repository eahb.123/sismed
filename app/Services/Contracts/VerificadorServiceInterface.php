<?php
// app/Services/Contracts/VerificadorServiceInterface.php

namespace App\Services\Contracts;

interface VerificadorServiceInterface
{

    public function validar_stk_stkd($parametros);

}
