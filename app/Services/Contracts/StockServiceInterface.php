<?php
// app/Services/Contracts/StockServiceInterface.php

namespace App\Services\Contracts;

interface StockServiceInterface
{

    public function listarStockMedicamento($parametros);

    public function CuadroResumen($parametros);

    public function DetalleCuadroResumen($parametros);

    public function LotesPorVencer($parametros);

    public function listDisponibilidadDiaria($id_almacen,$anio,$nmes,$id_tipo,$fecha);

}
