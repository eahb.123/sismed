<?php
// app/Services/Contracts/EmpresaServiceInterface.php

namespace App\Services\Contracts;

interface SesionServiceInterface
{
    public function createSesion($usuario,$token);

    public function ValidarSesionActiva($usuario,$token,$payload,$route);

    public function getAllSesion();

    public function getSesionById($id);

    public function updateSesion($id, $data);

    public function deleteSesion($id);
}
