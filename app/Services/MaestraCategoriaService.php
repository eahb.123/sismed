<?php
// app/Services/MaestraCategoriaService.php

namespace App\Services;

use App\Repositories\Contracts\MaestraCategoriaRepositoryInterface;
use App\Services\Contracts\MaestraCategoriaServiceInterface;

class MaestraCategoriaService implements MaestraCategoriaServiceInterface
{
    private $maestraCategoriaRepository;

    public function __construct(MaestraCategoriaRepositoryInterface $maestraCategoriaRepository)
    {
        $this->maestraCategoriaRepository = $maestraCategoriaRepository;
    }

    public function listarMaestraCategoria($parametros)
    {
        return $this->maestraCategoriaRepository->listar($parametros);
    }

    public function consultaCategoriaMaestraMedicamento($categoria,$ris)
    {
        return $this->maestraCategoriaRepository->consultaCategoriaMaestraMedicamento($categoria,$ris);
    }

    public function lista_documento_identidad_combo()
    {
        return $this->maestraCategoriaRepository->lista_documento_identidad_combo();
    }

    public function listar_kit_escenario_iii()
    {
        return $this->maestraCategoriaRepository->listar_kit_escenario_iii();
    }



}
