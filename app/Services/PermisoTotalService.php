<?php
// app/Services/PermisoTotalService.php

namespace App\Services;

use App\Repositories\Contracts\PermisoTotalRepositoryInterface;
use App\Services\Contracts\PermisoTotalServiceInterface;

class PermisoTotalService implements PermisoTotalServiceInterface
{
    private $permisoTotalRepository;

    public function __construct(PermisoTotalRepositoryInterface $permisoTotalRepository)
    {
        $this->permisoTotalRepository = $permisoTotalRepository;
    }


    public function buscarPorUsuarioPermisoTotal($id_usuario,$parametros)
    {
        return $this->permisoTotalRepository->buscarPorUsuario($id_usuario,$parametros);
    }


    public function crearPermisoTotal($permisototal,$id_usuario)
    {
        return $this->permisoTotalRepository->crear($permisototal,$id_usuario);
    }

    public function eliminarPermisosTotales($arrPermisototalIds)
    {
        return $this->permisoTotalRepository->eliminarPermisosTotales($arrPermisototalIds);
    }

    public function activarPermisosTotales($arrPermisototalIds)
    {
        return $this->permisoTotalRepository->activarPermisosTotales($arrPermisototalIds);
    }

    public function desactivarPermisosTotales($arrPermisototalIds)
    {
        return $this->permisoTotalRepository->desactivarPermisosTotales($arrPermisototalIds);
    }

}
