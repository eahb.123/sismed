<?php
// app/Services/StockService.php

namespace App\Services;

use App\Repositories\Contracts\StockRepositoryInterface;
use App\Services\Contracts\StockServiceInterface;

class StockService implements StockServiceInterface
{
    private $stockRepository;

    public function __construct(StockRepositoryInterface $stockRepository)
    {
        $this->stockRepository = $stockRepository;
    }

    public function listarStockMedicamento($parametros)
    {
        return $this->stockRepository->listar($parametros);
    }

    public function listarDetalleStockMedicamento($parametros)
    {
        return $this->stockRepository->listarDetalle($parametros);
    }

    public function ReportelistarDetalleStockMedicamento($parametros)
    {
        return $this->stockRepository->ReportelistarDetalle($parametros);
    }

    public function CuadroResumen($parametros)
    {
        return $this->stockRepository->CuadroResumen($parametros);
    }

    public function DetalleCuadroResumen($parametros)
    {
        return $this->stockRepository->DetalleCuadroResumen($parametros);
    }

    public function LotesPorVencer($parametros)
    {
        return $this->stockRepository->LotesPorVencer($parametros);
    }

    public function listDisponibilidadDiaria($id_almacen,$anio,$nmes,$id_tipo,$fecha)
    {
        return $this->stockRepository->listDisponibilidadDiaria($id_almacen,$anio,$nmes,$id_tipo,$fecha);
    }

}
