<?php
// app/Services/LibroDeControlService.php

namespace App\Services;

use App\Repositories\Contracts\LibroDeControlRepositoryInterface;
use App\Services\Contracts\LibroDeControlServiceInterface;

class LibroDeControlService implements LibroDeControlServiceInterface
{
    private $libroDeControlRepository;

    public function __construct(LibroDeControlRepositoryInterface $libroDeControlRepository)
    {
        $this->libroDeControlRepository = $libroDeControlRepository;
    }

    public function libroDeControl($desde,$hasta,$local,$libroDeControl)
    {
        return $this->libroDeControlRepository->libroDeControl($desde,$hasta,$local,$libroDeControl);
    }

}
