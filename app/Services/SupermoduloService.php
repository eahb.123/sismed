<?php
// app/Services/SupermoduloService.php

namespace App\Services;

use App\Repositories\Contracts\SupermoduloRepositoryInterface;
use App\Services\Contracts\SupermoduloServiceInterface;

class SupermoduloService implements SupermoduloServiceInterface
{
    private $SupermoduloRepository;

    public function __construct(SupermoduloRepositoryInterface $supermoduloRepository)
    {
        $this->supermoduloRepository = $supermoduloRepository;
    }

    public function listarSupermodulo()
    {
        return $this->supermoduloRepository->listar();
    }

    public function reordenarSupermodulo($supermodulos)
    {
        return $this->supermoduloRepository->reordenar($supermodulos);
    }

    public function cambiarEstadoSupermodulo($supermodulos)
    {
        return $this->supermoduloRepository->cambiarEstado($supermodulos);
    }

    public function crearSupermodulo($supermodulo)
    {
        return $this->supermoduloRepository->crear($supermodulo);
    }

    public function eliminarSupermodulo($id,$id_usuario)
    {
        return $this->supermoduloRepository->eliminar($id,$id_usuario);

    }

    public function actualizarSupermodulo($id,$supermodulo,$id_usuario)
    {
        return $this->supermoduloRepository->actualizar($id,$supermodulo,$id_usuario);

    }

    public function listarSupermoduloCombo()
    {
        return $this->supermoduloRepository->listarSupermoduloCombo();

    }
}
