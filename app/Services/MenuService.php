<?php
// app/Services/SesionService.php

namespace App\Services;

use App\Repositories\Contracts\MenuRepositoryInterface;
use App\Services\Contracts\MenuServiceInterface;

class MenuService implements MenuServiceInterface
{
    private $menuRepository;

    public function __construct(MenuRepositoryInterface $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }


    public function listarMenu($usuario)
    {
        return $this->menuRepository->listar($usuario);
    }
}
