<?php
// app/Services/EvaluacionService.php

namespace App\Services;

use App\Repositories\Contracts\EvaluacionRepositoryInterface;
use App\Services\Contracts\EvaluacionServiceInterface;

class EvaluacionService implements EvaluacionServiceInterface
{
    private $evaluacionRepository;

    public function __construct(EvaluacionRepositoryInterface $evaluacionRepository)
    {
        $this->evaluacionRepository = $evaluacionRepository;
    }

    public function buscar_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->buscar_evaluacion($id_evaluacion);
    }

    public function preguntas_x_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->preguntas_x_evaluacion($id_evaluacion);
    }

    public function buscar_x_documento($tipo_documento,$numero_documento)
    {
        return $this->evaluacionRepository->buscar_x_documento($tipo_documento,$numero_documento);
    }

    public function guardar_actualizar_persona($parametros)
    {
        return $this->evaluacionRepository->guardar_actualizar_persona($parametros);
    }

    public function enviar_prespuesta($parametros)
    {
        return $this->evaluacionRepository->enviar_prespuesta($parametros);
    }

    public function evaluacion_listar()
    {
        return $this->evaluacionRepository->evaluacion_listar();
    }

    public function combo_estado()
    {
        return $this->evaluacionRepository->combo_estado();
    }

    public function combo_tipo()
    {
        return $this->evaluacionRepository->combo_tipo();
    }

    public function combo_tipo_pregunta()
    {
        return $this->evaluacionRepository->combo_tipo_pregunta();
    }

    public function crear_evaluacion($parametros)
    {
        return $this->evaluacionRepository->crear_evaluacion($parametros);
    }

    public function guardar_pregunta_respuesta($parametros)
    {
        return $this->evaluacionRepository->guardar_pregunta_respuesta($parametros);
    }

    public function preguntas_abiertas_x_persona($id_evaluacion)
    {
        return $this->evaluacionRepository->preguntas_abiertas_x_persona($id_evaluacion);
    }

    public function guardar_preguntas_abiertas($id_evaluacion,$respuestas)
    {
        return $this->evaluacionRepository->guardar_preguntas_abiertas($id_evaluacion,$respuestas);
    }

    public function calcular_resultados_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->calcular_resultados_evaluacion($id_evaluacion);
    }

    public function resultados_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->resultados_evaluacion($id_evaluacion);
    }

    public function iniciar_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->iniciar_evaluacion($id_evaluacion);
    }

    public function cerrado_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->cerrado_evaluacion($id_evaluacion);
    }

    public function finalizado_evaluacion($id_evaluacion)
    {
        return $this->evaluacionRepository->finalizado_evaluacion($id_evaluacion);
    }


}
