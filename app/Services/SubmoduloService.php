<?php
// app/Services/SubmoduloService.php

namespace App\Services;

use App\Repositories\Contracts\SubmoduloRepositoryInterface;
use App\Services\Contracts\SubmoduloServiceInterface;

class SubmoduloService implements SubmoduloServiceInterface
{
    private $submoduloRepository;

    public function __construct(SubmoduloRepositoryInterface $submoduloRepository)
    {
        $this->submoduloRepository = $submoduloRepository;
    }


    public function buscarSubmoduloPorModulo($id_modulo)
    {
        return $this->submoduloRepository->buscarPorModulo($id_modulo);
    }

    public function listarSubmodulo($parametros)
    {
        return $this->submoduloRepository->listar($parametros);
    }

    public function crearSubmodulo($submodulo,$id_usuario)
    {
        return $this->submoduloRepository->crear($submodulo,$id_usuario);
    }
    
    public function eliminarSubmodulo($id,$id_usuario)
    {
        return $this->submoduloRepository->eliminar($id,$id_usuario);

    }

    public function actualizarSubmodulo($id,$submodulo,$id_usuario)
    {
        return $this->submoduloRepository->actualizar($id,$submodulo,$id_usuario);

    }

}
