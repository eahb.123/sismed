<?php
// app/Services/ModuloService.php

namespace App\Services;

use App\Repositories\Contracts\ModuloRepositoryInterface;
use App\Services\Contracts\ModuloServiceInterface;

class ModuloService implements ModuloServiceInterface
{
    private $moduloRepository;

    public function __construct(ModuloRepositoryInterface $moduloRepository)
    {
        $this->moduloRepository = $moduloRepository;
    }

    public function listarModulo($parametros)
    {
        return $this->moduloRepository->listar($parametros);
    }

    public function buscarModuloPorSprmodulo($id_supermodulo)
    {
        return $this->moduloRepository->buscarPorSprmodulo($id_supermodulo);
    }

    public function crearModulo($modulo,$id_usuario)
    {
        return $this->moduloRepository->crear($modulo,$id_usuario);
    }

    public function eliminarModulo($id,$id_usuario)
    {
        return $this->moduloRepository->eliminar($id,$id_usuario);

    }

    public function actualizarModulo($id,$modulo,$id_usuario)
    {
        return $this->moduloRepository->actualizar($id,$modulo,$id_usuario);

    }

    public function lista_modulo_combo()
    {
        return $this->moduloRepository->lista_modulo_combo();

    }



}
