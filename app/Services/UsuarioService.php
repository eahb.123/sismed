<?php
// app/Services/UsuairoService.php

namespace App\Services;

use App\Repositories\Contracts\UsuarioRepositoryInterface;
use App\Services\Contracts\UsuarioServiceInterface;

class UsuarioService implements UsuarioServiceInterface
{
    private $usuarioRepository;

    public function __construct(UsuarioRepositoryInterface $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }

    public function listarUsuario()
    {
        return $this->usuarioRepository->listar();
    }

    public function crearUsuario($usuario,$id_usuario)
    {
        return $this->usuarioRepository->crear($usuario,$id_usuario);
    }

    public function buscarUsuario($id_usuario)
    {
        return $this->usuarioRepository->buscar($id_usuario);
    }

    public function editarUsuario($id,$usuario,$id_login)
    {
        return $this->usuarioRepository->editar($id,$usuario,$id_login);
    }

    public function buscarmeUsuario()
    {
        return $this->usuarioRepository->buscarme();
    }

    public function actualizarmeUsuario($usuario,$request)
    {
        return $this->usuarioRepository->actualizarme($usuario,$request);
    }

}
