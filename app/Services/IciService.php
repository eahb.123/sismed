<?php
// app/Services/IciService.php

namespace App\Services;

use App\Repositories\Contracts\IciRepositoryInterface;
use App\Services\Contracts\IciServiceInterface;

class IciService implements IciServiceInterface
{
    private $iciRepository;

    public function __construct(IciRepositoryInterface $iciRepository)
    {
        $this->iciRepository = $iciRepository;
    }

    public function ici_diario($parametros)
    {
        return $this->iciRepository->ici_diario($parametros);
    }

    public function ici_estrategico($parametros)
    {
        return $this->iciRepository->ici_estrategico($parametros);
    }

    public function ici_por_producto($parametros)
    {
        return $this->iciRepository->ici_por_producto($parametros);
    }

}
