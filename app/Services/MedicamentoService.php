<?php
// app/Services/UsuairoService.php

namespace App\Services;

use App\Repositories\Contracts\AlmacenRepositoryInterface;
use App\Services\Contracts\AlmacenServiceInterface;

class AlmacenService implements AlmacenServiceInterface
{
    private $almacenRepository;

    public function __construct(AlmacenRepositoryInterface $almacenRepository)
    {
        $this->almacenRepository = $almacenRepository;
    }

    public function listarAlmacen($parametros)
    {
        return $this->almacenRepository->listar($parametros);
    }

    public function listarRis()
    {
        return $this->almacenRepository->listarRis();
    }


}
