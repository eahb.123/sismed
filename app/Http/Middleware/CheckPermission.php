<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\PermisoTotal; // Ajusta el namespace y el modelo según tu configuración
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Usuario;

class CheckPermission
{
    public function handle($request, Closure $next, ...$permission)
    {
        try {
            // Obtener el token desde la solicitud
            $token = JWTAuth::parseToken();
            // Obtener el usuario desde el token
            $usuario = $token->authenticate();
        } catch (TokenExpiredException $e) {
            try {
                // Intentar renovar el token
                $newToken = JWTAuth::refresh(JWTAuth::getToken());
                // Establecer el nuevo token en la respuesta
                $request->headers->set('Authorization', 'Bearer ' . $newToken);
                // Reintentar la autenticación con el nuevo token
                $usuario = JWTAuth::setToken($newToken)->toUser();
            } catch (JWTException $e) {
                return response()->json(['error' => 'Token has expired and could not be refreshed','success'=>'true','code'=>'401'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Token is invalid','success'=>'true','code'=>'401'], 401);
        }

        // Verificar si el usuario tiene el permiso específico
        $validar = $usuario->hasPermission($usuario, $permission);

        if (!$usuario || !$validar) {
            return response()->json(['error' => 'No tienes permisos para acceder a esta ruta.','success'=>'true','code'=>'403'], 403);
        }

        return $next($request);
    }

    protected function userHasPermission($user, $permission)
    {
        // Acceder a la base de datos y verificar si el usuario tiene el permiso
        return $user->permissions()->where('nombre', $permission)->exists();
    }
}
