<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\PermisoServiceInterface;
use Illuminate\Support\Facades\Auth;

class PermisoController extends Controller
{
    private $permisoService;

    public function __construct(
        PermisoServiceInterface $permisoService
    )
    {
        $this->permisoService = $permisoService;
    }

    public function buscarporsubmodulo(Request $request,$id_submodulo)
    {
        $permisos = $this->permisoService->buscarPermisoPorSubmodulo($id_submodulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Submodulos retornados correctamente',
            'data' => $permisos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function listar(Request $request)
    {
        $parametros = $request->all();
        $permisos = $this->permisoService->listarPermiso($parametros);

        return response()->json([
            'success' => true,
            'data' => $permisos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function crear(Request $request)
    {
        $usuario = Auth::user();

        $permiso = $request->all();
        $permisorsp = $this->permisoService->crearPermiso($permiso,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $permisorsp
        ]);
    }

    public function eliminar(Request $request,$id)
    {
        $usuario = Auth::user();

        $permisorsp = $this->permisoService->eliminarPermiso($id,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $permisorsp
        ]);
    }

    public function editar(Request $request,$id)
    {
        $usuario = Auth::user();

        $permiso = $request->all();
        $permisorsp = $this->permisoService->actualizarPermiso($id,$permiso,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $permisorsp
        ]);
    }

    public function lista_permiso_combo(Request $request,$id_submodulo)
    {
        $permisos = $this->permisoService->buscarPermisoPorSubmodulo($id_submodulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Permiso retornados correctamente',
            'data' => $permisos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }
}
