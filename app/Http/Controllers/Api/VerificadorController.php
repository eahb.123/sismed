<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\VerificadorServiceInterface;
use DateTime;

class VerificadorController extends Controller
{
    private $verificadorService;

    public function __construct(
        VerificadorServiceInterface $verificadorService
    )
    {
        $this->verificadorService = $verificadorService;
    }

    public function validar_stk_stkd(Request $request)
    {

        $parametros = $request->all();
        $resultado = $this->verificadorService->validar_stk_stkd($parametros);
        return response()->json($resultado);
    }

}
