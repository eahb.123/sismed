<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\UsuarioServiceInterface;
use Illuminate\Support\Facades\Auth;

use App\Models\Usuario;

class UsuariosController extends Controller
{

    private $usuarioService;

    public function __construct(
        UsuarioServiceInterface $usuarioService
    )
    {
        $this->usuarioService = $usuarioService;
    }

    public function listar()
    {

        $usuarios = $this->usuarioService->listarUsuario();


        return response()->json([
            'success' => true,
            'mensaje' => 'Usuarios retornados correctamente',
            'data' => $usuarios,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function crear(Request $request)
    {
        $usuariologin = Auth::user();
        $usuario = $request->all();
        $rspta = $this->usuarioService->crearUsuario($usuario,$usuariologin->id_usuario);


        return response()->json([
            'success' => true,
            'message' => 'usuario creado correctamente',
            'data' => $rspta
        ]);
    }


    public function buscar(Request $request,$id)
    {

        $usuario = $this->usuarioService->buscarUsuario($id);
        return response()->json([
            'success' => true,
            'data' => $usuario
        ]);
    }

    public function editar(Request $request,$id)
    {
        $usuariologin = Auth::user();
        $usaurio = $request->all();
        $rspta = $this->usuarioService->editarUsuario($id,$usaurio,$usuariologin->id_usuario);
        if ($rspta == null) {
            return response()->json([
                'success' => false,
                'message' => 'Error al editar el usuario'
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => 'usuario editado correctamente',
            'data' => $rspta
        ]);
    }

    public function buscarme(Request $request)
    {
        $rspta = $this->usuarioService->buscarmeUsuario();

        if ($rspta == null) {
            return response()->json([
                'success' => true,
                'message' => 'error al retornar el usuario'
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => 'usuario retornado correctamente',
            'data' => $rspta
        ]);
    }

    public function actualizarme(Request $request)
    {
        $boolImg = $request->file('imagen_perfil');
        $usuario = $request->all();
        $rspta = $this->usuarioService->actualizarmeUsuario($usuario,$request);

        if ($rspta == null) {
            return response()->json([
                'success' => true,
                'message' => 'error al retornar el usuario'
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => 'usuario retornado correctamente',
            'data' => $rspta
        ]);
    }
}
