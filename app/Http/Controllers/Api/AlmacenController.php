<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\AlmacenServiceInterface;
use Illuminate\Support\Facades\Auth;

class AlmacenController extends Controller
{
    private $almacenService;

    public function __construct(
        AlmacenServiceInterface $almacenService
    )
    {
        $this->almacenService = $almacenService;
    }

    public function listar(Request $request)
    {
        $parametros = $request->all();
        $almacenes = $this->almacenService->listarAlmacen($parametros);


        return response()->json([
            'success' => true,
            'mensaje' => 'Almacenes retornados correctamente',
            'data' => $almacenes,
        ]);
    }

    public function listar_ris()
    {

        $almacenes = $this->almacenService->listarRis();


        return response()->json([
            'success' => true,
            'mensaje' => 'Ris retornados correctamente',
            'data' => $almacenes,
        ]);
    }



    public function listar_ris_combo()
    {

        $almacenes = $this->almacenService->listarRis();


        return response()->json([
            'success' => true,
            'mensaje' => 'Ris retornados correctamente',
            'data' => $almacenes,
        ]);
    }

    public function listar_filtrado(Request $request)
    {

        $usuario = Auth::user();
        $parametros = $request->all();

        $almacenes = $this->almacenService->listarAlmacenFiltrado($parametros,$usuario);


        return response()->json([
            'success' => true,
            'mensaje' => 'Almacenes filtrado retornados correctamente',
            'data' => $almacenes,
        ]);
    }



    public function listar_almacen_combo(Request $request)
    {
        $parametros = $request->all();
        $almacenes = $this->almacenService->listarAlmacen($parametros);


        return response()->json([
            'success' => true,
            'mensaje' => 'Almacenes retornados correctamente',
            'data' => $almacenes,
        ]);
    }
}
