<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\PermisoTotalServiceInterface;

class PermisoTotalController extends Controller
{
    private $permisototalService;

    public function __construct(
        PermisoTotalServiceInterface $permisototalService
    )
    {
        $this->permisototalService = $permisototalService;
    }

    public function buscarPorUsuario(Request $request,$id)
    {
        $parametros = $request->all();
        $permisoTotal = $this->permisototalService->buscarPorUsuarioPermisoTotal($id,$parametros);

        return response()->json([
            'success' => true,
            'data' => $permisoTotal
        ]);
    }

    public function crear(Request $request,$id_usuario)
    {

        $permisototal = $request->all();


        $permisoTotalcreate = $this->permisototalService->crearPermisoTotal($permisototal,$id_usuario);

        return response()->json([
            'success' => true,
            'message' => 'Permisos creados correctamente',
            'data' => json_decode($permisoTotalcreate,true)
        ]);
    }

    public function eliminar(Request $request)
    {
        $arrPermisototalIds = $request->all();

        $permisoTotalEliminados = $this->permisototalService->eliminarPermisosTotales($arrPermisototalIds);

        return response()->json([
            'success' => true,
            'message' => 'Permisos eliminados correctamente',
            'data' => $permisoTotalEliminados
        ]);
    }

    public function activar(Request $request)
    {
        $arrPermisototalIds = $request->all();

        $permisoTotalActivados = $this->permisototalService->activarPermisosTotales($arrPermisototalIds);

        return response()->json([
            'success' => true,
            'message' => 'Permisos activar correctamente',
            'data' => $permisoTotalActivados
        ]);
    }

    public function desactivar(Request $request)
    {
        $arrPermisototalIds = $request->all();

        $permisoTotalDesactivados = $this->permisototalService->desactivarPermisosTotales($arrPermisototalIds);

        return response()->json([
            'success' => true,
            'message' => 'Permisos desactivar correctamente',
            'data' => $permisoTotalDesactivados
        ]);
    }

    public function listar_permiso_total_x_usuario(Request $request,$id)
    {
        $parametros = $request->all();
        $permisoTotal = $this->permisototalService->buscarPorUsuarioPermisoTotal($id,$parametros);

        return response()->json([
            'success' => true,
            'data' => $permisoTotal
        ]);
    }



}
