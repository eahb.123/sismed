<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\SubmoduloServiceInterface;
use Illuminate\Support\Facades\Auth;

class SubmoduloController extends Controller
{
    private $submoduloService;

    public function __construct(
        SubmoduloServiceInterface $submoduloService
    )
    {
        $this->submoduloService = $submoduloService;
    }

    public function buscarpormodulo(Request $request,$id_modulo)
    {
        $submodulos = $this->submoduloService->buscarSubmoduloPorModulo($id_modulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Submodulos retornados correctamente',
            'data' => $submodulos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function listar(Request $request)
    {
        $parametros = $request->all();
        $submodulos = $this->submoduloService->listarSubmodulo($parametros);

        return response()->json([
            'success' => true,
            'data' => $submodulos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function crear(Request $request)
    {
        $usuario = Auth::user();

        $submodulo = $request->all();
        $submodulorsp = $this->submoduloService->crearSubmodulo($submodulo,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $submodulorsp
        ]);
    }

    public function eliminar(Request $request,$id)
    {
        $usuario = Auth::user();

        $submodulodl = $this->submoduloService->eliminarSubmodulo($id,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $submodulodl
        ]);
    }

    public function editar(Request $request,$id)
    {
        $usuario = Auth::user();

        $submodulo = $request->all();
        $submodulorsp = $this->submoduloService->actualizarSubmodulo($id,$submodulo,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $submodulorsp
        ]);
    }

    public function lista_submodulo_combo(Request $request,$id_modulo)
    {
        $submodulos = $this->submoduloService->buscarSubmoduloPorModulo($id_modulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Submodulos retornados correctamente',
            'data' => $submodulos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }
}
