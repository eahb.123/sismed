<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\StockServiceInterface;
use Carbon\Carbon;

class StockController extends Controller
{
    private $stockService;

    public function __construct(
        StockServiceInterface $stockService
    )
    {
        $this->stockService = $stockService;
    }

    public function listar(Request $request)
    {
        $parametros = $request->all();

        $stock = $this->stockService->listarStockMedicamento($parametros);
        if (isset($parametros["cantidad"])) {
            $paginas = round(($stock["resultsLength"]/$parametros["cantidad"])+0.5,0);
        }else{
            $paginas = 0;
            $parametros["pagina"] = 1;
            $parametros["cantidad"] = $stock["resultsLength"];
        }


        return response()->json([
            'success' => true,
            'mensaje' => 'stock retornados correctamente',
            'resultado' => $stock["data"],
            'actual_pagina' => $parametros["pagina"],
            'ultima_pagina' => $paginas,
            'por_pagina' => $parametros["cantidad"],
            'total' => $stock["resultsLength"],
        ]);
    }

    public function listar_detalle(Request $request)
    {
        $parametros = $request->all();

        $stockDetalle = $this->stockService->listarDetalleStockMedicamento($parametros);



        return response()->json([
            'success' => true,
            'mensaje' => 'detalle stock retornados correctamente',
            'resultado' => $stockDetalle
        ]);
    }

    public function reporte_listar_detalle_stock_medicamento(Request $request)
    {
        $parametros = $request->all();

        $stockDetalle = $this->stockService->ReportelistarDetalleStockMedicamento($parametros);



        return response()->json([
            'success' => true,
            'mensaje' => 'reporte detallado stock retornados correctamente',
            'resultado' => $stockDetalle
        ]);
    }

    public function cuadro_resumen(Request $request)
    {

        $parametros = $request->all();

        $stockDetalle = $this->stockService->CuadroResumen($parametros);



        return response()->json([
            'success' => true,
            'mensaje' => 'reporte cuadro resumen retornados correctamente',
            'resultado' => $stockDetalle
        ]);
    }

    public function detalle_cuadro_resumen(Request $request)
    {

        $parametros = $request->all();

        $stockDetalle = $this->stockService->DetalleCuadroResumen($parametros);



        return response()->json([
            'success' => true,
            'mensaje' => 'reporte detalle cuadro resumen retornados correctamente',
            'resultado' => $stockDetalle
        ]);
    }

    public function lotes_por_vencer(Request $request)
    {

        $parametros = $request->all();

        $lotesPorVencer = $this->stockService->LotesPorVencer($parametros);

        if (isset($parametros["cantidad"])) {
            $paginas = round(($lotesPorVencer["resultsLength"]/$parametros["cantidad"])+0.5,0);
        }else{
            $paginas = 0;
            $parametros["pagina"] = 1;
            $parametros["cantidad"] = $lotesPorVencer["resultsLength"];
        }

        return response()->json([
            'success' => true,
            'mensaje' => 'lotes por vencer retornados correctamente',
            'resultado' => $lotesPorVencer["data"],
            'actual_pagina' => $parametros["pagina"],
            'ultima_pagina' => $paginas,
            'por_pagina' => $parametros["cantidad"],
            'total' => $lotesPorVencer["resultsLength"],
        ]);
    }


    public function consulta_ici_diario(Request $request)
    {
        $fechaInput = $request->input('date');
        $arrAlmacen = $request->input('arrAlmacen');
        $fechaCarbon = Carbon::createFromFormat('Y-m-d', $fechaInput);

        //MES DE LA FECHA
        $fechames = $fechaCarbon->format('m');    //01

        //AÑO DE LA FECHA
		$fechaanio = $fechaCarbon->format('Y');    //2023

		//FORMATEO DE LA FECHA
		$fech = $fechaCarbon->format('d-m-Y');  //17-01-2023

        //FECHA PRIMER DIA DEL MES ACTUAL
        $fec=$fechaCarbon->format('01-m-Y'); //01-01-2023

        //JUNTA TODA LA FECHA
        $dat=$fechaCarbon->format('Ymd'); //20230117

        //RESTO 11 MESES PARA LA FECHA INICIAL
        $monthIni = $fechaCarbon->copy()->subMonths(12)->format('Ym');
        //$monthIni;

        //RESTO 1 MES PARA LA FECHA FINAL
        $monthEnd = $fechaCarbon->copy()->subMonths(1)->format('Y-m-d');

        //AÑO M($monthIni);ES JUNTO
		$monthFin=$fechaCarbon->format('Ym');
        //FECHA INICIAL
		$fi=$fechaCarbon->copy()->subMonths(12)->format('Y-m-01');
		$ff=$fechaCarbon->format('Y-m-d');

        $cadenaAlmacen = implode("'', ''", $arrAlmacen);
        $sql="SELECT * FROM ici_diario_consulta('$fi'::text, '$ff'::text,$monthIni::int,$monthFin::int,".$monthFin."::int,''(''$cadenaAlmacen'')''::text);";

        $lotesPorVencer = $this->stockService->LotesPorVencer($parametros);

    }

}
