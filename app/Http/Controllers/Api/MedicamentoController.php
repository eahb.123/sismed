<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Medicamento;


class MedicamentoController extends Controller
{
    public function listar(Request $request)
    {

        $parametros = $request->all();
        if (isset($parametros["search"])) {
            $search = strtoupper($parametros["search"]);
            $medicamento = Medicamento::
            where('nombre', 'like', "%$search%")
            ->orWhere('id_medicamento', 'like', "%$search%")
            ->limit(10)
            ->get();

        }else{
            $medicamento = Medicamento::
            limit(10)
            ->get();

        }



        return response()->json([
            'success' => true,
            'mensaje' => 'Medicamento retornados correctamente',
            'data' => $medicamento,
        ]);
    }

    public function listar_medicamento_combo(Request $request)
    {

        $parametros = $request->all();
        if (isset($parametros["search"])) {
            $search = strtoupper($parametros["search"]);
            $medicamento = Medicamento::
            where('nombre', 'like', "%$search%")
            ->orWhere('id_medicamento', 'like', "%$search%")
            ->orderBy('nombre','asc')
            ->limit(200)
            ->get();

        }else{
            $medicamento = Medicamento::
            limit(200)
            ->orderBy('nombre','asc')
            ->get();

        }



        return response()->json([
            'success' => true,
            'mensaje' => 'Medicamento retornados correctamente',
            'data' => $medicamento,
        ]);
    }
}
