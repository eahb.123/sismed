<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\GeneralServiceInterface;
use DateTime;

class GeneralController extends Controller
{
    private $generalService;

    public function __construct(
        GeneralServiceInterface $generalService
    )
    {
        $this->generalService = $generalService;
    }

    public function listar_estrategia_combo(Request $request)
    {
        $parametros = $request->all();

        $progrEstrategica = $this->generalService->listarEstrategia($parametros);


        //$array = $parametros["arrAlmacen"];
        //$cadena = implode("'',''", $array);
        //$cadena = "'''" . $cadena . "'''";
        return response()->json([
            'success' => true,
            'mensaje' => 'Programacion Estrategica',
            'resultado' => $progrEstrategica
        ]);
    }

}
