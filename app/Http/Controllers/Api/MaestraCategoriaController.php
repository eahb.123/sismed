<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\MaestraCategoriaServiceInterface;


class MaestraCategoriaController extends Controller
{
    private $maestraCategoriaService;

    public function __construct(
        MaestraCategoriaServiceInterface $maestraCategoriaService
    )
    {
        $this->maestraCategoriaService = $maestraCategoriaService;
    }

    public function lista_maestra_categoria(Request $request)
    {

        $parametros = $request->all();
        $maestraCategoria = $this->maestraCategoriaService->listarMaestraCategoria($parametros);


        return response()->json([
            'success' => true,
            'mensaje' => 'listado maestra categoria retornados correctamente',
            'resultado' => $maestraCategoria
        ]);
    }

    public function consulta_categoria_maestra_medicamento(Request $request)
    {
        $parametros = $request->all();

        $categoria = $parametros["categoria"];
        $ris = isset($parametros["ris"]) ? $parametros["ris"] : '' ;

        $kitCategoria = $this->maestraCategoriaService->consultaCategoriaMaestraMedicamento($categoria, $ris);

        $primerObjeto = $kitCategoria[0];
        $propiedades = array_keys(get_object_vars($primerObjeto));

        // Crear un nuevo array para almacenar las propiedades filtradas
        $propiedadesFiltradas = [];

        // Iterar sobre las propiedades
        foreach ($propiedades as $indice => $propiedad) {
            // Mantener los primeros tres elementos y los elementos en posiciones pares
            if ($indice <= 2 || $indice % 2 == 0) {
                $propiedadesFiltradas[] = $propiedad;
            }
        }


        return response()->json([
            'success' => true,
            'mensaje' => 'kit categoria categoria retornados correctamente',
            'resultado' => $kitCategoria,
            'cabecera' => $propiedadesFiltradas
        ]);
    }

    public function botiquin_categoria_medicamento(Request $request)
    {
        $parametros = $request->all();
        $maestraCategoria = $this->maestraCategoriaService->botiquin_categoria_medicamento($parametros);
        return response()->json([
            'success' => true,
            'mensaje' => 'listado maestra categoria retornados correctamente',
            'resultado' => $maestraCategoria
        ]);
    }

    public function lista_documento_identidad_combo(Request $request)
    {
        $parametros = $request->all();
        $maestraCategoria = $this->maestraCategoriaService->lista_documento_identidad_combo();
        return response()->json([
            'success' => true,
            'mensaje' => 'listado maestra categoria retornados correctamente',
            'resultado' => $maestraCategoria
        ]);
    }

    public function listar_kit_escenario_iii(Request $request)
    {
        $parametros = $request->all();


        $kitCategoria = $this->maestraCategoriaService->listar_kit_escenario_iii();
        $primerObjeto = $kitCategoria[0];
        $propiedades = array_keys(get_object_vars($primerObjeto));

        // Crear un nuevo array para almacenar las propiedades filtradas
        $propiedadesFiltradas = [];

        // Crear el array para la primera línea (cabeceras)
        $contador = 0;
        $primeraLinea = [];
        $titulo1 = "";
        $contadorTitulo1 = 0;
        $segundaLinea = [];
        $titulo2 = "";
        $contadorTitulo2 = 0;
        $terceraLinea = [];
        $titulo3 = "";
        $contadorTitulo3 = 0;
        $cuartaLinea = [];
        $titulo4 = "";
        $contadorTitulo4 = 0;
        foreach ($propiedades as $value) {

            //////////////////////////////////////////////////////////////////
            //PRIMERA FILA ///////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            if ($contador<=3) {
                $primeraLinea[] = [
                    'colspan' => 1,
                    'rowspan' => 4,
                    'valor' => $value
                ];
            }
            if ($contador>3 && $contador % 2 == 0) {
                $array = explode('_', $value);

                if ($titulo1!="" && $contadorTitulo1>0 && $titulo1!=$array[2]) {
                    $primeraLinea[] = [
                        'colspan' => $contadorTitulo1*2,
                        'rowspan' => 1,
                        'valor' => $titulo1
                    ];

                    $contadorTitulo1=0;
                    $titulo1=$array[2];
                }


                    $titulo1=$array[2];

                $contadorTitulo1++;

            }
            //////////////////////////////////////////////////////////////////
            //FIN PRIMERA FILA ///////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //SEGUNDA FILA ///////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////

            if ($contador>3 && $contador % 2 == 0) {
                $array = explode('_', $value);

                if ($titulo2!="" && $contadorTitulo2>0 && $titulo2!=$array[1]) {
                    $segundaLinea[] = [
                        'colspan' => 2,
                        'rowspan' => 1,
                        'valor' => $titulo2
                    ];

                    $contadorTitulo2=0;
                    $titulo2=$array[1];
                }


                    $titulo2=$array[1];

                $contadorTitulo2++;

            }
            //////////////////////////////////////////////////////////////////
            //FIN SEGUNDA FILA ///////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //TERCERA FILA ///////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////

            if ($contador>3 && $contador % 2 == 0) {
                $array = explode('_', $value);

                if ($titulo3!="" && $contadorTitulo3>0 && $titulo3!=$array[3]) {
                    $terceraLinea[] = [
                        'colspan' => 2,
                        'rowspan' => 1,
                        'valor' => $titulo3
                    ];

                    $contadorTitulo3=0;
                    $titulo3=$array[3];
                }


                    $titulo3=$array[3];

                $contadorTitulo3++;

            }
            //////////////////////////////////////////////////////////////////
            //FIN TERCERA FILA ///////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            //CUARTA FILA ///////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////

            if ($contador>3 && $contador % 2 == 0) {
                $array = explode('_', $value);

                if ($array[0]=='M') {
                    $cuartaLinea[] = [
                        'colspan' => 1,
                        'rowspan' => 1,
                        'valor' => 'MINIMO'
                    ];
                    $cuartaLinea[] = [
                        'colspan' => 1,
                        'rowspan' => 1,
                        'valor' => 'STOCK'
                    ];
                }

                $contadorTitulo4++;

            }
            //////////////////////////////////////////////////////////////////
            //FIN CUARTA FILA ///////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
            $contador++;
        }

        //////////////////////////////////////////////////////////////////
        //PRIMERA FILA ///////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        $primeraLinea[] = [
            'colspan' => $contadorTitulo1*2,
            'rowspan' => 1,
            'valor' => $titulo1
        ];
        //////////////////////////////////////////////////////////////////
        //FIN PRIMERA FILA ///////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //SEGUNDA FILA ///////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        $segundaLinea[] = [
            'colspan' => 2,
            'rowspan' => 1,
            'valor' => $titulo2
        ];
        //////////////////////////////////////////////////////////////////
        //FIN SEGUNDA FILA ///////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //TERCERA FILA ///////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        $terceraLinea[] = [
            'colspan' => 2,
            'rowspan' => 1,
            'valor' => $titulo3
        ];
        //////////////////////////////////////////////////////////////////
        //FIN TERCERA FILA ///////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////

                // Iterar sobre las propiedades
        foreach ($propiedades as $indice => $propiedad) {
            $propiedadesFiltradas[] = $propiedad;
        }

        return response()->json([
            'success' => true,
            'mensaje' => 'kit categoria categoria retornados correctamente',
            'resultado' => $kitCategoria,
            'primera' => $primeraLinea,
            'segunda' => $segundaLinea,
            'tercera' => $terceraLinea,
            'cuarta' => $cuartaLinea,
            'cabecera' => $propiedadesFiltradas
        ]);






        foreach ($kitCategoria as $key => $value) {
            $linea = [];
            $contador = 0;
            foreach ($value as $key1 => $value1) {

                if ($contador<=0) {
                    $primeraLinea[] = [
                        'rowspan' => 4,
                        'colspan' => 1,
                        'valor' => $value1
                    ];
                }
                $contador++;
            }
        }







        return response()->json([
            'success' => true,
            'mensaje' => 'kit categoria categoria retornados correctamente',
            'resultado' => $kitCategoria,
            'cabecera' => $propiedadesFiltradas
        ]);
    }


}
