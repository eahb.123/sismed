<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\ModuloServiceInterface;
use Illuminate\Support\Facades\Auth;

class ModuloController extends Controller
{
    //buscarporsprmodulo
    private $moduloService;

    public function __construct(
        ModuloServiceInterface $moduloService
    )
    {
        $this->moduloService = $moduloService;
    }

    public function listar(Request $request)
    {
        $parametros = $request->all();
        $modulos = $this->moduloService->listarModulo($parametros);
        return response()->json([
            'success' => true,
            'mensaje' => 'Modulos retornados correctamente',
            'data' => $modulos,
        ]);
    }

    public function crear(Request $request)
    {
        $usuario = Auth::user();

        $modulo = $request->all();
        $modulorsp = $this->moduloService->crearModulo($modulo,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $modulorsp
        ]);
    }

    public function buscarporsprmodulo(Request $request,$id_supermodulo)
    {
        $modulos = $this->moduloService->buscarModuloPorSprmodulo($id_supermodulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Modulos retornados correctamente',
            'data' => $modulos,
        ]);
    }

    public function eliminar(Request $request,$id)
    {
        $usuario = Auth::user();

        $modulodl = $this->moduloService->eliminarModulo($id,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $modulodl
        ]);
    }

    public function editar(Request $request,$id)
    {
        $usuario = Auth::user();

        $modulo = $request->all();
        $modulorsp = $this->moduloService->actualizarModulo($id,$modulo,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $modulorsp
        ]);
    }

    public function lista_modulo_combo(Request $request,$id_supermodulo)
    {
        $modulos = $this->moduloService->buscarModuloPorSprmodulo($id_supermodulo);
        return response()->json([
            'success' => true,
            'mensaje' => 'Modulos retornados correctamente',
            'data' => $modulos,
        ]);
    }

}
