<?php
// app/Http/Controllers/Api/TblEmpresasApiController.php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\EmpresaServiceInterface;

class TblEmpresasApiController extends Controller
{
    private $empresaService;

    public function __construct(EmpresaServiceInterface $empresaService)
    {
        $this->empresaService = $empresaService;
    }

    public function index()
    {
        $empresas = $this->empresaService->getAllEmpresas();
        return response()->json($empresas);
    }

    public function show($id)
    {
        $empresa = $this->empresaService->getEmpresaById($id);

        if (!$empresa) {
            return response()->json(['message' => 'Empresa no encontrada'], 404);
        }

        return response()->json($empresa);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string',
        ]);

        $empresa = $this->empresaService->createEmpresa($request->all());

        return response()->json($empresa, 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string',
        ]);

        $empresa = $this->empresaService->updateEmpresa($id, $request->all());

        return response()->json($empresa, 200);
    }

    public function destroy($id)
    {
        $this->empresaService->deleteEmpresa($id);

        return response()->json(['message' => 'Empresa eliminada correctamente'], 204);
    }
}
