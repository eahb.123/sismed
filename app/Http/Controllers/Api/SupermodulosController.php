<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\SupermoduloServiceInterface;
use Illuminate\Support\Facades\Auth;


class SupermodulosController extends Controller
{

    private $supermoduloService;

    public function __construct(
        SupermoduloServiceInterface $supermoduloService
    )
    {
        $this->supermoduloService = $supermoduloService;
    }

    public function listar()
    {
        $supermodulos = $this->supermoduloService->listarSupermodulo();

        return response()->json([
            'success' => true,
            'data' => $supermodulos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }

    public function reordenar(Request $request)
    {
        $supermodulos = $request->all()["supermodulo"];

        $supermodulos = $this->supermoduloService->reordenarSupermodulo($supermodulos);

        return response()->json([
            'success' => true,
            'data' => $supermodulos
        ]);
    }

    public function cambiarEstado(Request $request)
    {
        $supermodulo = $request->all()["supermodulo"];
        //$supermodulos = $request->all();

        $supermodulorsp = $this->supermoduloService->cambiarEstadoSupermodulo($supermodulo);


        return response()->json([
            'success' => true,
            'data' => $supermodulorsp
        ]);
    }

    public function crear(Request $request)
    {
        $supermodulo = $request->all()["supermodulo"];
        $supermodulorsp = $this->supermoduloService->crearSupermodulo($supermodulo);
        return response()->json([
            'success' => true,
            'data' => $supermodulo
        ]);
    }

    public function eliminar(Request $request,$id)
    {
        $usuario = Auth::user();

        $supermodulodl = $this->supermoduloService->eliminarSupermodulo($id,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $supermodulodl
        ]);
        //$supermodulos = $request->all();

        $supermodulorsp = $this->supermoduloService->cambiarEstadoSupermodulo($supermodulo);


        return response()->json([
            'success' => true,
            'data' => $supermodulorsp
        ]);
    }

    public function editar(Request $request,$id)
    {
        $usuario = Auth::user();

        $supermodulo = $request->all()["supermodulo"];
        $supermodulorsp = $this->supermoduloService->actualizarSupermodulo($id,$supermodulo,$usuario->id_usuario);
        return response()->json([
            'success' => true,
            'data' => $supermodulorsp
        ]);
        //$supermodulos = $request->all();
    }

    public function lista_supermodulo_combo()
    {
        $supermodulos = $this->supermoduloService->listarSupermoduloCombo();

        return response()->json([
            'success' => true,
            'data' => $supermodulos,
            //'current_page' => $users->currentPage(),
            //'last_page' => $users->lastPage(),
            //'per_page' => $users->perPage(),
            //'total' => $users->total(),
        ]);
    }
}
