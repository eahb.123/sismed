<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\LibroDeControlServiceInterface;
use DateTime;

class LibroDeControlController extends Controller
{
    private $libroDeControlService;

    public function __construct(
        LibroDeControlServiceInterface $libroDeControlService
    )
    {
        $this->libroDeControlService = $libroDeControlService;
    }

    public function libro_de_control(Request $request)
    {
        $parametros = $request->all();
        $desde = new DateTime($parametros["start"]);
        $hasta = new DateTime($parametros["end"]);

        $local = $parametros["selectedAlmacen"];
        $libroDeControl = $parametros["libroDeControl"];


        $rspLibroDeControl = $this->libroDeControlService->libroDeControl($desde,$hasta,$local,$libroDeControl);

        return response()->json([
            'success' => true,
            'mensaje' => 'libro de control retornados correctamente',
            'resultado' => $rspLibroDeControl
        ]);
    }

}
