<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\IciServiceInterface;
use DateTime;

class IciController extends Controller
{
    private $iciService;

    public function __construct(
        IciServiceInterface $iciService
    )
    {
        $this->iciService = $iciService;
    }

    public function ici_diario(Request $request)
    {
        $parametros = $request->all();

        $iciDiario = $this->iciService->ici_diario($parametros);

        //$array = $parametros["arrAlmacen"];
        //$cadena = implode("'',''", $array);
        //$cadena = "'''" . $cadena . "'''";
        return response()->json([
            'success' => true,
            'mensaje' => 'ici diario',
            'resultado' => $iciDiario
        ]);

    }

    public function ici_estrategico(Request $request)
    {
        $parametros = $request->all();

        $iciEstrategico = $this->iciService->ici_estrategico($parametros);

        return response()->json([
            'success' => true,
            'mensaje' => 'ici diario',
            'resultado' => $iciEstrategico
        ]);
    }

    public function ici_por_producto(Request $request)
    {
        $parametros = $request->all();

        $iciEstrategico = $this->iciService->ici_por_producto($parametros);

        return response()->json([
            'success' => true,
            'mensaje' => 'ici diario',
            'resultado' => $iciEstrategico
        ]);
    }

}
