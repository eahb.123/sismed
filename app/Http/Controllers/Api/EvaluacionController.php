<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\EvaluacionServiceInterface;
use Illuminate\Support\Facades\Auth;

class EvaluacionController extends Controller
{
    private $evaluacionService;

    public function __construct(
        EvaluacionServiceInterface $evaluacionService
    )
    {
        $this->evaluacionService = $evaluacionService;
    }

    public function buscar_evaluacion(Request $request,$id_evaluacion)
    {
        $parametros = $request->all();
        $evaluacion = $this->evaluacionService->buscar_evaluacion($id_evaluacion);

        return response()->json([
            'success' => true,
            'mensaje' => 'evaluacion retornada correctamente',
            'data' => $evaluacion,
        ]);

    }

    public function preguntas_x_evaluacion(Request $request,$id_evaluacion)
    {
        $parametros = $request->all();
        $evaluacion = $this->evaluacionService->preguntas_x_evaluacion($id_evaluacion);


        return response()->json([
            'success' => true,
            'mensaje' => 'evaluacion retornada correctamente',
            'data' => $evaluacion,
        ]);
    }

    public function buscar_x_documento(Request $request,$tipo_documento,$numero_documento)
    {

        $persona = $this->evaluacionService->buscar_x_documento($tipo_documento,$numero_documento);



        return response()->json([
            'success' => true,
            'mensaje' => 'persona retornada correctamente',
            'data' => $persona,
        ]);

    }


    public function guardar_actualizar_persona(Request $request)
    {
        $parametros = $request->all();
        $persona = $this->evaluacionService->guardar_actualizar_persona($parametros);
            return response()->json([
                'success' => true,
                'mensaje' => 'actualizado correctamente',
                'data' => $persona
            ]);
    }


    public function enviar_prespuesta(Request $request)
    {

        $parametros = $request->all();
        $confirmarespuesta = $this->evaluacionService->enviar_prespuesta($parametros);


            return response()->json([
                'success' => $confirmarespuesta["success"],
                'mensaje' => $confirmarespuesta["mensaje"]
            ]);
    }

    public function evaluacion_listar(Request $request)
    {

        $parametros = $request->all();
        $evaluaciones = $this->evaluacionService->evaluacion_listar();

        return response()->json([
            'success' => true,
            'mensaje' => 'lista de evaluaciones',
            'data' => $evaluaciones
        ]);
    }

    public function combo_estado(Request $request)
    {

        $parametros = $request->all();
        $estado = $this->evaluacionService->combo_estado();

        return response()->json([
            'success' => true,
            'mensaje' => 'lista de estado evaluacion',
            'data' => $estado
        ]);
    }

    public function combo_tipo(Request $request)
    {

        $parametros = $request->all();
        $tipo = $this->evaluacionService->combo_tipo();

        return response()->json([
            'success' => true,
            'mensaje' => 'lista de tipo evaluacion',
            'data' => $tipo
        ]);
    }

    public function combo_tipo_pregunta(Request $request)
    {

        //$parametros = $request->all();
        $tipo = $this->evaluacionService->combo_tipo_pregunta();

        return response()->json([
            'success' => true,
            'mensaje' => 'lista de tipo pregunta',
            'data' => $tipo
        ]);
    }


    public function crear_evaluacion(Request $request)
    {

        $parametros = $request->all();
        $evaluacion = $this->evaluacionService->crear_evaluacion($parametros);
            return response()->json([
                'success' => true,
                'mensaje' => 'actualizado correctamente',
                'data' => $evaluacion
            ]);
    }


    public function buscar_evaluacion_x_id(Request $request)
    {

        $parametros = $request->all();
        $evaluacion = $this->evaluacionService->buscar_evaluacion($parametros);
            return response()->json([
                'success' => true,
                'mensaje' => 'actualizado correctamente',
                'data' => $evaluacion
            ]);
    }

    public function guardar_pregunta_respuesta(Request $request)
    {


        $parametros = $request->all();
        $evaluacion = $this->evaluacionService->guardar_pregunta_respuesta($parametros["parametros"]);

        if ($evaluacion) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al actualizar',
            ]);
        }

    }

    public function preguntas_abiertas_x_persona(Request $request,$id_evaluacion)
    {
        $evaluacion = $this->evaluacionService->preguntas_abiertas_x_persona($id_evaluacion);

        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
                'data' => $evaluacion["data"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }


    public function guardar_preguntas_abiertas(Request $request,$id_evaluacion)
    {

        $respuestas=$request->all();
        $respuestas=$respuestas['preguntas'];
        $evaluacion = $this->evaluacionService->guardar_preguntas_abiertas($id_evaluacion,$respuestas);


        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }


    public function calcular_resultados_evaluacion(Request $request,$id_evaluacion)
    {

        $evaluacion = $this->evaluacionService->calcular_resultados_evaluacion($id_evaluacion);



        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }


    public function resultados_evaluacion(Request $request,$id_evaluacion)
    {
        $evaluacion = $this->evaluacionService->resultados_evaluacion($id_evaluacion);

        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
                'data' => $evaluacion["data"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }


    public function iniciar_evaluacion(Request $request,$id_evaluacion)
    {
        $evaluacion = $this->evaluacionService->iniciar_evaluacion($id_evaluacion);


        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }

    public function cerrado_evaluacion(Request $request,$id_evaluacion)
    {
        $evaluacion = $this->evaluacionService->cerrado_evaluacion($id_evaluacion);

        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],

            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }

    public function finalizado_evaluacion(Request $request,$id_evaluacion)
    {
        $evaluacion = $this->evaluacionService->finalizado_evaluacion($id_evaluacion);


        if (isset($evaluacion["success"]) && $evaluacion["success"]) {
            return response()->json([
                'success' => $evaluacion["success"],
                'mensaje' => $evaluacion["mensaje"],
            ]);
        }else{
            return response()->json([
                'success' => false,
                'mensaje' => 'error al traer la informacion',
            ]);
        }

    }

}
