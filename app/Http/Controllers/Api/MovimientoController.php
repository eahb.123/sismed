<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\MovimientoServiceInterface;

class MovimientoController extends Controller
{
    private $movimientoService;

    public function __construct(
        MovimientoServiceInterface $movimientoService
    )
    {
        $this->movimientoService = $movimientoService;
    }

    public function listar(Request $request)
    {

        $parametros = $request->all();
        $movimientos = $this->movimientoService->listarMovimientos($parametros);

        if (isset($parametros["cantidad"])) {
            $paginas = round(($movimientos["resultsLength"]/$parametros["cantidad"])+0.5,0);
        }else{
            $paginas = 0;
            $parametros["pagina"] = 1;
            $parametros["cantidad"] = $movimientos["resultsLength"];
        }

        return response()->json([
            'success' => true,
            'mensaje' => 'lotes por vencer retornados correctamente',
            'resultado' => $movimientos["data"],
            'actual_pagina' => $parametros["pagina"],
            'ultima_pagina' => $paginas,
            'por_pagina' => $parametros["cantidad"],
            'total' => $movimientos["resultsLength"],
        ]);
    }

    public function listar_almacen(Request $request)
    {

        $parametros = $request->all();
        $movimientos = $this->movimientoService->listarMovimientosAlmacen($parametros);


        if (isset($parametros["cantidad"])) {
            $paginas = round(($movimientos["resultsLength"]/$parametros["cantidad"])+0.5,0);
        }else{
            $paginas = 0;
            $parametros["pagina"] = 1;
            $parametros["cantidad"] = $movimientos["resultsLength"];
        }

        return response()->json([
            'success' => true,
            'mensaje' => 'lotes por vencer retornados correctamente',
            'resultado' => $movimientos["data"],
            'actual_pagina' => $parametros["pagina"],
            'ultima_pagina' => $paginas,
            'por_pagina' => $parametros["cantidad"],
            'total' => $movimientos["resultsLength"],
        ]);
    }

}
