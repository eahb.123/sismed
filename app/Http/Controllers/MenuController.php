<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Services\Contracts\MenuServiceInterface;


class MenuController extends Controller
{
    private $menuService;

    public function __construct(MenuServiceInterface $menuService)
    {
        $this->menuService = $menuService;
    }

    public function listarMenu(Request $request)
    {
        $usuario = JWTAuth::parseToken()->authenticate();

        $resultados = $this->menuService->listarMenu($usuario);


        // Convertir la colección a un array
        $arrayResultados = $resultados->toArray();


        $datos = $arrayResultados;
        // Convertir los objetos stdClass en un array asociativo
        $datos = collect($datos)->map(function ($item) {
            return (array) $item;
        });

        // Agrupar por supermodulonombre, modulonombre y submodulonombre
        $agrupados = $datos->groupBy(['supermodulo', 'modulo', 'submodulo']);
        // Convertir la colección agrupada de nuevo a formato de array
        $agrupados = $agrupados->toArray();
        $ruta_modulo=null;
        // Construir la estructura deseada
        $resultado = collect($agrupados)->map(function ($items, $supermodulonombre) {
            $modulos = collect($items)->map(function ($modulos, $modulonombre) {
                $ruta_modulo = null; // Inicializar la variable fuera del bucle interno

                $submodulos = collect($modulos)->map(function ($submodulos) use (&$ruta_modulo) {
                    // Aquí puedes asignar $ruta_modulo
                    $ruta_modulo = $submodulos[0]['ruta_modulo'];

                    return [
                        'nombre' => $submodulos[0]['submodulo'],
                        'ruta' => $submodulos[0]['ruta'],
                        'id_submodulo' => $submodulos[0]['id_submodulo']
                    ];
                })->values()->toArray();

                // Ahora puedes acceder a $ruta_modulo aquí
                return [
                    'nombre' => $modulonombre,
                    'submodulo' => $submodulos,
                    'ruta_modulo' => $ruta_modulo
                ];
            })->values()->toArray();

            return [
                'nombre' => $supermodulonombre,
                'modulo' => $modulos,
            ];
        })->values()->toArray();



        $resultadosx["supermodulo"] = $resultado;


        // Imprimir la estructura deseada
        //var_dump($resultado);
        //exit;
        // Convertir el array a formato JSON
        //$jsonAgrupado = json_encode($resultado, JSON_PRETTY_PRINT);


        return response()->json($resultadosx);
    }
}
