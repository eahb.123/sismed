<?php
// app/Http/Controllers/AuthController.php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Sesion;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Services\Contracts\SesionServiceInterface;


class AuthController extends Controller
{

    private $sesionService;

    public function __construct(SesionServiceInterface $sesionService)
    {
        $this->sesionService = $sesionService;
    }

    public function login(Request $request)
    {
        $request->validate([
            'correo_electronico' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('correo_electronico', 'password');
        $credentials['correo_electronico'] = strtolower($credentials['correo_electronico']); // Convertir a minúsculas


        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }


        $usuario = Auth::user()->load('persona');
        //$usuario = Usuario::findOrFail(1)->load('persona');
        //return response()->json(['token' => $token]);
        //$token = JWTAuth::fromUser($usuario);
        //$payload = JWTAuth::getPayload($token);

        $usuarioToSend = [
            'id_usuario' => $usuario->id_usuario,
            'nombre' => $usuario->persona->nombres,
            'ape_paterno' => $usuario->persona->ape_paterno,
            'ape_materno' => $usuario->persona->ape_materno,
            'correo_electronico' => $usuario->correo_electronico,
            'id_cargo' => $usuario->id_cargo,
            // Agrega aquí otros campos que desees enviar
        ];
        //crear la sesion
        //$sesion = $this->sesionService->createSesion($usuario,$token);
        return response()->json(['token' => $token,
                                'usuario' => $usuario]);
    }

    public function guardartoken(Request $request)
    {
        $token = $request->token;
        $usuario = JWTAuth::parseToken()->authenticate();
        $sesion = $this->sesionService->createSesion($usuario,$token);
        if (!$sesion) {
            return response()->json([
                'status' => 'error',
                'mensaje' => 'La sesion no fue creada'
            ], 401);
        }

        return response()->json([
            'status' => 'success',
            'mensaje' => 'Sesion creada exitosamente'
        ]);

    }


    public function checkauth(Request $request)
    {

        $usuario = JWTAuth::parseToken()->authenticate();
        $token = $request->token;
        $payload = JWTAuth::parseToken($token)->getPayload()->toArray();

        $route = $request->route;

        // if ($request->path() == 'api/checkauth') {
        //     return response()->json(['error' => '','success'=>'true','code'=>'200']);
        // }
        $sesion = $this->sesionService->ValidarSesionActiva($usuario,$token,$payload,$route);

        return $sesion;

        if ($sesion) {
            return response()->json([
                'success' => $sesion["success"],
                'mensaje' => 'Sesion creada exitosamente111'
            ]);
            return $sesion;
        } else {
            echo 'false';
        }

        //return array($sesion);
    }



    public function register(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string',
            'correo_electronico' => 'required|unique:usuarios',
            'password' => 'required|string|min:8',
        ]);

        $usuario = Usuario::create([
            'nombre' => $request->nombre,
            'correo_electronico' => strtolower($request->correo_electronico), // Convertir a minúsculas
            'password' => bcrypt($request->password),
        ]);

        return response()->json(['usuario' => $usuario, 'message' => 'Usuario registrado con éxito']);
    }
}
