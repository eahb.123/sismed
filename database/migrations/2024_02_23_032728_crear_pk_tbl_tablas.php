<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_tablas', function (Blueprint $table) {
            $table->id(); // Agrega un campo 'id' autoincrementable como clave primaria
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_tablas', function (Blueprint $table) {
            $table->dropColumn('id'); // Elimina el campo 'id' en caso de rollback
        });
    }
};
