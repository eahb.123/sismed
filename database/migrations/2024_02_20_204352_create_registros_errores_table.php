<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('registros_errores', function (Blueprint $table) {
            $table->id('id_error');
            $table->foreignId('id_usuario')->nullable()->constrained('usuarios', 'id_usuario');
            $table->timestamp('fecha_error')->useCurrent();
            $table->string('descripcion_error');
            $table->text('detalles_error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('registros_errores');
    }
};
