<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('permiso_total', function (Blueprint $table) {
            $table->id('id_permiso_total');
            $table->string('id_empresa')->nullable(); // Cambiamos el tipo de dato a string
            $table->unsignedBigInteger('id_ris')->nullable();
            $table->string('id_almacen')->nullable(); // Cambiamos el tipo de dato a string
            $table->foreignId('id_supermodulo')->constrained('supermodulos', 'id_supermodulo');
            $table->foreignId('id_modulo')->constrained('modulos', 'id_modulo');
            $table->foreignId('id_submodulo')->constrained('submodulos', 'id_submodulo');
            $table->foreignId('id_permiso')->constrained('permisos', 'id_permiso');
            $table->foreignId('id_usuario')->constrained('usuarios', 'id_usuario');
            $table->foreignId('id_rol')->nullable();//->constrained('roles', 'id_rol');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->nullable();
            $table->unsignedBigInteger('usuario_creacion')->nullable();
            $table->unsignedBigInteger('usuario_modificacion')->nullable();
            $table->boolean('estado')->default(true);
            $table->boolean('eliminado')->default(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('permiso_total');
    }
};
