<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('submodulos', function (Blueprint $table) {
            $table->id('id_submodulo');
            $table->integer('orden');
            $table->string('nombre');
            $table->string('ruta');
            $table->string('descripcion');
            $table->boolean('almacenes')->default(true);
            $table->foreignId('id_modulo')->constrained('modulos', 'id_modulo');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->unsignedBigInteger('usuario_creacion')->nullable();
            $table->timestamp('fecha_modificacion')->nullable();
            $table->unsignedBigInteger('usuario_modificacion')->nullable();
            $table->timestamp('fecha_elimino')->nullable();
            $table->unsignedBigInteger('usuario_elimino')->nullable();
            $table->boolean('estado')->default(true);
            $table->boolean('eliminado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('submodulos');
    }
};
