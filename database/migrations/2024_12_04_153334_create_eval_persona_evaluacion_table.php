<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_persona_evaluacion', function (Blueprint $table) {
            // Definir los campos de la tabla
            $table->id('id_persona_evaluacion');

            $table->foreignId('id_persona')->constrained('tbl_persona','id');
            $table->foreignId('id_evaluacion')->constrained('eval_evaluaciones','id_evaluacion');

            $table->integer('cantidad_pregunta');
            $table->integer('cantidad_pregunta_correcta');
            $table->integer('porcentaje');

            $table->unique(['id_persona', 'id_evaluacion']);
            //$table->primary('id_persona_evaluacion');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_persona_evaluacion');
    }
};
