<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_persona_pregunta', function (Blueprint $table) {
            $table->id('id_persona_pregunta');
            $table->foreignId('id_persona')->constrained('tbl_persona','id');
            $table->foreignId('id_pregunta')->constrained('eval_preguntas','id_pregunta');
            $table->boolean('correccion')->nullable();
            $table->unique(['id_persona', 'id_pregunta']);
            //$table->primary('id_persona_pregunta');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_persona_pregunta');
    }
};
