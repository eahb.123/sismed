<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_persona_respuesta', function (Blueprint $table) {
            $table->id('id_persona_respuesta');
            $table->foreignId('id_persona')->constrained('tbl_persona','id');
            $table->foreignId('id_pregunta')->constrained('eval_preguntas','id_pregunta');
            $table->foreignId('id_respuesta')->constrained('eval_respuesta','id_respuesta');
            $table->text('respuesta_texto_persona')->nullable();
            $table->boolean('respuesta_boolean_persona')->nullable();
            $table->boolean('correccion')->nullable();
            $table->unique(['id_persona', 'id_pregunta','id_respuesta']);
            //$table->primary('id_persona_respuesta');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_persona_respuesta');
    }
};
