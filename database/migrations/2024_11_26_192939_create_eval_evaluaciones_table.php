<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_evaluaciones', function (Blueprint $table) {
            $table->id('id_evaluacion');
            $table->string('nombre', 255);
            $table->text('descripcion')->nullable();
            $table->foreignId('id_estado')->constrained('catalogo_maestro');
            $table->foreignId('id_tipo_evaluacion')->constrained('catalogo_maestro');
            $table->timestamp('fecha_hora_limite')->nullable();
            $table->timestamp('fecha_crear')->nullable();
            $table->foreignId('usuario_crear')->nullable();
            $table->timestamp('fecha_editar')->nullable();
            $table->foreignId('usuario_editar')->nullable();
            $table->timestamp('fecha_eliminar')->nullable();
            $table->foreignId('usuario_eliminar')->nullable();

            //$table->primary('id_evaluacion');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_evaluaciones');
    }
};
