<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_persona', function (Blueprint $table) {
            // Agregar columna id_cargo
            $table->foreignId('id_cargo')->nullable()->constrained('cargos', 'id_cargo');

            // Cambiar tipo de dato de id_almacen a string y agregar restricción
            $table->string('id_almacen')->nullable()->constrained('tbl_almacenes', 'id_almacen');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_persona', function (Blueprint $table) {
            // Drop foreign key constraint for id_cargo
            //$table->dropForeign(['id_cargo']);

            // Drop foreign key constraint for id_almacen
            //$table->dropForeign(['id_almacen']);

            // Drop columns id_cargo and id_almacen
            $table->dropColumn(['id_cargo', 'id_almacen']);
        });
    }
};
