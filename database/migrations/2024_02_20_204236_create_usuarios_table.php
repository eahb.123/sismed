<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id('id_usuario');
            //$table->string('nombre');
            //$table->string('ape_paterno')->nullable();
            //$table->string('ape_materno')->nullable();
            $table->string('correo_electronico');
            $table->string('password');
            $table->timestamp('fecha_registro')->useCurrent();
            $table->string('imagen_perfil')->nullable();
            $table->foreignId('id_persona')->nullable()->constrained('tbl_persona', 'id'); // Agregado nullable()
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->unsignedBigInteger('usuario_creacion')->nullable();
            $table->timestamp('fecha_modificacion')->nullable();
            $table->unsignedBigInteger('usuario_modificacion')->nullable();
            $table->timestamp('fecha_elimino')->nullable();
            $table->unsignedBigInteger('usuario_elimino')->nullable();
            $table->boolean('estado')->default(true);
            $table->boolean('eliminado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuarios');
    }
};
