<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_respuesta', function (Blueprint $table) {
            $table->id('id_respuesta');
            $table->foreignId('id_pregunta')->constrained('eval_preguntas','id_pregunta');
            $table->string('respuesta_texto', 255)->nullable();
            $table->boolean('es_correcta')->nullable();
            //$table->primary('id_respuesta');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_respuesta');
    }
};
