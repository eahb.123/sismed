<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eval_preguntas', function (Blueprint $table) {
            $table->id('id_pregunta');
            $table->string('pregunta_texto', 255);
            $table->foreignId('id_evaluacion')->constrained('eval_evaluaciones','id_evaluacion');
            $table->foreignId('id_tipo_pregunta')->constrained('catalogo_maestro');

            //$table->primary('id_pregunta');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eval_preguntas');
    }
};
