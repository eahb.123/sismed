<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
            DB::statement('ALTER TABLE tbl_tablas DROP CONSTRAINT pkid_tabla');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('tbl_tablas')) {
            DB::statement('ALTER TABLE tbl_tablas ADD CONSTRAINT "pkid_tabla" PRIMARY KEY ("id_tabla", "id_tipo");');
        }
    }
};
