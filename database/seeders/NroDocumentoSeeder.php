<?php
// database/seeders/UsuariosTableSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
use App\Models\Persona;
use Illuminate\Support\Facades\DB;


class NroDocumentoSeeder extends Seeder
{
    public function run()
    {
        $nroDocumento = DB::table('tbl_tablas')->where('abreviado','=','TIP_DOC_IDENT')->first();

        if ($nroDocumento == null) {

            $idTipo = DB::table('tbl_tablas')->where('id_tabla','=','0')->max('id_tipo');

             DB::table('tbl_tablas')->insert([
                'id_tabla'=> '0',
                'id_tipo'=> $idTipo+1,
                'descripcion'=> 'TIPO DOCUMENTO DE IDENTIDAD',
                'abreviado'=> 'TIP_DOC_IDENT'
            ]);

            $nroDocumento = DB::table('tbl_tablas')->where('abreviado', 'TIP_DOC_IDENT')->first();
        }

        $tipoAgregar = [
            ['DNI', 'DOCUMENTO NACIONAL DE IDENTIDAD','1'],
            ['RUC', 'REGISTRO UNICO DE CONTRIBUYENTES','2'],
            ['CE', 'CARNET DE EXTRANJERIA','3']
        ];

        foreach ($tipoAgregar as $key => $value) {

            $tipoDocumento = DB::table('tbl_tablas')
            ->where('id_tabla','=',$nroDocumento->id_tipo)
            ->where('abreviado','=',$value[0])
            ->first();

            if ($tipoDocumento == null) {
                DB::table('tbl_tablas')->insert([
                    'id_tabla' => $nroDocumento->id_tipo,
                    'id_tipo' => $value[2],
                    'descripcion' => $value[1],
                    'abreviado' => $value[0]
                ]);

            }
        }
    }
}
