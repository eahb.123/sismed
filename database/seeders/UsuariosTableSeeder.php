<?php
// database/seeders/UsuariosTableSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
use App\Models\Persona;

class UsuariosTableSeeder extends Seeder
{
    public function run()
    {
        // Crear un usuario de ejemplo
        Usuario::create([
            'password' => bcrypt('123456789'),
            'correo_electronico' => 'admin@admin.com',
            'fecha_registro' => now(),
            'id_persona' => 1
            // Agrega otros campos según sea necesario
        ]);

        Usuario::create([
            'password' => bcrypt('123456789'),
            'correo_electronico' => 'visor@visor.com',
            'fecha_registro' => now(),
            'id_persona' => 1
            // Agrega otros campos según sea necesario
        ]);
        // Puedes agregar más usuarios según tus necesidades
    }
}
