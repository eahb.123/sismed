<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Submodulo;

class SubmoduloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 1:EMPRESA
        Submodulo::create([
            'orden' => 1,
            'nombre' => 'EMPRESA',
            'ruta' => 'empresa',
            'descripcion' => 'Empresa',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 2:SUPERMODULO
        Submodulo::create([
            'orden' => 7,
            'nombre' => 'SUPERMODULO',
            'ruta' => 'supermodulo',
            'descripcion' => 'Supermodulo',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 3:MODULO
        Submodulo::create([
            'orden' => 8,
            'nombre' => 'MODULO',
            'ruta' => 'modulo',
            'descripcion' => 'Modulo',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 4:USUARIO
        Submodulo::create([
            'orden' => 9,
            'nombre' => 'USUARIO',
            'ruta' => 'usuario',
            'descripcion' => 'usuario',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 5:SUBMODULO
        Submodulo::create([
            'orden' => 8,
            'nombre' => 'SUBMODULO',
            'ruta' => 'submodulo',
            'descripcion' => 'Submodulo',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 6:PERMISO
        Submodulo::create([
            'orden' => 8,
            'nombre' => 'PERMISO',
            'ruta' => 'permiso',
            'descripcion' => 'Permiso',
            'id_modulo' => '1',
            'almacenes' => false

        ]);
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        //SUBMODULO: 7:ALMACENES
        Submodulo::create([
            'orden' => 5,
            'nombre' => 'ALMACENES',
            'ruta' => 'almacenes',
            'descripcion' => 'Almacenes',
            'id_modulo' => '3',
            'almacenes' => false

        ]);


        //SUPERMODULO: 2:CONSULTA
        //MODULO: 2:STOCK
        //SUBMODULO: 8:MEDICAMENTO
        Submodulo::create([
            'orden' => 2,
            'nombre' => 'MEDICAMENTO',
            'ruta' => 'medicamento',
            'descripcion' => 'Medicamento',
            'id_modulo' => '2',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 2:STOCK
        //SUBMODULO: 9:PORCENTAJE
        Submodulo::create([
            'orden' => 3,
            'nombre' => 'CUADRO RESUMEN',
            'ruta' => 'cuadro_resumen',
            'descripcion' => 'Cuadro Resumen',
            'id_modulo' => '2',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 3:MOVIMIENTOS
        //SUBMODULO: 10:ESTABLECIMIENTO
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'ESTABLECIMIENTOS',
            'ruta' => 'establecimientos',
            'descripcion' => 'Establecimientos',
            'id_modulo' => '3',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:REPORTE
        //MODULO: 4:ICI
        //SUBMODULO: 11:ICI DIARIO
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'ICI DIARIO',
            'ruta' => 'ici_diario',
            'descripcion' => 'Ici Diario',
            'id_modulo' => '4',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 2:STOCK
        //SUBMODULO: 12:LOTES POR VENCER
        Submodulo::create([
            'orden' => 2,
            'nombre' => 'LOTES POR VENCER',
            'ruta' => 'lotes_por_vencer',
            'descripcion' => 'Lotes Por Vencer',
            'id_modulo' => '2',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 3:MOVIMIENTOS
        //SUBMODULO: 13:ALMACENES
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'ALMACENES',
            'ruta' => 'almacenes',
            'descripcion' => 'Almacenes',
            'id_modulo' => '3',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 14:BOTIQUIN
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'BOTIQUIN',
            'ruta' => 'botiquin',
            'descripcion' => 'Botiquin',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 15:COCHE DE PARO
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'COCHE DE PARO',
            'ruta' => 'coche_de_paro',
            'descripcion' => 'Coche De Paro',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 16:KITS
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'KIT EN CASO DE VIOLENCIA SEXUAL',
            'ruta' => 'kit_en_caso_de_violencia_sexual',
            'descripcion' => 'Kit En Caso De Violencia Sexual',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 17:CLAVES
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'CLAVES',
            'ruta' => 'claves',
            'descripcion' => 'Claves',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 18:DENGUE
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'DENGUE',
            'ruta' => 'dengue',
            'descripcion' => 'Dengue',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 19:KIT DE PARTO
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'KIT DE PARTO',
            'ruta' => 'kit_de_parto',
            'descripcion' => 'Kit De Parto',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 20:KIT DE RECIEN NACIDO
        Submodulo::create([
            'orden' => 4,
            'nombre' => 'KIT DE RECIEN NACIDO',
            'ruta' => 'kit_de_recien_nacido',
            'descripcion' => 'Kit De Recien Nacido',
            'id_modulo' => '5',
            'almacenes' => false

        ]);


        //SUPERMODULO: 2:REPORTE
        //MODULO: 4:ICI
        //SUBMODULO: 21:ICI ESTRATEGICO
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'ICI ESTRATEGICO',
            'ruta' => 'ici_estrategico',
            'descripcion' => 'Ici Estrategico',
            'id_modulo' => '4',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:REPORTE
        //MODULO: 4:ICI
        //SUBMODULO: 22:ICI POR PRODUCTO
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'ICI POR PRODUCTO',
            'ruta' => 'ici_por_producto',
            'descripcion' => 'Ici Por Producto',
            'id_modulo' => '4',
            'almacenes' => false

        ]);

        //SUPERMODULO: 4:LIBRO
        //MODULO: 6:LIBRO DE CONTROL
        //SUBMODULO: 23:ESTUPEFACIENTE IIA
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'ESTUPEFACIENTE IIA',
            'ruta' => 'estupefaciente_iia',
            'descripcion' => 'Estupefaciente IIA',
            'id_modulo' => '6',

        ]);

        //SUPERMODULO: 4:LIBRO
        //MODULO: 6:LIBRO DE CONTROL
        //SUBMODULO: 24:PSICOTROPICO (IIIA, IIB, IIC)
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'PSICOTROPICO (IIIA, IIB, IIC)',
            'ruta' => 'psicotropico_iiia_iib_iic',
            'descripcion' => 'Psicotropico (IIIA,IIB,IIC)',
            'id_modulo' => '6',

        ]);


        //SUPERMODULO: 4:LIBRO
        //MODULO: 6:LIBRO DE CONTROL
        //SUBMODULO: 25:PSICOTROPICO LISTA IVB
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'PSICOTROPICO LISTA IVB',
            'ruta' => 'psicotropico_lista_ivb',
            'descripcion' => 'Psicotropico Lista IVB',
            'id_modulo' => '6',

        ]);

        //SUPERMODULO: 5:MI CUENTA
        //MODULO: 7:USUARIO
        //SUBMODULO: 26: PERFIL
        Submodulo::create([
            'orden' => 6,
            'nombre' => 'PERFIL',
            'ruta' => 'perfil',
            'descripcion' => 'Perfil',
            'id_modulo' => '7',
            'almacenes' => false

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        //SUBMODULO: 27:KIT ESCENARIO III
        Submodulo::create([
            'orden' => 8,
            'nombre' => 'KIT ESCENARIO III',
            'ruta' => 'kit_escenario_iii',
            'descripcion' => 'Kit Escenario III',
            'id_modulo' => '5',
            'almacenes' => false

        ]);

        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 8:EVALUACION
        //SUBMODULO: 28:EVALUACION
        Submodulo::create([
            'orden' => 8,
            'nombre' => 'EVALUACION',
            'ruta' => 'evaluacion',
            'descripcion' => 'Evaluacion',
            'id_modulo' => '8',
            'almacenes' => false

        ]);

    }
}
