<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Supermodulo;

class SupermoduloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //SUPERMODULO: 1:ADMINISTRADOR
        Supermodulo::create([
            'orden' => 1,
            'nombre' => 'ADMINISTRADOR',
            'id_almacen' => '07278F0101',
            'ruta' => 'administrador',
            'descripcion' => 'administrador',
            'fecha_creacion' => now(),
            'fecha_modificacion' => now()
        ]);
        //SUPERMODULO: 2:CONSULTA
        Supermodulo::create([
            'orden' => 2,
            'nombre' => 'CONSULTA',
            'id_almacen' => '07278F0101',
            'ruta' => 'consulta',
            'descripcion' => 'Consulta',
            'fecha_creacion' => now(),
            'fecha_modificacion' => now()
        ]);
        //SUPERMODULO: 3:REPORTE
        Supermodulo::create([
            'orden' => 3,
            'nombre' => 'REPORTE',
            'id_almacen' => '07278F0101',
            'ruta' => 'reporte',
            'descripcion' => 'Reporte',
            'fecha_creacion' => now(),
            'fecha_modificacion' => now()
        ]);

        //SUPERMODULO: 4:LIBRO
        Supermodulo::create([
            'orden' => 4,
            'nombre' => 'LIBRO',
            'id_almacen' => '07278F0101',
            'ruta' => 'libro',
            'descripcion' => 'Libro',
            'fecha_creacion' => now(),
            'fecha_modificacion' => now()
        ]);

        //SUPERMODULO: 5:MI CUENTA
        Supermodulo::create([
            'orden' => 5,
            'nombre' => 'MI CUENTA',
            'id_almacen' => '07278F0101',
            'ruta' => 'mi_cuenta',
            'descripcion' => 'Mi Cuenta',
            'fecha_creacion' => now(),
            'fecha_modificacion' => now()
        ]);
}
}
