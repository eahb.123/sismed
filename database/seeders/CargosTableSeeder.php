<?php
// database/seeders/CargosTableSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cargo;

class CargosTableSeeder extends Seeder
{
    public function run()
    {
        // Crear un cargo de ejemplo
        Cargo::create([
            'nombre_cargo' => 'administrador',
            'descripcion_cargo' => 'administrador',
            'jerarquia_cargo' => 1, // Asigna el valor de jerarquía correspondiente
            // Agrega otros campos según sea necesario
        ]);

        // Puedes agregar más cargos según tus necesidades
    }
}