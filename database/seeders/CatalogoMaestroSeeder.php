<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CatalogoMaestroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['categoria' => 'TIPO DE PREGUNTA', 'valor' => 'OPCION MULTIPLE', 'opciones' => true, 'orden' => 1],
            ['categoria' => 'TIPO DE PREGUNTA', 'valor' => 'VERDADERO O FALSO', 'opciones' => true, 'orden' => 2],
            ['categoria' => 'TIPO DE PREGUNTA', 'valor' => 'RESPUESTA ABIERTA', 'opciones' => false, 'orden' => 3],
            ['categoria' => 'TIPO DE EVALUACION', 'valor' => 'EXAMEN', 'opciones' => false, 'orden' => 1],
            ['categoria' => 'TIPO DE EVALUACION', 'valor' => 'ENCUESTA', 'opciones' => false, 'orden' => 2],
            ['categoria' => 'ESTADO DE EVALUACION', 'valor' => 'CREADO', 'opciones' => false, 'orden' => 1],
            ['categoria' => 'ESTADO DE EVALUACION', 'valor' => 'INICIADO', 'opciones' => false, 'orden' => 2],
            ['categoria' => 'ESTADO DE EVALUACION', 'valor' => 'CERRADO', 'opciones' => false, 'orden' => 3],
            ['categoria' => 'ESTADO DE EVALUACION', 'valor' => 'FINALIZADO', 'opciones' => false, 'orden' => 4],
        ];

        // Insertar los datos en la tabla
        DB::table('catalogo_maestro')->insert($data);
    }
}
