<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permiso;

class PermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $cruds = [1,2,3,4,5,6,7,26,28];
        foreach ($cruds as $crud) {
        //LISTAR
        Permiso::create([
            'nombre' => 'LISTAR',
            'descripcion' => 'Listar',
            'ruta' => 'listar',
            'id_submodulo' => $crud,

        ]);

        //CREAR
        Permiso::create([
            'nombre' => 'CREAR',
            'descripcion' => 'Crear',
            'ruta' => 'crear',
            'id_submodulo' => $crud,

        ]);

        //EDITAR
        Permiso::create([
            'nombre' => 'EDITAR',
            'descripcion' => 'Editar',
            'ruta' => 'editar',
            'id_submodulo' => $crud,

        ]);

        //ELIMINAR
        Permiso::create([
            'nombre' => 'ELIMINAR',
            'descripcion' => 'Eliminar',
            'ruta' => 'eliminar',
            'id_submodulo' => $crud,

        ]);
        }




        $listars = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,27];
        foreach ($listars as $listar) {
        //LISTAR
        Permiso::create([
            'nombre' => 'LISTAR',
            'descripcion' => 'Listar',
            'ruta' => 'listar',
            'id_submodulo' => $listar,

        ]);
        }


        $listars = [28];
        foreach ($listars as $listar) {
        //LISTAR
        Permiso::create([
            'nombre' => 'CORRECCION',
            'descripcion' => 'Correccion',
            'ruta' => 'Correccion',
            'id_submodulo' => $listar,

        ]);
        }

        return;
        for ($i=0; $i < 25 ; $i++) {

        }

    }
}
