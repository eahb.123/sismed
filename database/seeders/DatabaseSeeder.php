<?php
// database/seeders/DatabaseSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // Llama al seeder de Cargos
        $this->call(CargosTableSeeder::class);

        // Llama al seeder de Usuarios
        $this->call(UsuariosTableSeeder::class);

        // Llama al seeder de Supermodulo
        $this->call(SupermoduloSeeder::class);

        // Llama al seeder de modulo
        $this->call(ModuloSeeder::class);

        // Llama al seeder de submodulo
        $this->call(SubmoduloSeeder::class);

        // Llama al seeder de permiso
        $this->call(PermisoSeeder::class);

        // Llama al seeder de permiso
        $this->call(PermisoTotalSeeder::class);

        // Llama al seeder de numero de documento
        $this->call(NroDocumentoSeeder::class);

        // Llama al seeder de numero de documento
        $this->call(CatalogoMaestroSeeder::class);
    }
}
