<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PermisoTotal;
use App\Models\Supermodulo;
use App\Models\Modulo;
use App\Models\Submodulo;
use App\Models\Permiso;
use App\Models\Almacen;

class PermisoTotalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $permiso=Permiso::
        with('submodulo')
        ->with('submodulo.modulo')
        ->with('submodulo.modulo.supermodulo')
        ->get();

        foreach ($permiso as $key => $value) {

            if ($value->submodulo->almacenes) {

                $almacen = Almacen::all();

                foreach ($almacen as $key1 => $value1) {

                    PermisoTotal::create([
                        'id_empresa' => '1',
                        'id_almacen' => $value1->id_almacen,
                        'id_supermodulo' => $value->submodulo->modulo->supermodulo->id_supermodulo,
                        'id_modulo' => $value->submodulo->modulo->id_modulo,
                        'id_submodulo' => $value->submodulo->id_submodulo,
                        'id_permiso' => $value->id_permiso,
                        'id_usuario' => '1',
                    ]);

                }


            }else{
                PermisoTotal::create([
                    'id_empresa' => '1',
                    'id_almacen' => '',
                    'id_supermodulo' => $value->submodulo->modulo->supermodulo->id_supermodulo,
                    'id_modulo' => $value->submodulo->modulo->id_modulo,
                    'id_submodulo' => $value->submodulo->id_submodulo,
                    'id_permiso' => $value->id_permiso,
                    'id_usuario' => '1',
                ]);
            }

        }


        foreach ($permiso as $key => $value) {

            if ($value->submodulo->modulo->supermodulo->id_supermodulo != '1' && $value->submodulo->modulo->supermodulo->id_supermodulo != '6') {

                if ($value->submodulo->almacenes) {
                    $almacen = Almacen::all();
                    foreach ($almacen as $key1 => $value1) {
                        PermisoTotal::create([
                            'id_empresa' => '1',
                            'id_almacen' => $value1->id_almacen,
                            'id_supermodulo' => $value->submodulo->modulo->supermodulo->id_supermodulo,
                            'id_modulo' => $value->submodulo->modulo->id_modulo,
                            'id_submodulo' => $value->submodulo->id_submodulo,
                            'id_permiso' => $value->id_permiso,
                            'id_usuario' => '2',
                        ]);
                    }
                }else{
                    PermisoTotal::create([
                        'id_empresa' => '1',
                        'id_almacen' => '',
                        'id_supermodulo' => $value->submodulo->modulo->supermodulo->id_supermodulo,
                        'id_modulo' => $value->submodulo->modulo->id_modulo,
                        'id_submodulo' => $value->submodulo->id_submodulo,
                        'id_permiso' => $value->id_permiso,
                        'id_usuario' => '2',
                    ]);
                }
            }
        }
        return;


    }
}
