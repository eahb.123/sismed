<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Modulo;


class ModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 1:MAESTRA
        Modulo::create([
            'orden' => 1,
            'nombre' => 'MAESTRA',
            'id_supermodulo' => '1',
            'ruta' => 'maestra',
            'descripcion' => 'Maestra',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 2:STOCK
        Modulo::create([
            'orden' => 2,
            'nombre' => 'STOCK',
            'id_supermodulo' => '2',
            'ruta' => 'stock',
            'descripcion' => 'Stock',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 3:MOVIMIENTOS
        Modulo::create([
            'orden' => 3,
            'nombre' => 'MOVIMIENTOS',
            'id_supermodulo' => '2',
            'ruta' => 'movimientos',
            'descripcion' => 'Movimientos',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 2:REPORTE
        //MODULO: 4:ICI
        Modulo::create([
            'orden' => 4,
            'nombre' => 'ICI',
            'id_supermodulo' => '3',
            'ruta' => 'ici',
            'descripcion' => 'Ici',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 2:CONSULTA
        //MODULO: 5:KITS
        Modulo::create([
            'orden' => 5,
            'nombre' => 'KITS',
            'id_supermodulo' => '2',
            'ruta' => 'kits',
            'descripcion' => 'Kits',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 4:LIBRO
        //MODULO: 6:LIBRO DE CONTROL
        Modulo::create([
            'orden' => 6,
            'nombre' => 'LIBRO DE CONTROL',
            'id_supermodulo' => '4',
            'ruta' => 'libro_de_control',
            'descripcion' => 'Libro De Control',
            'fecha_creacion' => now(),

        ]);


        //SUPERMODULO: 5:MI CUENTA
        //MODULO: 7:USUARIO
        Modulo::create([
            'orden' => 7,
            'nombre' => 'USUARIO',
            'id_supermodulo' => '5',
            'ruta' => 'usuario',
            'descripcion' => 'Usuario',
            'fecha_creacion' => now(),

        ]);

        //SUPERMODULO: 1:ADMINISTRADOR
        //MODULO: 8:EVALUACION
        Modulo::create([
            'orden' => 8,
            'nombre' => 'EVALUACION',
            'id_supermodulo' => '1',
            'ruta' => 'evaluacion',
            'descripcion' => 'Evaluacion',
            'fecha_creacion' => now(),

        ]);

    }
}
